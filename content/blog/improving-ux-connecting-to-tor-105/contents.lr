title: Improving the user experience of connecting to Tor in Tor Browser 10.5
---
pub_date: 2021-06-24
---
author: antonela
---
summary: The journey to improve the user experience of connecting to the Tor network starts with Tor Browser 10.5, which will be released later this month. This release is the first in an upcoming series of releases helping censored users seamlessly access the open internet.
---
_html_body:

<p>During the past few years,<a href="https://community.torproject.org/user-research/reports/"> the UX team has been working on qualitatively improving the entire Tor Browser user journey:</a> from discovering to finding, downloading, installing, starting, and browsing; we released a seamless and familiar experience for our largest user base. Tor Browser <a href="https://blog.torproject.org/new-release-tor-browser-95">9.5</a> was an entire reshaping of the experience for users reaching onion sites, Tor Browser <a href="https://blog.torproject.org/new-release-tor-browser-90">9.0</a> and <a href="https://blog.torproject.org/new-release-tor-browser-80">8.0</a> shipped an improved experience for core legacy issues, and Tor Browser <a href="https://blog.torproject.org/new-release-tor-browser-85">8.5</a> was a rebranded release. However, users continue to report that launching Tor Browser for the first time is an abrasive experience.</p>
<p>Our <a href="https://blog.torproject.org/reaching-people-where-they-are">user research program in the Global South</a> has shed light on how users experiment when confronted with pain points while connecting to Tor. Censored users have also explicitly mentioned how confusing they find the process of copying a bridge address from a webpage and then pasting that address into the browser interface. During our interviews in late 2020, a journalist living in Hong Kong said, “Using bridges is a very manual process.”</p>
<p>Additionally, users have specifically reported that they find <a href="https://support.torproject.org/glossary/tor-launcher/">Tor Launcher</a> confusing. Research exposed these pain points and has demonstrated how confusion caused by cognitive overload delays the user’s decision-making flow. Known issues with Tor Launcher, like the time gap between Tor Launcher and the main browser window opening after first-time installation, has left some users disappointed. In response, the growing ecosystem of Tor apps have opted to embed the connection experience in their UI instead, like you can see in Brave’s <a href="https://support.brave.com/hc/en-us/articles/360018121491-What-is-a-Private-Window-with-Tor-Connectivity-">Private Windows with Tor</a> or <a href="https://onionshare.org/">OnionShare</a>.</p>
<p>Our findings have informed the development of a new iteration of Tor Browser that focuses on improving the flow users follow when connecting to Tor for the first time and on subsequent uses by removing the Tor Launcher UI, making Tor bootstrapping automatic, and relying on a better UI embedded in the main browser screen to provide visual feedback. We are also working on improving the censored user's experience by designing a user flow that will detect censorship and suggest and provide bridges to connect to the Tor network successfully.</p>
<p> </p>
<p><img alt="Quickstart in Tor Browser 10.5" src="/static/images/blog/inline-images/tb105-6.gif" class="align-center" /></p>
<p> </p>
<p>The journey to improve the user experience of connecting to the Tor network starts with Tor Browser 10.5, which will be released later this month. This release is the first in an upcoming series of releases helping censored users seamlessly access the open internet.</p>
<p> </p>
<p><img alt="Quickstart preferences in Tor Browser 10.5" src="/static/images/blog/inline-images/quickstart-stable-release_2.png" class="align-center" /></p>
<p style="line-height:1.38"> </p>
<p style="line-height:1.38">We would love to hear your thoughts about this release. Feel free to <a href="https://anonticket.onionize.space/">open a ticket anonymously </a>or email the <a href="https://lists.torproject.org/cgi-bin/mailman/listinfo/ux">tor-ux mailing list</a>.</p>
<p> </p>

