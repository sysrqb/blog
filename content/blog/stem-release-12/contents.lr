title: Stem Release 1.2
---
pub_date: 2014-06-01
---
author: atagar
---
tags: stem
---
_html_body:

<p>Hi all. After months of work I'm please to announce the release of Stem 1.2.0!</p>

<p>For those who aren't familiar with it, Stem is a Python library for interacting with Tor. With it you can script against your relay, descriptor data, or even write applications similar to arm and Vidalia.</p>

<p><b><a href="https://stem.torproject.org/" rel="nofollow">https://stem.torproject.org/</a></b></p>

<p>So what's new in this release?</p>

<h2>Interactive Tor Interpreter</h2>

<p>The control interpreter is a new method for interacting with Tor's control interface that combines an interactive python interpreter with raw access similar to telnet. This adds several usability features, such as...</p>

<ul>
<li>Irc-style commands like '/help'.</li>
<li>Tab completion for Tor's controller commands.</li>
<li>History scrollback by pressing up/down.</li>
<li>Transparently handles Tor authentication at startup.</li>
<li>Colorized output for improved readability.</li>
</ul>

<p>For a tutorial to get you started see...</p>

<p><b><a href="https://stem.torproject.org/tutorials/down_the_rabbit_hole.html" rel="nofollow">Down the Rabbit Hole</a></b></p>

<h2>New connect() Function</h2>

<p>This release of Stem provides a new, even easier method for establishing controllers. Connecting to Tor can now be as easy as...</p>

<p>import sys</p>

<p>from stem.connection import connect</p>

<p>if __name__ == '__main__':<br />
  controller = connect()</p>

<p>  if not controller:<br />
    sys.exit(1)  # unable to get a connection</p>

<p>  print 'Tor is running version %s' % controller.get_version()<br />
  controller.close()</p>

<p>For a rundown on the myriad of improvements and fixes in this release see...</p>

<p><b><a href="https://stem.torproject.org/change_log.html#version-1-2" rel="nofollow">https://stem.torproject.org/change_log.html#version-1-2</a></b></p>

<p>Cheers! -Damian</p>

---
_comments:

<a id="comment-62450"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-62450" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">June 01, 2014</p>
    </div>
    <a href="#comment-62450">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-62450" class="permalink" rel="bookmark">Has there been a problem</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Has there been a problem with the Tor network of late?  The reason I ask is because it has become increasingly slower to the point of being unusable.  Tor was actually becoming almost indistinguishable from a non-Tor connection, and at times it appeared to be even faster.  But recently Tor has taken a step into the dark ages!</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-62462"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-62462" class="contextual-region comment js-comment" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  arma
  </article>
    <div class="comment-header">
      <p class="comment__submitted">arma said:</p>
      <p class="date-time">June 01, 2014</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-62450" class="permalink" rel="bookmark">Has there been a problem</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-62462">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-62462" class="permalink" rel="bookmark">No, it&#039;s about the same. Tor</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>No, it's about the same. Tor network performance has improved a lot in the last few years, I agree -- a lot of that has to do with additional capacity from big relays.<br />
<a href="https://metrics.torproject.org/network.html" rel="nofollow">https://metrics.torproject.org/network.html</a></p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-62681"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-62681" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">June 03, 2014</p>
    </div>
    <a href="#comment-62681">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-62681" class="permalink" rel="bookmark">Hi everybody,
Just noticed a</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Hi everybody,<br />
Just noticed a website torproject.lu... What the hell? Since when usbtor in Luxembourg?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-62834"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-62834" class="contextual-region comment js-comment" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  arma
  </article>
    <div class="comment-header">
      <p class="comment__submitted">arma said:</p>
      <p class="date-time">June 05, 2014</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-62681" class="permalink" rel="bookmark">Hi everybody,
Just noticed a</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-62834">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-62834" class="permalink" rel="bookmark">They&#039;re on our mirror</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>They're on our mirror list:<br />
<a href="https://www.torproject.org/getinvolved/mirrors" rel="nofollow">https://www.torproject.org/getinvolved/mirrors</a><br />
along with a lot of other domains.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-63198"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-63198" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">June 13, 2014</p>
    </div>
    <a href="#comment-63198">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-63198" class="permalink" rel="bookmark">can somebody answer a</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>can somebody answer a question for me? when i open the tor browser does it automatically take me to the "dark web" , "invisible web"? because just seems to take me to almost same old sites???!!! I am just interested to go there!!!!</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-63241"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-63241" class="contextual-region comment js-comment" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  arma
  </article>
    <div class="comment-header">
      <p class="comment__submitted">arma said:</p>
      <p class="date-time">June 13, 2014</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-63198" class="permalink" rel="bookmark">can somebody answer a</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-63241">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-63241" class="permalink" rel="bookmark">The &quot;dark web&quot; is a myth</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>The "dark web" is a myth created by the media to sell newspapers.</p>
<p><a href="https://trac.torproject.org/projects/tor/wiki/doc/HowBigIsTheDarkWeb" rel="nofollow">https://trac.torproject.org/projects/tor/wiki/doc/HowBigIsTheDarkWeb</a></p>
</div>
  </div>
</article>
<!-- Comment END -->
</div>
