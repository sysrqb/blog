title: Tor Weekly News — March 11th, 2015
---
pub_date: 2015-03-11
---
author: harmony
---
tags: tor weekly news
---
categories: reports
---
_html_body:

<p>Welcome to the tenth issue in 2015 of Tor Weekly News, the <a href="https://lists.torproject.org/cgi-bin/mailman/listinfo/tor-news" rel="nofollow">weekly newsletter</a> that covers what’s happening in the Tor community.</p>

<h1>Tor 0.2.6.4-rc is out</h1>

<p>Nick Mathewson <a href="https://lists.torproject.org/pipermail/tor-talk/2015-March/037191.html" rel="nofollow">announced</a> the first release candidate in the Tor 0.2.6 series; the next release will hopefully be the stable version.</p>

<p>This update “fixes an issue in the directory code that an attacker might be able to use in order to crash certain Tor directories”, as well as various other bugfixes and improvements. See Nick’s announcement for a full changelog, and get your copy of the source code from the <a href="https://dist.torproject.org/" rel="nofollow">distribution directory</a>.</p>

<h1>More monthly status reports for February 2015</h1>

<p>The wave of regular monthly reports from Tor project members for the month of February continued, with reports from <a href="https://lists.torproject.org/pipermail/tor-reports/2015-March/000772.html" rel="nofollow">Sebastian Hahn</a>, <a href="https://lists.torproject.org/pipermail/tor-reports/2015-March/000773.html" rel="nofollow">Arlo Breault</a>, <a href="https://lists.torproject.org/pipermail/tor-reports/2015-March/000774.html" rel="nofollow">Karsten Loesing</a>, and <a href="https://lists.torproject.org/pipermail/tor-reports/2015-March/000775.html" rel="nofollow">George Kadianakis</a>.</p>

<p>Israel Leiva reported on behalf of the <a href="https://lists.torproject.org/pipermail/tor-reports/2015-March/000776.html" rel="nofollow">GetTor project</a>, while the Tails team also resumed publishing status reports, with one report for the <a href="https://tails.boum.org/news/report_end_of_2014" rel="nofollow">second half of 2014</a> and one for the <a href="https://tails.boum.org/news/report_2015_01-02" rel="nofollow">beginning of this year</a>.</p>

<h1>Miscellaneous news</h1>

<p>Ximin Luo <a href="https://lists.torproject.org/pipermail/tor-dev/2015-March/008369.html" rel="nofollow">announced</a> preliminary Debian packages for the <a href="https://trac.torproject.org/projects/tor/wiki/doc/meek" rel="nofollow">meek pluggable transport</a>. See Ximin’s message for installation instructions.</p>

<p>Sukhbir Singh <a href="https://trac.torproject.org/" rel="nofollow">announced</a> a second alpha version of Tor Messenger, the torified instant-messaging client with multi-protocol support and Off-the-Record encryption. While not yet ready for public use, Linux bundles are available for developers and advanced users who want to try out this exciting new software and provide feedback; if you run into any new issues, open a ticket on the <a href="https://lists.torproject.org/pipermail/tor-dev/2015-March/008363.html" rel="nofollow">bug tracker</a> and select the “Tor Messenger” component.</p>

<p>The Tails team <a href="https://mailman.boum.org/pipermail/tails-dev/2015-March/008354.html" rel="nofollow">set out</a> the release schedule for version 1.3.1 of the anonymous live operating system.</p>

<p>For the past few years, Tor has taken part in Google’s Summer of Code mentoring program, with many positive contributions made and several projects launched or redeveloped as a result; this year, however, the program has been scaled back somewhat, and longstanding participants like the Tor Project, Mozilla, and the Linux Foundation have been “dropped to make way for newcomers”, as Damian Johnson noted in his monthly report [see above]. If you’d still like to join the community of Tor developers, don’t be deterred: see the <a href="https://www.torproject.org/getinvolved/volunteer" rel="nofollow">Volunteer</a> page for a wealth of ideas for getting started.</p>

<p>The British Parliamentary Office of Science and Technology, “charged with providing independent and balanced analysis of policy issues that have a basis in science and technology”, issued a briefing paper on the subject of “<a href="http://www.parliament.uk/briefing-papers/POST-PN-488/the-darknet-and-online-anonymity" rel="nofollow">the darknet and online anonymity</a>”.</p>

<p><a href="https://www.atagar.com/arm/" rel="nofollow">arm</a> is the status monitor of choice for Tor relay operators everywhere; unfortunately, the name is easily confused with that of the ARM processor family, so lead developer Damian Johnson is <a href="https://lists.torproject.org/pipermail/tor-dev/2015-March/008398.html" rel="nofollow">poised</a> to change the program’s name to “Seth”: “Any strong reasons to pick something else? Nothing is set in stone yet so still open to alternatives.”</p>

<p>Tor developer and Nos Oignons member Lunar gave a lengthy and detailed <a href="http://www.franceculture.fr/emission-les-nouvelles-vagues-le-secret-35-bienvenue-sur-tor-l%E2%80%99internet-secret-2015-02-18" rel="nofollow">interview</a> to France Culture’s “Les Nouvelles vagues”, offering an introduction to Tor and online privacy.</p>

<p>This issue of Tor Weekly News has been assembled by Karsten Loesing, the Tails team, and Harmony.</p>

<p>Want to continue reading TWN? Please help us create this newsletter. We still need more volunteers to watch the Tor community and report important news. Please see the <a href="https://trac.torproject.org/projects/tor/wiki/TorWeeklyNews" rel="nofollow">project page</a>, write down your name and subscribe to the <a href="https://lists.torproject.org/cgi-bin/mailman/listinfo/news-team" rel="nofollow">team mailing list</a> if you want to get involved!</p>

