title: Tor 0.3.2.5-alpha is released!
---
pub_date: 2017-11-22
---
author: nickm
---
_html_body:

<p>Tor 0.3.2.5-alpha is the fifth alpha release in the 0.3.2.x series. It fixes several stability and reliability bugs, including a fix for intermittent bootstrapping failures that some people have been seeing since the 0.3.0.x series.</p>
<p>You can download the source from the usual place on the website. Binary packages should be available soon. Tor Browser is likely to skip this alpha, and pick up the next one, which should be out in early November.</p>
<p>Remember: This is an alpha release, and it's likely to have more bugs than usual. We hope that people will try it out to find and report bugs, though.</p>
<p>Please test this alpha out -- many of these fixes will soon be backported to stable Tor versions if no additional bugs are found in them.</p>
<h2>Changes in version 0.3.2.5-alpha - 2017-11-22</h2>
<ul>
<li>Major bugfixes (bootstrapping):
<ul>
<li>Fetch descriptors aggressively whenever we lack enough to build circuits, regardless of how many descriptors we are missing. Previously, we would delay launching the fetch when we had fewer than 15 missing descriptors, even if some of those descriptors were blocking circuits from building. Fixes bug <a href="https://bugs.torproject.org/23985">23985</a>; bugfix on 0.1.1.11-alpha. The effects of this bug became worse in 0.3.0.3-alpha, when we began treating missing descriptors from our primary guards as a reason to delay circuits.</li>
<li>Don't try fetching microdescriptors from relays that have failed to deliver them in the past. Fixes bug <a href="https://bugs.torproject.org/23817">23817</a>; bugfix on 0.3.0.1-alpha.</li>
</ul>
</li>
<li>Minor features (directory authority):
<ul>
<li>Make the "Exit" flag assignment only depend on whether the exit policy allows connections to ports 80 and 443. Previously relays would get the Exit flag if they allowed connections to one of these ports and also port 6667. Resolves ticket <a href="https://bugs.torproject.org/23637">23637</a>.</li>
</ul>
</li>
</ul>
<p> </p>
<!--break--><ul>
<li>Minor features (geoip):
<ul>
<li>Update geoip and geoip6 to the November 6 2017 Maxmind GeoLite2 Country database.</li>
</ul>
</li>
<li>Minor features (linux seccomp2 sandbox):
<ul>
<li>Update the sandbox rules so that they should now work correctly with Glibc 2.26. Closes ticket <a href="https://bugs.torproject.org/24315">24315</a>.</li>
</ul>
</li>
<li>Minor features (logging):
<ul>
<li>Downgrade a pair of log messages that could occur when an exit's resolver gave us an unusual (but not forbidden) response. Closes ticket <a href="https://bugs.torproject.org/24097">24097</a>.</li>
<li>Improve the message we log when re-enabling circuit build timeouts after having received a consensus. Closes ticket <a href="https://bugs.torproject.org/20963">20963</a>.</li>
</ul>
</li>
<li>Minor bugfixes (compilation):
<ul>
<li>Fix a memory leak warning in one of the libevent-related configuration tests that could occur when manually specifying -fsanitize=address. Fixes bug <a href="https://bugs.torproject.org/24279">24279</a>; bugfix on 0.3.0.2-alpha. Found and patched by Alex Xu.</li>
<li>When detecting OpenSSL on Windows from our configure script, make sure to try linking with the ws2_32 library. Fixes bug <a href="https://bugs.torproject.org/23783">23783</a>; bugfix on 0.3.2.2-alpha.</li>
</ul>
</li>
<li>Minor bugfixes (control port, linux seccomp2 sandbox):
<ul>
<li>Avoid a crash when attempting to use the seccomp2 sandbox together with the OwningControllerProcess feature. Fixes bug <a href="https://bugs.torproject.org/24198">24198</a>; bugfix on 0.2.5.1-alpha.</li>
</ul>
</li>
<li>Minor bugfixes (control port, onion services):
<ul>
<li>Report "FAILED" instead of "UPLOAD_FAILED" "FAILED" for the HS_DESC event when a service is not able to upload a descriptor. Fixes bug <a href="https://bugs.torproject.org/24230">24230</a>; bugfix on 0.2.7.1-alpha.</li>
</ul>
</li>
<li>Minor bugfixes (directory cache):
<ul>
<li>Recover better from empty or corrupt files in the consensus cache directory. Fixes bug <a href="https://bugs.torproject.org/24099">24099</a>; bugfix on 0.3.1.1-alpha.</li>
<li>When a consensus diff calculation is only partially successful, only record the successful parts as having succeeded. Partial success can happen if (for example) one compression method fails but the others succeed. Previously we misrecorded all the calculations as having succeeded, which would later cause a nonfatal assertion failure. Fixes bug <a href="https://bugs.torproject.org/24086">24086</a>; bugfix on 0.3.1.1-alpha.</li>
</ul>
</li>
<li>Minor bugfixes (logging):
<ul>
<li>Only log once if we notice that KIST support is gone. Fixes bug <a href="https://bugs.torproject.org/24158">24158</a>; bugfix on 0.3.2.1-alpha.</li>
<li>Suppress a log notice when relay descriptors arrive. We already have a bootstrap progress for this so no need to log notice everytime tor receives relay descriptors. Microdescriptors behave the same. Fixes bug <a href="https://bugs.torproject.org/23861">23861</a>; bugfix on 0.2.8.2-alpha.</li>
</ul>
</li>
<li>Minor bugfixes (network layer):
<ul>
<li>When closing a connection via close_connection_immediately(), we mark it as "not blocked on bandwidth", to prevent later calls from trying to unblock it, and give it permission to read. This fixes a backtrace warning that can happen on relays under various circumstances. Fixes bug <a href="https://bugs.torproject.org/24167">24167</a>; bugfix on 0.1.0.1-rc.</li>
</ul>
</li>
<li>Minor bugfixes (onion services):
<ul>
<li>The introduction circuit was being timed out too quickly while waiting for the rendezvous circuit to complete. Keep the intro circuit around longer instead of timing out and reopening new ones constantly. Fixes bug <a href="https://bugs.torproject.org/23681">23681</a>; bugfix on 0.2.4.8-alpha.</li>
<li>Rename the consensus parameter "hsdir-interval" to "hsdir_interval" so it matches dir-spec.txt. Fixes bug <a href="https://bugs.torproject.org/24262">24262</a>; bugfix on 0.3.1.1-alpha.</li>
<li>Silence a warning about failed v3 onion descriptor uploads that can happen naturally under certain edge cases. Fixes part of bug <a href="https://bugs.torproject.org/23662">23662</a>; bugfix on 0.3.2.1-alpha.</li>
</ul>
</li>
<li>Minor bugfixes (tests):
<ul>
<li>Fix a memory leak in one of the bridge-distribution test cases. Fixes bug <a href="https://bugs.torproject.org/24345">24345</a>; bugfix on 0.3.2.3-alpha.</li>
<li>Fix a bug in our fuzzing mock replacement for crypto_pk_checksig(), to correctly handle cases where a caller gives it an RSA key of under 160 bits. (This is not actually a bug in Tor itself, but rather in our fuzzing code.) Fixes bug <a href="https://bugs.torproject.org/24247">24247</a>; bugfix on 0.3.0.3-alpha. Found by OSS-Fuzz as issue 4177.</li>
</ul>
</li>
<li>Documentation:
<ul>
<li>Add notes in man page regarding OS support for the various scheduler types. Attempt to use less jargon in the scheduler section. Closes ticket <a href="https://bugs.torproject.org/24254">24254</a>.</li>
</ul>
</li>
</ul>

