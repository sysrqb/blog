title: Torbutton 1.4.0 Released
---
pub_date: 2011-07-09
---
author: phobos
---
tags:

torbutton
anonymity
firefox extensions
firefox plugins
firefox privacy
---
categories: applications
---
_html_body:

<p>Torbutton 1.4.0 has been released at:<br />
<a href="https://www.torproject.org/torbutton/" rel="nofollow">https://www.torproject.org/torbutton/</a></p>

<p>The addon has been disabled on addons.mozilla.org. Our URL is now<br />
canonical.</p>

<p>This release features support for Firefox 5.0, and has been tested<br />
against the vanilla release for basic functionality. However, it has<br />
not been audited for Network Isolation, State Separation, Tor<br />
Undiscoverability or Interoperability issues[1] due to toggling under<br />
Firefox 5. </p>

<p>If you desire Torbutton functionality with Firefox 4/5, we recommend<br />
you download the Tor Browser Bundle 2.2.x alphas from<br />
<a href="https://www.torproject.org/dist/torbrowser/" rel="nofollow">https://www.torproject.org/dist/torbrowser/</a> or run Torbutton in its<br />
own separate Firefox profile.</p>

<p>The reasons for this shift are explained here:<br />
<a href="https://blog.torproject.org/blog/toggle-or-not-toggle-end-torbutton" rel="nofollow">https://blog.torproject.org/blog/toggle-or-not-toggle-end-torbutton</a></p>

<p>If you find bugs specific to Firefox 5, toggling, and/or extension<br />
conflicts, file them under the component "Torbutton":<br />
<a href="https://trac.torproject.org/projects/tor/report/14" rel="nofollow">https://trac.torproject.org/projects/tor/report/14</a></p>

<p>Bugs that still apply to Tor Browser should be filed under component<br />
"TorBrowserButton":<br />
<a href="https://trac.torproject.org/projects/tor/report/39" rel="nofollow">https://trac.torproject.org/projects/tor/report/39</a></p>

<p>Bugs in the "Torbutton" component currently have no maintainer<br />
available to fix them. Feel free to step up.</p>

<p>(No, simply mis-filing your Torbutton toggle bugs under<br />
TorBrowserButton won't cause them to get fixed accidentally. It will<br />
just annoy me slightly as I relocate them to the correct component).</p>

<p>Here is the complete changelog:<br />
 * bug 3101: Disable WebGL. Too many unknowns for now.<br />
 * bug 3345: Make Google Captcha redirect work again.<br />
 * bug 3399: Fix a reversed exception check found by arno.<br />
 * bug 3177: Update torbutton to use new TorBrowser prefs.<br />
 * bug 2843: Update proxy preferences window to support env var.<br />
 * bug 2338: Force toggle at startup if tor is enabled<br />
 * bug 3554: Make Cookie protections obey disk settings<br />
 * bug 3441: Enable cookie protection UI by default.<br />
 * bug 3446: We're Firefox 5.0, we swear.<br />
 * bug 3506: Remove window resize event listener.<br />
 * bug 1282: Set fixed window size for each new window.<br />
 * bug 3508: Apply Stanford SafeCache patch (thanks Edward, Collin et al).<br />
 * bug 2361: Make about window work again on FF4+.<br />
 * bug 3436: T(A)ILS was renamed to Tails.<br />
 * bugfix: Fix a transparent context menu issue on Linux FF4+.<br />
 * misc: Squelch exception from app launcher in error console.<br />
 * misc: Make DuckDuckGo the default Google Captcha redirect destination.<br />
 * misc: Make it harder to accidentally toggle torbutton.</p>

<p>1. <a href="https://www.torproject.org/torbutton/en/design/#requirements" rel="nofollow">https://www.torproject.org/torbutton/en/design/#requirements</a></p>

---
_comments:

<a id="comment-10470"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-10470" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">August 09, 2011</p>
    </div>
    <a href="#comment-10470">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-10470" class="permalink" rel="bookmark">I just installed Tor button</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I just installed Tor button 1.40 and I notice that it doesn't toggle on and off with a single click anymore.  Is it possible to make it do that like the previous versions did?</p>
</div>
  </div>
</article>
<!-- Comment END -->
