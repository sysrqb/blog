title: New stable release: Tor 0.4.6.6
---
pub_date: 2021-06-30
---
author: nickm
---
tags: stable release
---
categories: releases
---
_html_body:

<p>We have a new stable release today. If you build Tor from source, you can download the source code for 0.4.6.6 on the <a href="https://www.torproject.org/download/tor/">download page</a>.</p>
<p>Tor 0.4.6.6 makes several small fixes on 0.4.6.5, including one that allows Tor to build correctly on older versions of GCC. You should upgrade to this version if you were having trouble building Tor 0.4.6.5; otherwise, there is probably no need.</p>
<h2>Changes in version 0.4.6.6 - 2021-06-30</h2>
<ul>
<li>Minor bugfixes (compilation):
<ul>
<li>Fix a compilation error when trying to build Tor with a compiler that does not support const variables in static initializers. Fixes bug <a href="https://bugs.torproject.org/tpo/core/tor/40410">40410</a>; bugfix on 0.4.6.5.</li>
<li>Suppress a strict-prototype warning when building with some versions of NSS. Fixes bug <a href="https://bugs.torproject.org/tpo/core/tor/40409">40409</a>; bugfix on 0.3.5.1-alpha.</li>
</ul>
</li>
<li>Minor bugfixes (testing):
<ul>
<li>Enable the deterministic RNG for unit tests that covers the address set bloomfilter-based API's. Fixes bug <a href="https://bugs.torproject.org/tpo/core/tor/40419">40419</a>; bugfix on 0.3.3.2-alpha.</li>
</ul>
</li>
</ul>

---
_comments:

<a id="comment-292201"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-292201" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">July 08, 2021</p>
    </div>
    <a href="#comment-292201">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-292201" class="permalink" rel="bookmark">This version has still not…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>This version has still not appeared as the new package on deb.torproject.org (nor 0.4.6.5), despite it being over a weeks since 0.4.6 was released.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-292337"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-292337" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">July 14, 2021</p>
    </div>
    <a href="#comment-292337">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-292337" class="permalink" rel="bookmark">I found out that on unstable…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I found out that on unstable wi-fi connections (packet drops every minute or so) little-t-tor is almost useless :(. Seems that current algorithm thinks the network is broken and extends the timers to over 60 s, so that tor just waits for something instead of trying to get the maximum from that interval.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-292348"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-292348" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">July 16, 2021</p>
    </div>
    <a href="#comment-292348">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-292348" class="permalink" rel="bookmark">&quot;Obsolete&quot; versions (0.3.5.7…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>"Obsolete" versions (0.3.5.7 and 0.3.5.8) are being run by 108 (1.22%) of 8882 relays and bridges that currently have the "Valid" flag. Version 0.3.5.7 is 920 days old. Version 0.3.5.8 is 875 days old. No major Linux distributions are distributing those versions.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-292568"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-292568" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>AGKaine (not verified)</span> said:</p>
      <p class="date-time">August 12, 2021</p>
    </div>
    <a href="#comment-292568">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-292568" class="permalink" rel="bookmark">Many relays are run on…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Many relays are run on raspberry pi, the lack of 32bit support for Raspbian version of Tor is a problem for some people.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-292685"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-292685" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">August 25, 2021</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-292568" class="permalink" rel="bookmark">Many relays are run on…</a> by <span>AGKaine (not verified)</span></p>
    <a href="#comment-292685">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-292685" class="permalink" rel="bookmark">Tell Raspbian&#039;s package…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Tell Raspbian's package maintainers, or try a different GNU/Linux distribution, or build tor from source. (BTW, Raspbian is no longer the official distro. Raspberry Pi OS is.)</p>
<p><a href="http://raspbian.raspberrypi.org/raspbian/pool/main/t/tor/" rel="nofollow">http://raspbian.raspberrypi.org/raspbian/pool/main/t/tor/</a><br />
<a href="https://archive.raspbian.org/raspbian/pool/main/t/tor/" rel="nofollow">https://archive.raspbian.org/raspbian/pool/main/t/tor/</a><br />
<a href="https://ubuntu.com/raspberry-pi" rel="nofollow">https://ubuntu.com/raspberry-pi</a><br />
<a href="https://wiki.debian.org/RaspberryPiImages" rel="nofollow">https://wiki.debian.org/RaspberryPiImages</a></p>
</div>
  </div>
</article>
<!-- Comment END -->
</div>
