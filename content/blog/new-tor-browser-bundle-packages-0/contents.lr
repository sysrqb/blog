title: New Tor Browser Bundle packages
---
pub_date: 2010-12-15
---
author: erinn
---
tags:

tor browser bundle
bug fixes
polipo
firefox updates
tbb
https everywhere
---
categories:

applications
partners
releases
---
_html_body:

<p><strong>Linux Bundles</strong></p>

<p><strong>Important:</strong> Polipo has been removed from the Linux Tor Browser Bundle. Please read the full changelog and <a href="https://trac.torproject.org" rel="nofollow">report bugs</a> if you have any problems.</p>

<p><strong>1.1.0:  Released 2010-12-13</strong></p>

<ul>
<li>Update Firefox to 3.6.13</li>
<li>Update NoScript to 2.0.7</li>
<li>Update HTTPS Everywhere to 0.9.9.development.1
<ul>
<li>This version of HTTPS-Everywhere is patched to include a fix for bug #2096 which<br />
          prevented globally installed versions of the extension from working. It also<br />
          includes better protection from Firesheep. See the changelog here:<br />
          <a href="https://www.eff.org/files/Changelog.txt" rel="nofollow">https://www.eff.org/files/Changelog.txt</a></li>
</ul>
</li>
<li>Add Chris Davis's patch
<ul>
<li>This patch improves Firefox's SOCKS support and eliminates the need for Polipo, so<br />
          Torbutton has been reconfigured to just use a SOCKS proxy. Polipo has been removed<br />
          entirely from this release of the Tor Browser Bundle. If this causes you problems,<br />
          please file a bug: <a href="https://trac.torproject.org/projects/tor" rel="nofollow">https://trac.torproject.org/projects/tor</a></li>
</ul>
</li>
<li>Rebuild all binaries against glibc 2.7 so they work for older distros again</li>
</ul>

<p><strong>OS X bundle</strong></p>

<p><strong>1.0.7: Released 2010-12-14</strong></p>

<ul>
<li>Update Firefox to 3.6.13
</li>
<li>Update NoScript to 2.0.7
</li>
<li>Update HTTPS-Everywhere to 0.9.9.development.1
<ul>
<li>This version of HTTPS-Everywhere is patched to include a fix for bug #2096 which<br />
         prevented globally installed versions of the extension from working. It also<br />
         includes better protection from Firesheep. See the changelog here:<br />
         <a href="https://www.eff.org/files/Changelog.txt" rel="nofollow">https://www.eff.org/files/Changelog.txt</a></li>
</ul>
</li>
</ul>

<p><strong>Windows bundles</strong></p>

<p><strong>1.3.14: Released 2010-12-13</strong></p>

<ul>
<li>Update Firefox to 3.6.13</li>
<li>Update HTTPS-Everywhere to 0.9.9.development.1
<ul>
<li>This version of HTTPS-Everywhere is patched to include a fix for bug #2096 which<br />
    prevented globally installed versions of the extensions from working. It also<br />
    includes better protection from Firesheep. See the changelog here:<br />
    <a href="https://www.eff.org/files/Changelog.txt" rel="nofollow">https://www.eff.org/files/Changelog.txt</a></li>
</ul>
</li>
</ul>

