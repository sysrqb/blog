title: Snowflake moving to stable in Tor Browser 10.5
---
pub_date: 2021-06-08
---
author: cohosh
---
summary: We're excited to announce that Snowflake will be shipped as one of the default bridge options with stable versions of Tor Browser later this month. Snowflake is a pluggable transport that uses a combination of domain fronting and peer-to-peer WebRTC connections between clients and volunteers to circumvent Internet censorship.
---
_html_body:

<p>We're excited to announce that Snowflake will be shipped as one of the default bridge options with stable versions of Tor Browser later this month.</p>
<p> </p>
<p><img alt="Snowflake in Tor Browser 10.5" src="/static/images/blog/inline-images/snowflake-stable-blog.png" class="align-center" /></p>
<p> </p>
<h3>What is Snowflake?</h3>
<p>Snowflake is a pluggable transport that uses a combination of domain fronting and peer-to-peer WebRTC connections between clients and volunteers to circumvent Internet censorship. Snowflake, which is the spiritual successor to <a href="https://www.bamsoftware.com/papers/flashproxy.pdf">flashproxy</a>, aims to lower the barrier for running anti-censorship proxies, resulting in a large pool of proxies for users to connect to. Instead of requiring a server with consistent up-time, Snowflake proxies run as an addon or extension in your browser. These proxies can move locations as users connect to different networks, providing a moving target that is more difficult to block. We currently have about 8 thousand available Snowflake proxies each day. When a user connects to Snowflake in order to circumvent censorship, they are matched with a currently available proxy. If this proxy "melts," or disappears, the user will be seamlessly matched with a new proxy.</p>
<p>Snowflake currently uses <a href="https://www.icir.org/vern/papers/meek-PETS-2015.pdf">domain fronting</a> for the initial connection to match users with Snowflake proxies, and to allow each peer to exchange the connection information necessary for WebRTC. This channel is highly censorship resistant, and used only for the initial bootstrapping of the connection. As such, it requires much lower bandwidth and shorter connections than existing domain fronting pluggable transports like <a href="https://gitweb.torproject.org/pluggable-transports/meek.git/">meek</a>, making it a more scalable alternative.</p>
<p>For more technical details on how Snowflake is designed, see our <a href="https://gitlab.torproject.org/tpo/anti-censorship/pluggable-transports/snowflake/-/wikis/home">documentation</a>, the <a href="https://gitlab.torproject.org/tpo/anti-censorship/pluggable-transports/snowflake">source code</a>, and the Snowflake <a href="https://keroserene.net/snowflake/technical/">technical report</a>.</p>
<h3>Snowflake user survey</h3>
<p>Beginning in March 2021, we launched a social media campaign to recruit more alpha testers of Snowflake as a client in order to stress test the system, identify any unreported bugs, and compare Snowflake’s latency between different geographical locations. To provide Tor Browser Alpha users with a structured way to feed back and aid our reporting, the campaign was accompanied by an anonymous 14 question survey – from which we’d like to share a few highlights:</p>
<p><img alt="" src="/static/images/blog/inline-images/snowflake-survey-figs-4-and-7.png" /></p>
<p><em>Left: Do you use Snowflake as a Pluggable Transport?, Right: Snowflake users by platform</em></p>
<p>The response to the survey was amazing, and we’re incredibly thankful to all participants for taking the time to reply. Of the 726 participants who confirmed they use Snowflake as a pluggable transport, mobile was by far the platform of choice – with desktop coming in a distant second.</p>
<p><img alt="" src="/static/images/blog/inline-images/snowflake-survey-fig-5.png" /></p>
<p><em>Snowflake users by given country, including the number of additional participants who responded by region instead</em></p>
<p>243 confirmed Snowflake users volunteered their location, with the five most popular locations being the United States (46), Russia (26), India (16), Germany (13) and Mainland China (9). However, it must be noted that anglophone locations and those with fewer instances of censorship may be over-represented due to the nature of the study.</p>
<p><img alt="" src="/static/images/blog/inline-images/snowflake-survey-fig-12.png" /></p>
<p><em>Perception of Snowflake’s bootstrapping vs browsing speeds, relative to not using Snowflake</em></p>
<p>While 69% of Snowflake users reported some degree of increased waiting times during bootstrapping, only 30% would be discouraged from using Snowflake again. Similarly, although 65% of users reported slower speeds than they would normally experience, only 25% found the speed completely prohibitive.</p>
<p><img alt="" src="/static/images/blog/inline-images/snowflake-survey-figs-9-and-10.png" /></p>
<p><em>Left: Proportion of users who experienced issues, Right: Proportion of users who recommend Snowflake</em></p>
<p>For some questions, freeform text responses have been quantified using simple string-matching methods. Despite the full responses not being made public to protect the privacy of our participants, they have been gratefully received and read by the team. Overall, 75% of users held a positive view of Snowflake, despite some having experienced connection troubles and slow speeds while browsing</p>
<p>If you'd like to read the report in its entirety, you can <a href="https://gitlab.torproject.org/tpo/ux/research/-/raw/master/reports/2021/public-snowflake-survey-report.pdf?inline=false">download it in PDF format here</a>.</p>
<h3>How to volunteer</h3>
<p>Snowflake works because of our many volunteers! There are many ways to help users circumvent censorship with Snowflake.</p>
<h5>Run a Snowflake proxy</h5>
<p>Snowflake works because of the thousands of volunteers that run Snowflake proxies. These proxies are intentionally very lightweight and easy to set up! You do not need a dedicated server and can run a proxy by simply installing an extension in your browser. The extension is available for <a href="https://addons.mozilla.org/en-US/firefox/addon/torproject-snowflake/">Firefox</a> and <a href="https://chrome.google.com/webstore/detail/snowflake/mafpmfcccpbjnhfhjnllmmalhifmlcie">Chrome</a>. <strong>There is no need to worry about which websites people are accessing through your proxy.</strong> Their visible browsing IP address will match their Tor exit node, not yours.</p>
<h5>Contribute to Snowflake development</h5>
<p>Snowflake is an open source project and we welcome new contributors! You can file a bug report by <a href="https://gitlab.torproject.org/tpo/anti-censorship/pluggable-transports/snowflake/-/issues">opening a new issue</a> directly on the Tor Project GitLab instance (if you are a new user, you'll need to <a href="https://gitlab.onionize.space/">request an account</a>). You can also <a href="https://anonticket.onionize.space/">submit issues anonymously</a>.</p>
<p>To get started developing Snowflake, take a look at the <a href="https://gitlab.torproject.org/tpo/anti-censorship/pluggable-transports/snowflake">source code</a>, and open issues in our <a href="https://gitlab.torproject.org/tpo/anti-censorship/pluggable-transports/snowflake/-/issues">issue tracker</a>.</p>
<h5>Conduct research</h5>
<p>We post safely anonymized Snowflake metrics publicly online. If you're interested in doing research on how to improve snowflake, take a look at our <a href="https://metrics.torproject.org/collector.html#snowflake-stats">collected metrics</a> and feel free to reach out to our team on IRC.</p>
<p>See also: <a href="https://snowflake.torproject.org">https://snowflake.torproject.org/</a></p>

