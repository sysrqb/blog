title: New Tor Browser Bundles with Firefox 6
---
pub_date: 2011-08-21
---
author: erinn
---
tags:

tor browser bundle
security fixes
tor browser
firefox updates
firefox 6
package updates
---
categories:

applications
releases
---
_html_body:

<p>We've updated the experimental Tor Browser Bundles to Firefox 6 and all users are <strong>strongly</strong> encouraged to upgrade, as Firefox 6 fixes some serious security issues present in Firefox 5.</p>

<ul>
<li><a href="https://archive.torproject.org/tor-package-archive/torbrowser/tor-browser-2.2.31-1-alpha_en-US.exe" rel="nofollow">Windows Tor Browser Bundle with Firefox 6</a> (<a href="https://www.torproject.org/dist/torbrowser/tor-browser-2.2.31-1-alpha_en-US.exe.asc" rel="nofollow">sig</a>)</li>
<li><a href="https://archive.torproject.org/tor-package-archive/torbrowser/osx/TorBrowser-2.2.31-1-alpha-osx-x86_64-en-US.zip" rel="nofollow">64-bit OS X Tor Browser Bundle with Firefox 6</a> (<a href="https://archive.torproject.org/tor-package-archive//torbrowser/osx/TorBrowser-2.2.31-1-alpha-osx-x86_64-en-US.zip.asc" rel="nofollow">sig</a>)</li>
<li><a href="https://archive.torproject.org/tor-package-archive//torbrowser/osx/TorBrowser-2.2.31-1-alpha-osx-i386-en-US.zip" rel="nofollow">32-bit OS X Tor Browser Bundle with Firefox 6</a> (<a href="https://archive.torproject.org/tor-package-archive//torbrowser/osx/TorBrowser-2.2.31-1-alpha-osx-i386-en-US.zip.asc" rel="nofollow">sig</a>)</li>
<li><a href="https://archive.torproject.org/tor-package-archive/torbrowser/linux/tor-browser-gnu-linux-i686-2.2.31-1-alpha-en-US.tar.gz" rel="nofollow">32-bit GNU/Linux Tor Browser Bundle with Firefox 6</a> (<a href="https://archive.torproject.org/tor-package-archive/torbrowser/linux/tor-browser-gnu-linux-i686-2.2.31-1-alpha-en-US.tar.gz.asc" rel="nofollow">sig</a>)</li>
<li><a href="https://archive.torproject.org/tor-package-archive/torbrowser/linux/tor-browser-gnu-linux-x86_64-2.2.31-2-alpha-en-US.tar.gz" rel="nofollow">64-bit GNU/Linux Tor Browser Bundle with Firefox 6</a> (<a href="https://archive.torproject.org/tor-package-archive/torbrowser/linux/tor-browser-gnu-linux-x86_64-2.2.31-2-alpha-en-US.tar.gz.asc" rel="nofollow">sig</a>)</li>
</ul>

<p>The 64bit GNU/Linux version of the bundle is now available again.</p>

<p><strong>Tor Browser Bundle (2.2.31-1) alpha; suite=all</strong></p>

<ul>
<li>Update Tor to 0.2.2.31-rc</li>
<li>Update Firefox to 6.0</li>
<li>Update Libevent to 2.0.13-stable</li>
<li>Update NoScript to 2.1.2.6</li>
<li>Update HTTPS Everywhere to 1.0.0development.5</li>
<li>Remove BetterPrivacy until we can figure out how to make it safe in all bundles (see #3597)</li>
</ul>

