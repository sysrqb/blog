title: Experimental OS X Drag and Drop Vidalia Bundle Installer
---
pub_date: 2009-01-13
---
author: phobos
---
tags:

vidalia bundle
apple os x
alpha release
experimental
polipo
---
categories:

applications
releases
---
_html_body:

<p>I asked for community feedback in <a href="https://blog.torproject.org/blog/os-x-vidalia-bundle-thoughts" rel="nofollow">this post</a> about drag and drop installation of the Vidalia bundle for Apple's OS X.  In working with the Vidalia team, we now have a drag and drop installer.  This is <strong>experimental</strong>.  It's designed for a clean install.  It won't migrate your settings, nor will it configure anything for you.  Upon installing, your milk may sour and your salt may run off with your pepper.  Now that the disclaimers are over, here's what it contains and does do for you.</p>

<p>It includes Universal binaries for:
</p>

<ul>
<li>Vidalia version 0.2.0-svn r3425</li>
<li>Polipo 1.0.4 configured to use Tor as a socksproxy</li>
<li>Tor 0.2.1.10-alpha compiled with prefix and bindir set to /Applications/Vidalia.app</li>
</ul>

<p>You can <a href="http://interloper.org/tmp/vidalia/vidalia-bundle-0.2.1.11-alpha-0.2.0-svn-r3467-universal.dmg" rel="nofollow">download them</a> along with <a href="http://interloper.org/tmp/vidalia/vidalia-bundle-0.2.1.11-alpha-0.2.0-svn-r3467-universal.dmg.asc" rel="nofollow">the pgp signature</a> and <a href="http://interloper.org/tmp/vidalia/vidalia-bundle-0.2.1.11-alpha-0.2.0-svn-r3467-universal.dmg.sha1" rel="nofollow">the SHA-1 hash</a>. <strong>**Update 2009-01-22 changed packages to r3467 of Vidalia and updated Tor to 0.2.1.11-alpha.</strong></p>

<p>It's a self-contained disk image that has 3 items.  The Vidalia.app package, a README.txt, and a folder full of licenses.  There is a pretty background for the disk image courtesy of dr|z3d, and a link to the /Applications folder.  And while everything is in a simple dmg file, this is not a Tor Browser Bundle for OS X.  Running the applications out of the dmg may work, but OS X writes to plist files, caches, and other things all over the installed system.  Please wait until we can properly create a TBB for OS X; for this is not it.  However, if you are interested in helping out with a TBB for OS X, we're happy to have you help.</p>

<p>Installation is literally this: "Drag the Vidalia icon to the Applications folder".  Boom.  Done.  Vidalia, Tor, and Polipo are installed.  Go to your Applications folder and double-click the Vidalia icon.  Everything should work out of the box.  All bets are off if you have a pre-configured Tor, Vidalia, Polipo, or Privoxy.  </p>

<p>Here is a running <a href="http://trac.vidalia-project.net/browser/vidalia/trunk/CHANGELOG" rel="nofollow">CHANGELOG</a> of what's new in Vidalia 0.2.0-svn.  One of the  changes is that your Vidalia data directory is no longer ~/.vidalia/ but ~/Library/Vidalia.  If you don't have a vidalia.conf in ~/Library/Vidalia, there is a sample vidalia.conf in /Applications/Vidalia.app/Contents/Resources that is copied to ~/Library/Vidalia to make this bundle work.  </p>

<p>The changes to Polipo are <a href="https://svn.torproject.org/svn/tor/trunk/contrib/polipo/" rel="nofollow">documented</a>.  See the <a href="https://svn.torproject.org/svn/tor/trunk/contrib/polipo/README" rel="nofollow">README</a> file for some details.</p>

<p>The only change to Tor are the parameters passed to configure:<br />
"CFLAGS="-O -g -mmacosx-version-min=10.4 -isysroot /Developer/SDKs/MacOSX10.4u.sdk -arch i386 -arch ppc" LDFLAGS="-Wl,-syslibroot,/Developer/SDKs/MacOSX10.4u.sdk"<br />
CONFDIR=/Applications/Vidalia.app<br />
./configure --prefix=/Applications/Vidalia.app --bindir=/Applications/Vidalia.app<br />
--sysconfdir=/Library --disable-dependency-tracking".  And edit the Makefile to remove the tests for /Library/Tor.</p>

<p>The goal is to make it easier for users to install the Vidalia Bundle and get Tor working.  Using Vidalia to configure Tor is recommended.  I don't know if this is the final direction, but enough people have trouble installing and configuring our packages in OS X, this is worth a test.</p>

<p>I look forward to your feedback.  Thanks!</p>

---
_comments:

<a id="comment-519"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-519" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">January 13, 2009</p>
    </div>
    <a href="#comment-519">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-519" class="permalink" rel="bookmark">It works!</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Hi!</p>
<p>Thank for "OS X"-style distribution of V+T. As for me (Leopard 10.5.6) it works. When dragged and ran, Vidalia could not find tor, it works if change path in Vidalia specifically to /Applications/Vidalia.app/tor</p>
<p>Vidalia 2 svn has some glitches not allowing it to draw its window if clicked on its icon in Dock - in menubar I can manage Vidalia through menubar items, but there is now window of Vidalia itself ))</p>
<p>It would be nice if Vidalia will contain some run string for polipo and with options as proxy. =)</p>
<p>But it works! It is the most important I think...</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-526"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-526" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  phobos
  </article>
    <div class="comment-header">
      <p class="comment__submitted">phobos said:</p>
      <p class="date-time">January 14, 2009</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-519" class="permalink" rel="bookmark">It works!</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-526">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-526" class="permalink" rel="bookmark">Vidalia &amp; Polipo</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>There should be run strings for polipo in Vidalia, in the "General" pane of the Vidalia Settings, you should the ability to start Polipo, with an option to list the path to the polipo binary.  Under that is the ability to specify options, such as "-c /Applications/Vidalia.app/config"</p>
<p>And yes, there appears to be an issue where Vidalia doesn't respond to a single click on the menubar.  If you right click it twice, it should pop open.  We're still trying to figure out if this is really Qt or Vidalia.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-520"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-520" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">January 13, 2009</p>
    </div>
    <a href="#comment-520">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-520" class="permalink" rel="bookmark">First I want to say I would</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>First I want to say I would love to have a drag and drop installer.</p>
<p>Now, I tested the "Experimental OS X Drag and Drop Vidalia Bundle Installer" and it did not work for me...at first. The path for starting tor on Vidalia.app launch was set to "tor". Changing this path to "/Applications/Vidalia.app/tor" made the app work. Also I had to add the path "/Applications/Vidalia.app/polipo" to start proxy option.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-525"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-525" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  phobos
  </article>
    <div class="comment-header">
      <p class="comment__submitted">phobos said:</p>
      <p class="date-time">January 14, 2009</p>
    </div>
    <a href="#comment-525">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-525" class="permalink" rel="bookmark">Interesting</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>The entire point of the sample vidalia.conf was to avoid the issues with the path in Vidalia.  </p>
<p>Just for confirmation, you do have a vidalia.conf in /Applications/Vidalia.app/Contents/Resources/ right?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-535"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-535" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">January 15, 2009</p>
    </div>
              <p class="parent visually-hidden">In reply to phobos</p>
    <a href="#comment-535">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-535" class="permalink" rel="bookmark">yep. 256 bytes size</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>yep. 256 bytes size</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-531"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-531" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">January 14, 2009</p>
    </div>
    <a href="#comment-531">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-531" class="permalink" rel="bookmark">Yes Vidalia conf does</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Yes Vidalia conf does exist.</p>
<p>Here are some pics of the problems.</p>
<p>First run...<br />
<a href="http://i41.tinypic.com/2h69k3t.png" rel="nofollow">http://i41.tinypic.com/2h69k3t.png</a></p>
<p>Vidalia settings<br />
<a href="http://i42.tinypic.com/641ceb.png" rel="nofollow">http://i42.tinypic.com/641ceb.png</a></p>
<p>Vidalia conf in /Applications/Vidalia.app/Contents/Resources<br />
<a href="http://i42.tinypic.com/jj2qac.png" rel="nofollow">http://i42.tinypic.com/jj2qac.png</a></p>
<p>This is exactly how everything looks on first install without modifying any settings.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-543"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-543" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">January 20, 2009</p>
    </div>
    <a href="#comment-543">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-543" class="permalink" rel="bookmark">Polipo?</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Why Polipo instead of Privoxy? Any advantages of one over the other?</p>
<p>TIA</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-544"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-544" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">January 20, 2009</p>
    </div>
    <a href="#comment-544">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-544" class="permalink" rel="bookmark">Other settings?</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I looked up Polipo and can see the advantage. I had been using an install that I pieced together or the latest versions of tor, privoxy, and squid(man). Uninstalled all of that. Then installed this Vidalia package. </p>
<p>Got it running fine but when I go to check.torproject.org it says I am not using Tor!</p>
<p>What do I need to do in my Network Settings? Http and Https as 127.0.0.1 or localhost and port 8118 or something else?</p>
<p>How are tor and Polipo chained? Which is first? Are the issues of DNS leaks avoided?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-545"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-545" class="contextual-region comment js-comment" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  arma
  </article>
    <div class="comment-header">
      <p class="comment__submitted">arma said:</p>
      <p class="date-time">January 20, 2009</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-544" class="permalink" rel="bookmark">Other settings?</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-545">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-545" class="permalink" rel="bookmark">Re: Other settings?</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Depends where your polipo is listening.</p>
<p>I use polipo with its default port of 8123, so in my Torbutton preferences I<br />
selected "Use custom proxy settings" and put in localhost 8123 for http and<br />
https proxies, and left localhost 9050 for socks.</p>
<p>You're right that when we switch the packages from including privoxy to<br />
including polipo, we'll want to make sure that the Torbutton that's included<br />
knows what port it should tell Firefox to proxy to.</p>
<p>The easy answer is to configure the polipo we ship so that it listens on port<br />
8118. The only downside is that it will conflict if you have Privoxy installed too.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-546"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-546" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  phobos
  </article>
    <div class="comment-header">
      <p class="comment__submitted">phobos said:</p>
      <p class="date-time">January 20, 2009</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-544" class="permalink" rel="bookmark">Other settings?</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-546">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-546" class="permalink" rel="bookmark">for this package...</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>polipo is set to 8118, just like privoxy used to be.  This lets torbutton for firefox work without modification.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-547"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-547" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  phobos
  </article>
    <div class="comment-header">
      <p class="comment__submitted">phobos said:</p>
      <p class="date-time">January 20, 2009</p>
    </div>
    <a href="#comment-547">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-547" class="permalink" rel="bookmark">path issues</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I suspect people having problems with the new vidalia config using the wrong directory is because you already have a vidalia.conf in ~/Library/Vidalia/.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-549"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-549" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Kaleo (not verified)</span> said:</p>
      <p class="date-time">January 21, 2009</p>
    </div>
    <a href="#comment-549">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-549" class="permalink" rel="bookmark">Bye Bye Vidalia</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I had posted twice as Anonymous.</p>
<p>Unfortunately, at least the last few versions of Vidalia have been unstable on my PowerBook G4/10.5.6. I thrashed this package and installed the latest stable Tor and Polipo. Changed these settings in Polipo's config and the setup works:</p>
<p>socksParentProxy = localhost:9050<br />
diskCacheRoot=""<br />
disableLocalInterface=true</p>
<p>censoredHeaders = from<br />
censorReferer = maybe</p>
<p>Question: Why disable the page caching? Would the setup work faster if caching was on with a limited cache size?</p>
<p>Anyone know why PolipoStatusBar has disappeared? Would be easier to set up with it.</p>
<p>Note: I downloaded Polipo from the Max OS X list of univ binaries on it's site. There was no Leopard version so I downloaded Tiger 1.0.3 and installed it. Had to move the installed files into a single folder. Otherwise good. [Just did not have the time to mess with compiling.]</p>
<p>Anyone else having trouble with Vidalia?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-553"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-553" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  phobos
  </article>
    <div class="comment-header">
      <p class="comment__submitted">phobos said:</p>
      <p class="date-time">January 22, 2009</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-549" class="permalink" rel="bookmark">Bye Bye Vidalia</a> by <span>Kaleo (not verified)</span></p>
    <a href="#comment-553">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-553" class="permalink" rel="bookmark">This is experimental</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>From the post itself, this is <strong>experimental</strong>.  I warned you it might not work, might crash, might sour your milk.  It assumed a clean install.  if you have a ~/.vidalia or ~/Library/Vidalia directory, it won't work after drag and drop.  you will have to change the paths.  </p>
<p>As for polipo, diskcache is disabled, otherwise it records every site you've visited through polipo.  This is not good for anonymity.</p>
<p>Memory cache is enabled to improve performance.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-555"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-555" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  phobos
  </article>
    <div class="comment-header">
      <p class="comment__submitted">phobos said:</p>
      <p class="date-time">January 22, 2009</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-549" class="permalink" rel="bookmark">Bye Bye Vidalia</a> by <span>Kaleo (not verified)</span></p>
    <a href="#comment-555">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-555" class="permalink" rel="bookmark">On second thought</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>...do you have these vidalia crashing problems with the stable version of vidalia?</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-551"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-551" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Kaleo (not verified)</span> said:</p>
      <p class="date-time">January 22, 2009</p>
    </div>
    <a href="#comment-551">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-551" class="permalink" rel="bookmark">Then again</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I should have mentioned that I would love nothing better than to simply drag the Vidalia package and have it "work out of the box." However, as some others had to I had to change the path, probably because I had used privoxy, etc before. Bigger headache is that Vidalia becomes unresponsive too often.</p>
<p>I heard from the developer of PolipoStatusBar that his server crashed and he has not found a new "home" for his app. Meanwhile, I came across Dolipo. It is a GUI wrapper for polipo. I installed polipo and am now using tor with Dolipo. </p>
<p>If you are having trouble with this Vidalia package, you may want to take a look at a Dolipo (menu bar based) and Tor setup. In the Dolipo preferences, you can select the Polipo config file and toward the bottom enter localhost:9050 for Socks Parent Proxy. Tweak other settings you like and you are on your way. Only issue is having to manually configure relaying, if you want to do that.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-556"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-556" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  phobos
  </article>
    <div class="comment-header">
      <p class="comment__submitted">phobos said:</p>
      <p class="date-time">January 22, 2009</p>
    </div>
    <a href="#comment-556">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-556" class="permalink" rel="bookmark">Updated packages</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>As you can read in the post, I updated these <strong>experimental</strong> packages with Tor 0.2.1.11-alpha and vidalia 0.2.0-r3467.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-689"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-689" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Dave J. (not verified)</span> said:</p>
      <p class="date-time">February 18, 2009</p>
    </div>
    <a href="#comment-689">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-689" class="permalink" rel="bookmark">Can anyone tell me why</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Can anyone tell me why Privoxy is missing from the new  Zero Install Bundle Tor version for Windows? I don't see the privoxy icon at the right left corner as I used to when I started the old Vidalia. Does this pose a privacy hazard, or is privoxy running in the background, just not visible anymore?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-706"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-706" class="contextual-region comment js-comment" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  arma
  </article>
    <div class="comment-header">
      <p class="comment__submitted">arma said:</p>
      <p class="date-time">February 19, 2009</p>
    </div>
    <a href="#comment-706">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-706" class="permalink" rel="bookmark">Re: Can anyone tell me why</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>The Tor Browser Bundle actually includes Polipo, not Privoxy. Polipo is a lighter-weight proxy that serves pretty much the same role as Privoxy. It just runs in the background, and Vidalia launches it when everything starts up.</p>
<p>(Privoxy is a little bit better than Polipo at removing dangerous web stuff, but Torbutton 1.2 does a fine job at that, so the fact that Privoxy can do some of it is moot.)</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-1585"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-1585" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Kaleo (not verified)</span> said:</p>
      <p class="date-time">June 22, 2009</p>
    </div>
    <a href="#comment-1585">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-1585" class="permalink" rel="bookmark">Now, this is weird</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I had installed the "advanced" Tor only package and added polipo with dolipo. Worked out fine as I have described above. However, this weekend I decided to give Privoxy another go.</p>
<p>Much to my surprise, with Tor + Privoxy, Safari is much faster plus has more effective ad blocking.</p>
<p>???</p>
<p>PS Read online that if one is chaining Tor, Privoxy, Squid, the correct order is Squid &gt; Privoxy &gt; Tor but if chaining with Polipo instead, the correct order is Privoxy &gt; Polipo &gt; Tor. Why is that?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-2991"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-2991" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>the8ball (not verified)</span> said:</p>
      <p class="date-time">November 02, 2009</p>
    </div>
    <a href="#comment-2991">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-2991" class="permalink" rel="bookmark">for OS X bundle, may need to point to polipo.conf</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>My bundle didn't work out of the box, until I discovered that polipo was running its default configuration, rather than the one provided by the Vidalia bundle.</p>
<p>The solution was to open Vidalia's preferences, and under "Proxy Application Arguments" in the General tab to add the following:</p>
<p><span class="geshifilter"><code class="php geshifilter-php">&nbsp;<span style="color: #339933;">-</span>c <span style="color: #339933;">/</span>Applications<span style="color: #339933;">/</span>Vidalia<span style="color: #339933;">.</span>app<span style="color: #339933;">/</span>Contents<span style="color: #339933;">/</span>Resources<span style="color: #339933;">/</span>polipo<span style="color: #339933;">.</span>conf</code></span></p>
<p>Then everything worked perfectly.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-4384"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-4384" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">February 20, 2010</p>
    </div>
    <a href="#comment-4384">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-4384" class="permalink" rel="bookmark">I like the drag and drop. </a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I like the drag and drop.  But I dragged the Firefox extension installer to my applications folder too, before I realized I just needed to run it from the intaller location.  It could be more clear that only the Vidalia app need be dragged.<br />
thanks for the great software.</p>
</div>
  </div>
</article>
<!-- Comment END -->
