title: Tor Browser 5.5a6 is released
---
pub_date: 2016-01-07
---
author: gk
---
tags:

tor browser
tbb
tbb-5.5
---
categories:

applications
releases
---
_html_body:

<p>A new alpha Tor Browser release is available for download in the <a href="https://dist.torproject.org/torbrowser/5.5a6/" rel="nofollow">5.5a6 distribution directory</a> and on the <a href="https://www.torproject.org/projects/torbrowser.html.en#downloads-alpha" rel="nofollow">alpha download page</a>.</p>

<p>This release features an important fix for a <a href="https://trac.torproject.org/projects/tor/ticket/17931" rel="nofollow">crash bug</a> in one of our patches. All users are encouraged to update immediately as this bug is probably exploitable if JavaScript is enabled. The bug was not exploitable at High security level, or on non-HTTPS websites at Medium-High security level.</p>

<p>In the past, signing Windows .exe files on a Linux machine caused verification errors on some Windows 10 systems. This should be <a href="https://trac.torproject.org/projects/tor/ticket/17870" rel="nofollow">fixed</a> by adding the intermediate certificate in the signing process now. </p>

<p>Here is the complete changelog since 5.5a5:</p>

<ul>
<li>All Platforms
<ul>
<li>Update NoScript to 2.9
     </li>
<li>Update HTTPS Everywhere to 5.1.2
     </li>
<li>Bug 17931: Tor Browser crashes in LogMessageToConsole()
     </li>
<li>Bug 17875: Discourage editing of torrc-defaults
     </li>
<li>Bug 17870: Add intermediate certificate for authenticode signing
   </li>
</ul>
</li>
</ul>

---
_comments:

<a id="comment-149141"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-149141" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">January 08, 2016</p>
    </div>
    <a href="#comment-149141">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-149141" class="permalink" rel="bookmark">Thank you so much!</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Thank you so much!</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-149181"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-149181" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">January 08, 2016</p>
    </div>
    <a href="#comment-149181">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-149181" class="permalink" rel="bookmark">During an attempt to update</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>During an attempt to update to 5.5a6, my Firewall-suite isolated the updater and flagged it as an unrecognized executable. After restarting the browser, about:tor was not present, but a changelog appeared as the start screen, however there were no change log entries shown; just a blank page. Did my TB not fully update to 5.5a6? I'm confused because about:tor indicates that I'm using 5.5a6 &amp; when I check for updates under the green onion icon, the Software Update window indicates that there are no updates available.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-149353"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-149353" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  gk
  </article>
    <div class="comment-header">
      <p class="comment__submitted">gk said:</p>
      <p class="date-time">January 09, 2016</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-149181" class="permalink" rel="bookmark">During an attempt to update</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-149353">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-149353" class="permalink" rel="bookmark">I guess you are affected by</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I guess you are affected by <a href="https://bugs.torproject.org/17917" rel="nofollow">https://bugs.torproject.org/17917</a>.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-149804"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-149804" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">January 10, 2016</p>
    </div>
              <p class="parent visually-hidden">In reply to gk</p>
    <a href="#comment-149804">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-149804" class="permalink" rel="bookmark">Hmm; Well I&#039;m concerned that</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Hmm; Well I'm concerned that my firewall/defense-suite didn't allow the updater to actually update, rather than a disabled JS preventing the changelog from appearing like ticket #17917.Is there a way for me to revert back to the previous alpha release, and then back to 5.5a6? Or find out whether #17931 was  patched? Thanks gk, I just want to be sure that I'm not opening myself up to a potential JS exploit. I have to constantly enable it because of <a href="mailto:CL0uDFL@RE" rel="nofollow">CL0uDFL@RE</a> requiring it everywhere.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-149913"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-149913" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  gk
  </article>
    <div class="comment-header">
      <p class="comment__submitted">gk said:</p>
      <p class="date-time">January 11, 2016</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-149804" class="permalink" rel="bookmark">Hmm; Well I&#039;m concerned that</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-149913">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-149913" class="permalink" rel="bookmark">If the about:tor page on the</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>If the about:tor page on the upper right corner shows "5.5a6" then you have the correct version. Reverting back is not really possible.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div></div></div><a id="comment-149957"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-149957" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">January 11, 2016</p>
    </div>
    <a href="#comment-149957">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-149957" class="permalink" rel="bookmark">Hi all, as per my</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Hi all, as per my understanding, the TOR protocol protects network traffic through the TOR network. However, what happens if the hacker is spying through the user's computer such as through a malicious program? I know that there are certain techniques such as sandboxing which may help isolate the tbb from other say malicious app? (ref #7008). is that already implemented in the latest release?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-150497"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-150497" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">January 13, 2016</p>
    </div>
    <a href="#comment-150497">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-150497" class="permalink" rel="bookmark">New attack on Tor can</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>New attack on Tor can deanonymize hidden services with surprising accuracy</p>
<p><a href="http://arstechnica.com/security/2015/07/new-attack-on-tor-can-deanonymize-hidden-services-with-surprising-accuracy/" rel="nofollow">http://arstechnica.com/security/2015/07/new-attack-on-tor-can-deanonymi…</a></p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-152239"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-152239" class="contextual-region comment js-comment" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  arma
  </article>
    <div class="comment-header">
      <p class="comment__submitted">arma said:</p>
      <p class="date-time">January 18, 2016</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-150497" class="permalink" rel="bookmark">New attack on Tor can</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-152239">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-152239" class="permalink" rel="bookmark">https://blog.torproject.org/b</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p><a href="https://blog.torproject.org/blog/technical-summary-usenix-fingerprinting-paper" rel="nofollow">https://blog.torproject.org/blog/technical-summary-usenix-fingerprintin…</a></p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-150510"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-150510" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">January 13, 2016</p>
    </div>
    <a href="#comment-150510">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-150510" class="permalink" rel="bookmark">FBI Started De-anonymizing</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>FBI Started De-anonymizing The "Tor" Users Using Network Investigative Technique (NIT)</p>
<p><a href="http://www.haktuts.in/2016/01/de-anonymizing-tor-users-using-NIT.html" rel="nofollow">http://www.haktuts.in/2016/01/de-anonymizing-tor-users-using-NIT.html</a></p>
<p>?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-151185"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-151185" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">January 15, 2016</p>
    </div>
    <a href="#comment-151185">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-151185" class="permalink" rel="bookmark">gk, text 32/64-bit under MS</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>gk, text 32/64-bit under MS Win on the download page means combined TB bundle instead of version of OS. Please, correct.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-152158"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-152158" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  gk
  </article>
    <div class="comment-header">
      <p class="comment__submitted">gk said:</p>
      <p class="date-time">January 18, 2016</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-151185" class="permalink" rel="bookmark">gk, text 32/64-bit under MS</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-152158">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-152158" class="permalink" rel="bookmark">Not sure what you mean but</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Not sure what you mean but we ship only a 32 bit Windows Tor Browser which happens to run on 32 and 64 bit Windows systems though.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-152212"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-152212" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">January 18, 2016</p>
    </div>
              <p class="parent visually-hidden">In reply to gk</p>
    <a href="#comment-152212">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-152212" class="permalink" rel="bookmark">Exactly. But &quot;32/64&quot; means</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Exactly. But "32/64" means TBB with 32 AND 64 FF in one. See <a href="https://www.mozilla.org/en-US/firefox/all/" rel="nofollow">https://www.mozilla.org/en-US/firefox/all/</a>, where 64 means version of the product, not OS.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div></div><a id="comment-153468"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-153468" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">January 26, 2016</p>
    </div>
    <a href="#comment-153468">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-153468" class="permalink" rel="bookmark">I use this version ( 5.5.6a</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I use this version ( 5.5.6a ) and the normal version ( 5.0.7 ).<br />
After two weeks I noticed that the first ip address, after "this browser" in the onion menu ( tor circuit for this site ) changed and now is the same for both version of Tor browsers. So the normal version and the alpha one have the same ip address.</p>
<p>Is that ip address Tor Entry Guards? ( I suppose yes )<br />
Why I have that same ip on different versions of Tor Browser?<br />
Could you please take a look at this ip address if it could be suspicious?<br />
Could a suspicious Entry Guard try to get connected through a suspicious Exit Node? </p>
<p>I think I'm targeted but I not sure, I'm a "normal" user.</p>
<p>Could you tell me the best/official way to reset/change Tor Entry Guard?<br />
Could you tell me the best/official way to run multiple istances of Tor Browser?</p>
<p>The suspicious ip address is: 124.6.36.230</p>
<p>Cheers by,<br />
A Person Like You</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-153639"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-153639" class="contextual-region comment js-comment" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  arma
  </article>
    <div class="comment-header">
      <p class="comment__submitted">arma said:</p>
      <p class="date-time">January 27, 2016</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-153468" class="permalink" rel="bookmark">I use this version ( 5.5.6a</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-153639">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-153639" class="permalink" rel="bookmark">Yes, what you describe</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Yes, what you describe sounds exactly like the entry guard design.</p>
<p>For why two installs of Tor Browser ended up with the same entry guard, my bet is on 'coincidence'.<br />
That particular relay is huge:<br />
<a href="https://atlas.torproject.org/#search/124.6.36.230" rel="nofollow">https://atlas.torproject.org/#search/124.6.36.230</a><br />
so it probably has a bunch of users, including both of you.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-153659"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-153659" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">January 27, 2016</p>
    </div>
              <p class="parent visually-hidden">In reply to arma</p>
    <a href="#comment-153659">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-153659" class="permalink" rel="bookmark">Thank you for your reply.
A</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Thank you for your reply.</p>
<p>A Person Like You</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div></div>
