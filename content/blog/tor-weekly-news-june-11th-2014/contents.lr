title: Tor Weekly News — June 11th, 2014
---
pub_date: 2014-06-11
---
author: harmony
---
tags: tor weekly news
---
categories: reports
---
_html_body:

<p>Welcome to the twenty-third issue of Tor Weekly News in 2014, the <a href="https://lists.torproject.org/cgi-bin/mailman/listinfo/tor-news" rel="nofollow">weekly newsletter</a> that covers what is happening in the Tor community.</p>

<h1>Tor Browser 3.6.2 is out</h1>

<p>Version 3.6.2 of the Tor Browser has been <a href="https://blog.torproject.org/blog/tor-browser-362-released" rel="nofollow">released</a>, featuring “a fix to allow the configuration of a local HTTP or SOCKS proxy with all included Pluggable Transports”, as well as important fixes to mitigate recent OpenSSL vulnerabilities, among other security updates. All users are advised to <a href="https://www.torproject.org/download/download-easy.html" rel="nofollow">upgrade</a> as soon as possible.</p>

<h1>The EFF announces its 2014 Tor Challenge</h1>

<p>As part of the wider <a href="https://blog.torproject.org/blog/reset-net" rel="nofollow">“Reset the Net” event</a>, the Electronic Frontier Foundation has <a href="https://blog.torproject.org/blog/tor-challenge-2014" rel="nofollow">launched</a> another in its occasional series of Tor Challenges. The goal of the campaign is to increase the Tor network’s capacity and diversity by encouraging members of the public to run relays, and directing them to the legal and technical guidance necessary to do so.</p>

<p>So far, over 600 relays have been started (or had their capacity increased) as part of the campaign: you can see a running total of relays and bytes transferred on the <a href="https://www.eff.org/torchallenge/" rel="nofollow">campaign page</a>. Once you’ve set up your relay, you can register it on the page (anonymously or credited to your name); stickers and T-shirts are on offer for those who run relays of a certain size or for a certain period.</p>

<p>If you run into trouble setting up your relay, you can also find expert advice and discussion on the <a href="https://lists.torproject.org/cgi-bin/mailman/listinfo/tor-relays" rel="nofollow">tor-relays mailing list</a> or the #tor channel on irc.oftc.net.</p>

<h1>Tor and the “EarlyCCS” bug</h1>

<p>Following April’s much-loved “Heartbleed” bug, another OpenSSL vulnerability was discovered — nicknamed <a href="http://ccsinjection.lepidum.co.jp/" rel="nofollow">“EarlyCCS”</a> — that could have an impact on the security of many internet services, including Tor. Nick Mathewson <a href="https://lists.torproject.org/pipermail/tor-talk/2014-June/033161.html" rel="nofollow">explained</a> that although “Tor is comparatively resilient to having one layer of crypto removed”, it may be affected to the extent that “an adversary in the position to run a MITM attack on a Tor client or relay could cause a TLS connection to be negotiated without real encryption or authentication.”</p>

<p>Tor users and relay operators should make sure to update their OpenSSL and Tor packages as soon as possible; those using a system tor (rather than or in addition to the Tor Browser) should ensure that they restart it once the updates are installed; otherwise they will not take effect.</p>

<h1>A new website for the directory archive</h1>

<p>Karsten Loesing <a href="https://lists.torproject.org/pipermail/tor-dev/2014-June/006942.html" rel="nofollow">announced</a> the new <a href="https://collector.torproject.org/" rel="nofollow">CollecTor service</a>, which spins off the directory archive section from the <a href="https://metrics.torproject.org/" rel="nofollow">Metrics</a> portal.</p>

<p>What’s different? Archive tarballs are now provided in a <a href="https://collector.torproject.org/archive/" rel="nofollow">directory structure</a> rather than a single directory, recently published descriptors can now be accessed <a href="https://collector.torproject.org/recent/" rel="nofollow">much more easily</a>, and the <a href="https://collector.torproject.org/formats.html" rel="nofollow">documentation of descriptor formats</a> has been updated.</p>

<p>The now obsolete rsync access to metrics-archive and metrics-recent will be discontinued on August 4, 2014.</p>

<h1>More monthly status reports for May 2014</h1>

<p>The wave of regular monthly reports from Tor project members for the month of May continued, with reports from <a href="https://lists.torproject.org/pipermail/tor-reports/2014-June/000551.html" rel="nofollow">Karsten Loesing</a>, Isis Lovecruft (who submitted reports for both <a href="https://lists.torproject.org/pipermail/tor-reports/2014-June/000553.html" rel="nofollow">April</a> and <a href="https://lists.torproject.org/pipermail/tor-reports/2014-June/000552.html" rel="nofollow">May</a>), <a href="https://lists.torproject.org/pipermail/tor-reports/2014-June/000554.html" rel="nofollow">George Kadianakis</a>, <a href="https://lists.torproject.org/pipermail/tor-reports/2014-June/000556.html" rel="nofollow">Nicolas Vigier</a>, and <a href="https://lists.torproject.org/pipermail/tor-reports/2014-June/000559.html" rel="nofollow">Roger Dingledine</a>.</p>

<p>Roger also sent the report for <a href="https://lists.torproject.org/pipermail/tor-reports/2014-June/000558.html" rel="nofollow">SponsorF</a>.</p>

<h1>Miscellaneous news</h1>

<p>The Tails developers formally <a href="https://tails.boum.org/news/Join_us_at_the_Tails_HackFest_2014/" rel="nofollow">announced</a> the upcoming Tails Hackfest, inviting absolutely “anyone interested in making Tails more usable and more secure” to join them in Paris on the 5th and 6th of July (immediately after the Tor dev meeting) and “learn about the challenges faced by Tails, and how you can be part of the solution”. Fuller details of the venue and timetable can be found on the <a href="https://tails.boum.org/blueprint/HackFest_2014_Paris/" rel="nofollow">Tails website</a>.</p>

<p>Several of Tor’s Google Summer of Code students submitted their regular progress reports: Juha Nurmi on the <a href="https://lists.torproject.org/pipermail/tor-reports/2014-June/000555.html" rel="nofollow">ahmia.fi project</a>, Israel Leiva on the <a href="https://lists.torproject.org/pipermail/tor-dev/2014-June/006959.html" rel="nofollow">GetTor revamp</a>, Amogh Pradeep on the <a href="https://lists.torproject.org/pipermail/tor-dev/2014-June/006960.html" rel="nofollow">Orbot+Orfox project</a>, Quinn Jarrell on the <a href="https://lists.torproject.org/pipermail/tor-dev/2014-June/006961.html" rel="nofollow">pluggable transport combiner</a>, Marc Juarez on the <a href="https://lists.torproject.org/pipermail/tor-reports/2014-June/000557.html" rel="nofollow">link-padding pluggable transport development</a>, Noah Rahman on the <a href="https://lists.torproject.org/pipermail/tor-dev/2014-June/006962.html" rel="nofollow">Stegotorus refactoring work</a>, Sreenatha Bhatlapenumarthi on the <a href="https://lists.torproject.org/pipermail/tor-dev/2014-June/006964.html" rel="nofollow">Tor Weather rewrite</a>, Daniel Martí on the <a href="https://lists.torproject.org/pipermail/tor-dev/2014-June/006966.html" rel="nofollow">implementation of consensus diffs</a>, and Mikhail Belous on the <a href="https://lists.torproject.org/pipermail/tor-dev/2014-June/006984.html" rel="nofollow">multicore tor daemon</a>.</p>

<p>Thanks to <a href="https://lists.torproject.org/pipermail/tor-mirrors/2014-June/000612.html" rel="nofollow">moparisthebest</a> for running a mirror of the Tor Project website!</p>

<p>Roger Dingledine <a href="https://lists.torproject.org/pipermail/tor-relays/2014-June/004642.html" rel="nofollow">asked</a> the tor-relays mailing list about the situation of Mac OS X users who would like to run Tor relays, and what steps should be taken to make it easier for them to do so “now that the Vidalia bundles are deprecated and hard to find”.</p>

<p>Isis Lovecruft has deployed <a href="https://gitweb.torproject.org/bridgedb.git/blob_plain/cb8b01bc:/CHANGELOG" rel="nofollow">BridgeDB version 0.2.2</a> which contains many fixes and translation updates. The email autoresponder should not reply with empty emails any more.</p>

<p>Damian Johnson has <a href="https://lists.torproject.org/pipermail/tor-dev/2014-June/006970.html" rel="nofollow">written up</a> several ideas regarding a possible rewrite of the <a href="https://exonerator.torproject.org/" rel="nofollow">ExoneraTor service</a> in Python.</p>

<p>HTTPS is sometimes heavily throttled by censors, making it hard to download the Tor Browser over an HTTPS link. Israel Leiva is <a href="https://lists.torproject.org/pipermail/tor-dev/2014-June/006977.html" rel="nofollow">asking for feedback</a> about making the GetTor email service reply with links to unencrypted HTTP servers as a work-around.</p>

<h1>Tor help desk roundup</h1>

<p>The help desk has been asked for information on TorCoin, a proposed cryptocurrency. TorCoin is not affiliated with or endorsed by the Tor Project. The Tor Project publishes <a href="https://www.torproject.org/docs/trademark-faq.html.en" rel="nofollow">guidelines on the use of its trademark</a> to try to prevent confusing uses of the Tor name.</p>

<h1>Easy development tasks to get involved with</h1>

<p>obfsproxy, the traffic obfuscator, opens the “authcookie” file for each new incoming connection. George Kadianakis suggests that it should instead <a href="https://bugs.torproject.org/9822" rel="nofollow">read the file on startup and keep its content in memory during operation</a>. obfsproxy is written in Python/Twisted. The change should be pretty small, but if you like finding the right places that need changing, feel free to look at the ticket and post your patch there.</p>

<p>This issue of Tor Weekly News has been assembled by Lunar, harmony, Matt Pagan, Karsten Loesing, and Roger Dingledine.</p>

<p>Want to continue reading TWN? Please help us create this newsletter. We still need more volunteers to watch the Tor community and report important news. Please see the <a href="https://trac.torproject.org/projects/tor/wiki/TorWeeklyNews" rel="nofollow">project page</a>, write down your name and subscribe to the <a href="https://lists.torproject.org/cgi-bin/mailman/listinfo/news-team" rel="nofollow">team mailing list</a> if you want to get involved!</p>

