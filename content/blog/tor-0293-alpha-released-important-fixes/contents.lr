title: Tor 0.2.9.3-alpha is released, with important fixes
---
pub_date: 2016-09-23
---
author: nickm
---
tags:

bug fixes
release
source
0.2.9.2-alpha
---
categories: releases
---
_html_body:

<p>Tor 0.2.9.3-alpha adds improved support for entities that want to make high-performance services available through the Tor .onion mechanism without themselves receiving anonymity as they host those services. It also tries harder to ensure that all steps on a circuit are using the strongest crypto possible, strengthens some TLS properties, and resolves several bugs -- including a pair of crash bugs from the 0.2.8 series. Anybody running an earlier version of 0.2.9.x should upgrade.<br />
You can download the source from the usual place on the website. Packages should be available over the next several days. Remember to check the signatures!<br />
Please note: This is an alpha release. You should only try this one if you are interested in tracking Tor development, testing new features, making sure that Tor still builds on unusual platforms, or generally trying to hunt down bugs. If you want a stable experience, please stick to the stable releases.<br />
Below are the changes since 0.2.9.2-alpha.</p>

<h2>Changes in version 0.2.9.3-alpha - 2016-09-23</h2>

<ul>
<li>Major bugfixes (crash, also in 0.2.8.8):
<ul>
<li>Fix a complicated crash bug that could affect Tor clients configured to use bridges when replacing a networkstatus consensus in which one of their bridges was mentioned. OpenBSD users saw more crashes here, but all platforms were potentially affected. Fixes bug <a href="https://bugs.torproject.org/20103" rel="nofollow">20103</a>; bugfix on 0.2.8.2-alpha.
  </li>
</ul>
</li>
<li>Major bugfixes (relay, OOM handler, also in 0.2.8.8):
<ul>
<li>Fix a timing-dependent assertion failure that could occur when we tried to flush from a circuit after having freed its cells because of an out-of-memory condition. Fixes bug <a href="https://bugs.torproject.org/20203" rel="nofollow">20203</a>; bugfix on 0.2.8.1-alpha. Thanks to "cypherpunks" for help diagnosing this one.
  </li>
</ul>
</li>
</ul>

<p> </p>

<ul>
<li>Major features (circuit building, security):
<ul>
<li>Authorities, relays and clients now require ntor keys in all descriptors, for all hops (except for rare hidden service protocol cases), for all circuits, and for all other roles. Part of ticket <a href="https://bugs.torproject.org/19163" rel="nofollow">19163</a>.
  </li>
<li>Tor authorities, relays, and clients only use ntor, except for rare cases in the hidden service protocol. Part of ticket <a href="https://bugs.torproject.org/19163" rel="nofollow">19163</a>.
  </li>
</ul>
</li>
<li>Major features (single-hop "hidden" services):
<ul>
<li>Add experimental HiddenServiceSingleHopMode and HiddenServiceNonAnonymousMode options. When both are set to 1, every hidden service on a Tor instance becomes a non-anonymous Single Onion Service. Single Onions make one-hop (direct) connections to their introduction and renzedvous points. One-hop circuits make Single Onion servers easily locatable, but clients remain location-anonymous. This is compatible with the existing hidden service implementation, and works on the current tor network without any changes to older relays or clients. Implements proposal 260, completes ticket <a href="https://bugs.torproject.org/17178" rel="nofollow">17178</a>. Patch by teor and asn.
  </li>
</ul>
</li>
<li>Major features (resource management):
<ul>
<li>Tor can now notice it is about to run out of sockets, and preemptively close connections of lower priority. (This feature is off by default for now, since the current prioritizing method is yet not mature enough. You can enable it by setting "DisableOOSCheck 0", but watch out: it might close some sockets you would rather have it keep.) Closes ticket <a href="https://bugs.torproject.org/18640" rel="nofollow">18640</a>.
  </li>
</ul>
</li>
<li>Major bugfixes (circuit building):
<ul>
<li>Hidden service client-to-intro-point and service-to-rendezvous- point cicruits use the TAP key supplied by the protocol, to avoid epistemic attacks. Fixes bug <a href="https://bugs.torproject.org/19163" rel="nofollow">19163</a>; bugfix on 0.2.4.18-rc.
  </li>
</ul>
</li>
<li>Major bugfixes (compilation, OpenBSD):
<ul>
<li>Fix a Libevent-detection bug in our autoconf script that would prevent Tor from linking successfully on OpenBSD. Patch from rubiate. Fixes bug <a href="https://bugs.torproject.org/19902" rel="nofollow">19902</a>; bugfix on 0.2.9.1-alpha.
  </li>
</ul>
</li>
<li>Major bugfixes (hidden services):
<ul>
<li>Clients now require hidden services to include the TAP keys for their intro points in the hidden service descriptor. This prevents an inadvertent upgrade to ntor, which a malicious hidden service could use to distinguish clients by consensus version. Fixes bug <a href="https://bugs.torproject.org/20012" rel="nofollow">20012</a>; bugfix on 0.2.4.8-alpha. Patch by teor.
  </li>
</ul>
</li>
<li>Minor features (security, TLS):
<ul>
<li>Servers no longer support clients that without AES ciphersuites. (3DES is no longer considered an acceptable cipher.) We believe that no such Tor clients currently exist, since Tor has required OpenSSL 0.9.7 or later since 2009. Closes ticket <a href="https://bugs.torproject.org/19998" rel="nofollow">19998</a>.
  </li>
</ul>
</li>
<li>Minor feature (fallback directories):
<ul>
<li>Remove broken entries from the hard-coded fallback directory list. Closes ticket <a href="https://bugs.torproject.org/20190" rel="nofollow">20190</a>; patch by teor.
  </li>
</ul>
</li>
<li>Minor features (geoip, also in 0.2.8.8):
<ul>
<li>Update geoip and geoip6 to the September 6 2016 Maxmind GeoLite2 Country database.
  </li>
</ul>
</li>
<li>Minor feature (port flags):
<ul>
<li>Add new flags to the *Port options to finer control over which requests are allowed. The flags are NoDNSRequest, NoOnionTraffic, and the synthetic flag OnionTrafficOnly, which is equivalent to NoDNSRequest, NoIPv4Traffic, and NoIPv6Traffic. Closes enhancement 18693; patch by "teor".
  </li>
</ul>
</li>
<li>Minor features (directory authority):
<ul>
<li>After voting, if the authorities decide that a relay is not "Valid", they no longer include it in the consensus at all. Closes ticket <a href="https://bugs.torproject.org/20002" rel="nofollow">20002</a>; implements part of proposal 272.
  </li>
</ul>
</li>
<li>Minor features (testing):
<ul>
<li>Disable memory protections on OpenBSD when performing our unit tests for memwipe(). The test deliberately invokes undefined behavior, and the OpenBSD protections interfere with this. Patch from "rubiate". Closes ticket <a href="https://bugs.torproject.org/20066" rel="nofollow">20066</a>.
  </li>
</ul>
</li>
<li>Minor features (testing, ipv6):
<ul>
<li>Add the single-onion and single-onion-ipv6 chutney targets to "make test-network-all". This requires a recent chutney version with the single onion network flavours (git c72a652 or later). Closes ticket <a href="https://bugs.torproject.org/20072" rel="nofollow">20072</a>; patch by teor.
  </li>
<li>Add the hs-ipv6 chutney target to make test-network-all's IPv6 tests. Remove bridges+hs, as it's somewhat redundant. This requires a recent chutney version that supports IPv6 clients, relays, and authorities. Closes ticket <a href="https://bugs.torproject.org/20069" rel="nofollow">20069</a>; patch by teor.
  </li>
</ul>
</li>
<li>Minor features (Tor2web):
<ul>
<li>Make Tor2web clients respect ReachableAddresses. This feature was inadvertently enabled in 0.2.8.6, then removed by bugfix 19973 on 0.2.8.7. Implements feature <a href="https://bugs.torproject.org/20034" rel="nofollow">20034</a>. Patch by teor.
  </li>
</ul>
</li>
<li>Minor features (unit tests):
<ul>
<li>We've done significant work to make the unit tests run faster.
  </li>
<li>Our link-handshake unit tests now check that when invalid handshakes fail, they fail with the error messages we expected.
  </li>
<li>Our unit testing code that captures log messages no longer prevents them from being written out if the user asked for them (by passing --debug or --info or or --notice --warn to the "test" binary). This change prevents us from missing unexpected log messages simply because we were looking for others. Related to ticket <a href="https://bugs.torproject.org/19999" rel="nofollow">19999</a>.
  </li>
<li>The unit tests now log all warning messages with the "BUG" flag. Previously, they only logged errors by default. This change will help us make our testing code more correct, and make sure that we only hit this code when we mean to. In the meantime, however, there will be more warnings in the unit test logs than before. This is preparatory work for ticket <a href="https://bugs.torproject.org/19999" rel="nofollow">19999</a>.
  </li>
<li>The unit tests now treat any failure of a "tor_assert_nonfatal()" assertion as a test failure.
  </li>
</ul>
</li>
<li>Minor bug fixes (circuits):
<ul>
<li>Use the CircuitBuildTimeout option whenever LearnCircuitBuildTimeout is disabled. Previously, we would respect the option when a user disabled it, but not when it was disabled because some other option was set. Fixes bug <a href="https://bugs.torproject.org/20073" rel="nofollow">20073</a>; bugfix on 0.2.4.12-alpha. Patch by teor.
  </li>
</ul>
</li>
<li>Minor bugfixes (allocation):
<ul>
<li>Change how we allocate memory for large chunks on buffers, to avoid a (currently impossible) integer overflow, and to waste less space when allocating unusually large chunks. Fixes bug <a href="https://bugs.torproject.org/20081" rel="nofollow">20081</a>; bugfix on 0.2.0.16-alpha. Issue identified by Guido Vranken.
  </li>
<li>Always include orconfig.h before including any other C headers. Sometimes, it includes macros that affect the behavior of the standard headers. Fixes bug <a href="https://bugs.torproject.org/19767" rel="nofollow">19767</a>; bugfix on 0.2.9.1-alpha (the first version to use AC_USE_SYSTEM_EXTENSIONS).
  </li>
<li>Fix a syntax error in the IF_BUG_ONCE__() macro in non-GCC- compatible compilers. Fixes bug <a href="https://bugs.torproject.org/20141" rel="nofollow">20141</a>; bugfix on 0.2.9.1-alpha. Patch from Gisle Vanem.
  </li>
<li>Stop trying to build with Clang 4.0's -Wthread-safety warnings. They apparently require a set of annotations that we aren't currently using, and they create false positives in our pthreads wrappers. Fixes bug <a href="https://bugs.torproject.org/20110" rel="nofollow">20110</a>; bugfix on 0.2.9.1-alpha.
  </li>
</ul>
</li>
<li>Minor bugfixes (directory authority):
<ul>
<li>Die with a more useful error when the operator forgets to place the authority_signing_key file into the keys directory. This avoids an uninformative assert &amp; traceback about having an invalid key. Fixes bug <a href="https://bugs.torproject.org/20065" rel="nofollow">20065</a>; bugfix on 0.2.0.1-alpha.
  </li>
<li>When allowing private addresses, mark Exits that only exit to private locations as such. Fixes bug <a href="https://bugs.torproject.org/20064" rel="nofollow">20064</a>; bugfix on 0.2.2.9-alpha.
  </li>
</ul>
</li>
<li>Minor bugfixes (documentation):
<ul>
<li>Document the default PathsNeededToBuildCircuits value that's used by clients when the directory authorities don't set min_paths_for_circs_pct. Fixes bug <a href="https://bugs.torproject.org/20117" rel="nofollow">20117</a>; bugfix on 02c320916e02 in tor-0.2.4.10-alpha. Patch by teor, reported by Jesse V.
  </li>
<li>Fix manual for the User option: it takes a username, not a UID. Fixes bug <a href="https://bugs.torproject.org/19122" rel="nofollow">19122</a>; bugfix on 0.0.2pre16 (the first version to have a manpage!).
  </li>
</ul>
</li>
<li>Minor bugfixes (hidden services):
<ul>
<li>Stop logging intro point details to the client log on certain error conditions. Fixed as part of bug <a href="https://bugs.torproject.org/20012" rel="nofollow">20012</a>; bugfix on 0.2.4.8-alpha. Patch by teor.
  </li>
</ul>
</li>
<li>Minor bugfixes (IPv6, testing):
<ul>
<li>Check for IPv6 correctly on Linux when running test networks. Fixes bug <a href="https://bugs.torproject.org/19905" rel="nofollow">19905</a>; bugfix on 0.2.7.3-rc; patch by teor.
  </li>
</ul>
</li>
<li>Minor bugfixes (Linux seccomp2 sandbox):
<ul>
<li>Add permission to run the sched_yield() and sigaltstack() system calls, in order to support versions of Tor compiled with asan or ubsan code that use these calls. Now "sandbox 1" and "--enable-expensive-hardening" should be compatible on more systems. Fixes bug <a href="https://bugs.torproject.org/20063" rel="nofollow">20063</a>; bugfix on 0.2.5.1-alpha.
  </li>
</ul>
</li>
<li>Minor bugfixes (logging):
<ul>
<li>When logging a message from the BUG() macro, be explicit about what we were asserting. Previously we were confusing what we were asserting with what the bug was. Fixes bug <a href="https://bugs.torproject.org/20093" rel="nofollow">20093</a>; bugfix on 0.2.9.1-alpha.
  </li>
<li>When we are unable to remove the bw_accounting file, do not warn if the reason we couldn't remove it was that it didn't exist. Fixes bug <a href="https://bugs.torproject.org/19964" rel="nofollow">19964</a>; bugfix on 0.2.5.4-alpha. Patch from 'pastly'.
  </li>
</ul>
</li>
<li>Minor bugfixes (option parsing):
<ul>
<li>Count unix sockets when counting client listeners (SOCKS, Trans, NATD, and DNS). This has no user-visible behaviour changes: these options are set once, and never read. Required for correct behaviour in ticket <a href="https://bugs.torproject.org/17178" rel="nofollow">17178</a>. Fixes bug <a href="https://bugs.torproject.org/19677" rel="nofollow">19677</a>; bugfix on 0.2.6.3-alpha. Patch by teor.
  </li>
</ul>
</li>
<li>Minor bugfixes (options):
<ul>
<li>Check the consistency of UseEntryGuards and EntryNodes more reliably. Fixes bug <a href="https://bugs.torproject.org/20074" rel="nofollow">20074</a>; bugfix on tor- 0.2.4.12-alpha. Patch by teor.
  </li>
<li>Stop changing the configured value of UseEntryGuards on authorities and Tor2web clients. Fixes bug <a href="https://bugs.torproject.org/20074" rel="nofollow">20074</a>; bugfix on commits 51fc6799 in tor-0.1.1.16-rc and acda1735 in tor-0.2.4.3- alpha. Patch by teor.
  </li>
</ul>
</li>
<li>Minor bugfixes (Tor2web):
<ul>
<li>Prevent Tor2web clients running hidden services, these services are not anonymous due to the one-hop client paths. Fixes bug <a href="https://bugs.torproject.org/19678" rel="nofollow">19678</a>. Patch by teor.
  </li>
</ul>
</li>
<li>Minor bugfixes (unit tests):
<ul>
<li>Fix a shared-random unit test that was failing on big endian architectures due to internal representation of a integer copied to a buffer. The test is changed to take a full 32 bytes of data and use the output of a python script that make the COMMIT and REVEAL calculation according to the spec. Fixes bug <a href="https://bugs.torproject.org/19977" rel="nofollow">19977</a>; bugfix on 0.2.9.1-alpha.
  </li>
<li>The tor_tls_server_info_callback unit test no longer crashes when debug-level logging is turned on. Fixes bug <a href="https://bugs.torproject.org/20041" rel="nofollow">20041</a>; bugfix on 0.2.8.1-alpha.
  </li>
</ul>
</li>
</ul>

---
_comments:

<a id="comment-209578"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-209578" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">September 23, 2016</p>
    </div>
    <a href="#comment-209578">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-209578" class="permalink" rel="bookmark">how do we upgrade to</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>how do we upgrade to 0.2.9.3-alpha tried but nothing found</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-210202"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-210202" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">September 26, 2016</p>
    </div>
    <a href="#comment-210202">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-210202" class="permalink" rel="bookmark">would this 0.2.9.3 solve</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>would this 0.2.9.3 solve when unable to connect to tor port using Mac? If so how to download it?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-210402"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-210402" class="contextual-region comment js-comment" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  gk
  </article>
    <div class="comment-header">
      <p class="comment__submitted">gk said:</p>
      <p class="date-time">September 27, 2016</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-210202" class="permalink" rel="bookmark">would this 0.2.9.3 solve</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-210402">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-210402" class="permalink" rel="bookmark">I don&#039;t think so. I don&#039;t</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I don't think so. I don't have much information regarding your bug but maybe <a href="https://trac.torproject.org/projects/tor/ticket/20210#comment:15" rel="nofollow">https://trac.torproject.org/projects/tor/ticket/20210#comment:15</a> does help?</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-213186"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-213186" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">October 10, 2016</p>
    </div>
    <a href="#comment-213186">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-213186" class="permalink" rel="bookmark">I get the feeling The Tor</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I get the feeling The Tor project doesn't actually give a fuck about its community...</p>
<p>Again all comments are deleted, and again, not even a sorry.<br />
Just give us a plain forum, something like Discourse, assign volunteer moderators etc.</p>
<p>By the way tor.stackexchange isn't the answer, because they're quite unfriendly towards Tor users.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-213187"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-213187" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">October 10, 2016</p>
    </div>
    <a href="#comment-213187">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-213187" class="permalink" rel="bookmark">Can&#039;t yet reply to my</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Can't yet reply to my comment, but oops, thought the comments were deleted again.<br />
Anyway, Discourse would be a better alternative to this. I think.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-218352"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-218352" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">November 12, 2016</p>
    </div>
    <a href="#comment-218352">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-218352" class="permalink" rel="bookmark">When I leave tor and come</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>When I leave tor and come back later my history is still there. Why?</p>
</div>
  </div>
</article>
<!-- Comment END -->
