title: Technology Preview:  Marble and Vidalia-0.2.0
---
pub_date: 2009-03-29
---
author: phobos
---
tags:

vidalia
experimental
kde marble
technology preview
drag and drop OS X install
Qt updates
msi installer
---
categories: applications
---
_html_body:

<p>One of the most requested upgrades to <a href="https://www.torproject.org/vidalia/" rel="nofollow">Vidalia</a> is a better map of the world.  We looked into a few different technologies and decided on <a href="http://edu.kde.org/marble/" rel="nofollow">KDE's Marble</a> interface.  Marble enables an accurate mapping of nodes according to their geolocation, allows for future enhancements such as "click a country to start or end your Tor circuit", and plugins for extra data views.  This also gives us the ability to use Qt's Webkit browser to display custom information about nodes, circuits, or anything else in a pop-up window.  An anonymous funder covered the costs involved in developing this feature.  We thank them for their support.</p>

<p>I've attached some screenshots to this blog post.  You can download the relevant packages from the <a href="https://www.torproject.org/vidalia/dist/" rel="nofollow">Vidalia website</a>.  If you want marble-enabled Vidalia, look for -marble- in the file name.  Please report bugs on <a href="http://trac.vidalia-project.net/wiki/ReportingBugs" rel="nofollow">Vidalia's Bug Tracker</a>.  </p>

<p>As of this release, there are now two branches of Vidalia, stable and alpha.  The first release of the alpha branch is <a href="https://www.torproject.org/vidalia/dist/" rel="nofollow">Vidalia 0.2.0</a>.  The stable branch is at <a href="https://www.torproject.org/vidalia/dist/" rel="nofollow">Vidalia 0.1.12</a>.</p>

<p>One last big change for OS X users is the drag and drop installer.  There is no migration of old settings to new, nor do we clean up the old installs in /Library/Tor, /Library/Vidalia, /Library/Torbutton, /Library/Privoxy, etc.  Follow <a href="https://www.torproject.org/docs/tor-doc-osx#uninstall" rel="nofollow">these instructions</a> for how to remove the old vidalia-tor bundles from your computer if you want to completely test this alpha branch of Vidalia.  We will figure out a better way to do this in the near future.</p>

<p>Remember, this is a technology preview.  However, OS X installs are migrating to drag and drop.  I've solely used Vidalia 0.2.0 and drag-and-drop installer for 3 months now.  I even <a href="http://blog.torproject.org/blog/experimental-os-x-drag-and-drop-vidalia-bundle-installer" rel="nofollow">previously blogged</a> about it.</p>

<p>Additional changes in alpha Vidalia 0.2.0:</p>

<ul>
<li>Qt 4.5.0 is used for Win32 and OS X Universal packages.</li>
<li>We've switched from the Nullsoft Installer to the Microsoft System Installer (msi) for better integration with Microsoft Windows.</li>
<li>There are non-Marble and Marble-enabled packages for Win32 and OS X Universal.  OS X PowerPC based on OS X 10.3.9 isn't supported by Qt 4.5.x, therefore Marble-enabled Vidalia is not available</li>
<li>Add support for changing UI languages without having to restart Vidalia.</li>
<li>Add preliminary support for using the KDE Marble widget for the network map. It's currently a compile-time option and is disabled by default.</li>
<li>Add support for displaying Tor's plaintext port warnings. Also gives the user the option to disable future warnings.</li>
<li>Add an interface for displaying the geographic distribution of clients who have recently used a bridge operator's relay.</li>
<li> Add tooltips to tree items in the help browser's table of contents. Some of the help topic labels are a bit long.</li>
<li>Switch to a simpler About dialog and move the license information to a separate HTML-formatted display.</li>
<li>Switch to a simpler drag-and-drop installer in the OS X bundles.</li>
<li>Switch to an MSI-based installer on Windows.</li>
<li>Clear the list of default CA certificates used by QSslSocket before adding the only one we care about. Suggested by coderman.</li>
<li>Support building with Visual Studio again.</li>
<li>Add a Debian package structure from dererk.</li>
<li>Updated Albanian, Czech, Finnish, Polish, Portuguese, Romanian, Swedish, Turkish and many other translations.</li>
</ul>

---
_comments:

<a id="comment-878"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-878" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><a rel="nofollow" href="http://rorschachstagebuch.wordpress.com">Rorschach (not verified)</a> said:</p>
      <p class="date-time">March 29, 2009</p>
    </div>
    <a href="#comment-878">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-878" class="permalink" rel="bookmark">archlinux-packages and small review of the new map</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Hi,<br />
the new map looks pretty nice! I already packaged the marble-vidalia version and the new vidalia-alpha for Archlinux. The packages can be found here:</p>
<p>marble-vidalia: <a href="http://aur.archlinux.org/packages.php?ID=25092" rel="nofollow">http://aur.archlinux.org/packages.php?ID=25092</a><br />
vidalia-alpha: <a href="http://aur.archlinux.org/packages.php?ID=25093" rel="nofollow">http://aur.archlinux.org/packages.php?ID=25093</a></p>
<p>But at the moment this version is more or less unuseable because it creates a high cpu-usage. If you take a look at the map and play a bit with it I get about 20% cpu-usage on a Core Duo @1.6 GHz.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-886"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-886" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  phobos
  </article>
    <div class="comment-header">
      <p class="comment__submitted">phobos said:</p>
      <p class="date-time">March 29, 2009</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-878" class="permalink" rel="bookmark">archlinux-packages and small review of the new map</a> by <a rel="nofollow" href="http://rorschachstagebuch.wordpress.com">Rorschach (not verified)</a></p>
    <a href="#comment-886">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-886" class="permalink" rel="bookmark">Only in X</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I see high cpu usage in Xorg/X11 sometimes.  Any other OS seems to handle it just fine.  </p>
<p>Does the usage spike to 20% or constantly stay at that level?</p>
<p>I notice the cpu usage generally while many circuits are building.  Once circuits are built, it seems pretty quick to move around.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-887"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-887" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><a rel="nofollow" href="http://rorschachstagebuch.wordpress.com">Rorschach (not verified)</a> said:</p>
      <p class="date-time">March 30, 2009</p>
    </div>
              <p class="parent visually-hidden">In reply to phobos</p>
    <a href="#comment-887">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-887" class="permalink" rel="bookmark">I checked the behavior now</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I checked the behavior now on an Intel Atom N270 @ 1.6 Ghz with two Cores and there it is even worse.</p>
<p>Starting the map causes 80% CPU-usage by the vidalia process for about 1-2 minutes. After that CPU-usage goes completly down.</p>
<p>But as soon as I start to scroll the map or zoom the cpu-usage goes up to 20-30% again.</p>
<p>So if you use the map it is pretty high, if you don't use it it's as usual.</p>
<p>Maybe this is normal for marble under X to consume so much? But it would be cool if the old map could still be further a part of vidalia, so one can choose as compile-time option if he has the rescources to run the marble map or the other one, as it is right now. Also if marble becames vidalias default map it would be cool if one could use --disable-marble to use the older one.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-888"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-888" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  phobos
  </article>
    <div class="comment-header">
      <p class="comment__submitted">phobos said:</p>
      <p class="date-time">March 30, 2009</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-887" class="permalink" rel="bookmark">I checked the behavior now</a> by <a rel="nofollow" href="http://rorschachstagebuch.wordpress.com">Rorschach (not verified)</a></p>
    <a href="#comment-888">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-888" class="permalink" rel="bookmark">marble is optional</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>if you're compiling vidalia, you have to set -DUSE_MARBLE=1 to get the marble map, otherwise you get the default 2-D map.</p>
<p>We're well aware that not everyone can handle 3-D graphics with live circuits drawn on them right now.  The largest issue appears to be marble and X.  Marble and OSX and win32 is rather quick, even on older hardware.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-889"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-889" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><a rel="nofollow" href="http://rorschachstagebuch.wordpress.com">Rorschach (not verified)</a> said:</p>
      <p class="date-time">March 30, 2009</p>
    </div>
              <p class="parent visually-hidden">In reply to phobos</p>
    <a href="#comment-889">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-889" class="permalink" rel="bookmark">&gt; if you&#039;re compiling</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>&gt; if you're compiling vidalia, you have to set -DUSE_MARBLE=1 to get the marble &gt; map, otherwise you get the default 2-D map.</p>
<p>I know , I compiled and used it already ;) I just wanted to tell you my expereniece with it and that I think also if this feature becames part of vidalia stable, that there should still be the possibility to  switch it off...</p>
<p>But interesting that Marble, a KDE project, runs smoother on MacOS and MS'OS. Maybe some problem with graphiccards and 3D acceleration, so that X does it in software and thus consuming so much cpu?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-7224"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-7224" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">August 15, 2010</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-889" class="permalink" rel="bookmark">&gt; if you&#039;re compiling</a> by <a rel="nofollow" href="http://rorschachstagebuch.wordpress.com">Rorschach (not verified)</a></p>
    <a href="#comment-7224">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-7224" class="permalink" rel="bookmark">Hi,
The problem is that the</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Hi,</p>
<p>The problem is that the native graphics system (X11, XRender) is unfortunately very slow on some graphics cards. That's why Qt provides mechanisms to bypass the X-Server using the so called "raster" graphics system. </p>
<p>With recent Marble versions (Marble&gt;= 0.8) you can choose  a different graphics system via the settings dialog (Settings-&gt;Configure Marble-&gt;View-&gt;Graphics System: Raster).</p>
<p>This will make Marble on Linux/X11 as fast as on Windows and Mac OS X.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div></div></div></div></div><a id="comment-879"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-879" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">March 29, 2009</p>
    </div>
    <a href="#comment-879">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-879" class="permalink" rel="bookmark">I can&#039;t get .1.2 or .2 to</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I can't get .1.2 or .2 to work on OSX. Crashes on start. Bummer. I'll try to submit bug report later.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-880"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-880" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">March 29, 2009</p>
    </div>
    <a href="#comment-880">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-880" class="permalink" rel="bookmark">I can&#039;t get .1.2 or .2 to</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Actually there error message is quite small so I'm going to post it here and maybe someone will spot it. Also doing this as I may forget to post a bug report.</p>
<p>Exception Type:  EXC_BREAKPOINT (SIGTRAP)<br />
Exception Codes: 0x0000000000000002, 0x0000000000000000<br />
Crashed Thread:  0</p>
<p>Dyld Error Message:<br />
  Library not loaded: QtGui.framework/Versions/4/QtGui<br />
  Referenced from: /Volumes/vidalia-0.1.12-universal/Vidalia.app/Contents/MacOS/Vidalia<br />
  Reason: image not found</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-885"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-885" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  phobos
  </article>
    <div class="comment-header">
      <p class="comment__submitted">phobos said:</p>
      <p class="date-time">March 29, 2009</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-880" class="permalink" rel="bookmark">I can&#039;t get .1.2 or .2 to</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-885">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-885" class="permalink" rel="bookmark">Interesting</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>This is OS X 10.5.x?  And yes, please submit a bug. Thanks!</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-883"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-883" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">March 29, 2009</p>
    </div>
    <a href="#comment-883">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-883" class="permalink" rel="bookmark">Compilation from source</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Wait, how do I compile Vidalia from source (e.g. for Linux / BSD) with marble support? I don't see a plain .tar.gz with marble in it.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-884"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-884" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  phobos
  </article>
    <div class="comment-header">
      <p class="comment__submitted">phobos said:</p>
      <p class="date-time">March 29, 2009</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-883" class="permalink" rel="bookmark">Compilation from source</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-884">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-884" class="permalink" rel="bookmark">Good point</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>The marble that comes with your distro will probably work fine, but if you want the exact same version we used, it's in Vidalia's SVN.  <a href="http://trac.vidalia-project.net/wiki/GettingTheCode" rel="nofollow">http://trac.vidalia-project.net/wiki/GettingTheCode</a> or <a href="https://svn.vidalia-project.net/svn/marble/trunk/" rel="nofollow">https://svn.vidalia-project.net/svn/marble/trunk/</a> specifically.</p>
<p>In the vidalia directory, read README.marble for instructions.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-1465"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-1465" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span content="swisstorexit.dyndns.org">swisstorexit.d… (not verified)</span> said:</p>
      <p class="date-time">June 02, 2009</p>
    </div>
    <a href="#comment-1465">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-1465" class="permalink" rel="bookmark">cannot install on jaunty 64 bits</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Hello,</p>
<p>i have test some possibility and read all page about this version but are impossible to intsall it...</p>
<p>i have a version vidalia 0.2.0 make for jaunty from launchpad and work fine  but are not compiled with marble.</p>
<p>marble are on my system and work the only problem are with the source are not possible for me to build a compatible version for ubuntu jaunty 9.04 64 bits ... (intel)</p>
<p>i will really happy when someone can help me with that ;)</p>
<p>thanks in advance</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-1756"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-1756" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>swisstorexit (not verified)</span> said:</p>
      <p class="date-time">July 06, 2009</p>
    </div>
    <a href="#comment-1756">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-1756" class="permalink" rel="bookmark">vidalia 0.2.1 - marble work now on my jaunty 64</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>hello, </p>
<p>i write this post to say with the last source from vidalia 0.2.1 i can build it without error and with marble.</p>
<p>That's really a great design and option are useful :)</p>
<p>thanks for all</p>
</div>
  </div>
</article>
<!-- Comment END -->
