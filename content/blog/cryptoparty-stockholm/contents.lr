title: CryptoParty Stockholm
---
pub_date: 2013-03-05
---
author: phobos
---
tags:

stockholm
sweden
cryptoparty
kids
cryptography
fun times
---
categories: community
---
_html_body:

<p>I attended the <a href="https://cryptoparty.org/wiki/Stockholm" rel="nofollow">Stockholm Cryptoparty</a> on Saturday the 16th of February. I was asked to give the opening talk, "Varför krypto?", to start off the day. My goal was to explain why cryptography should be used daily by everyone in mundane ways. The general topic was about how I watch kids using cryptography daily, without knowing it or without fully understanding the technical details behind it. This is ok. Kids chat a lot. When you introduce <a href="http://www.cypherpunks.ca/otr/" rel="nofollow">Off-the-record</a> to their chats, they instantly understand that the chats are now private, and can be authenticated. The distinction between the two concepts is fairly easy to grasp, even if they don't understand the details of hashes, key exchanges, or ciphers. Once a few core people start using OTR, for example, then it spreads to their friends and soon you have networks of kids using OTR having safe and secure chats.<br />
The simplest three steps people can take to begin using cryptography daily are:</p>

<ol>
<li>Use <a href="https://www.eff.org/https-everywhere" rel="nofollow">https everywhere</a> in your browser.</li>
<li>Use a browser password manager. <a href="http://keepass.info/" rel="nofollow">KeePass</a> is as good as any. The point is to keep username/passwords unique and complex per site/service. The next time LinkedIn or some major site loses tens of millions of passwords, you're protected because it's not the same username and password you used for your gmail, facebook, twitter, banking, and vkontact accounts.</li>
<li>Use <a href="https://www.torproject.org" rel="nofollow">Tor</a> for actions you want to keep private. Everything on the Internet leaves a trace. The world knows <a href="https://en.wikipedia.org/wiki/On_the_Internet,_nobody_knows_you%27re_a_dog" rel="nofollow">you're a dog</a> online.</li>
</ol>

<p>Thankfully, I could give the introduction in English and not have to offend the attendees with my poor Swedish. Linus gave a great <a href="https://www.youtube.com/watch?v=gnkazegk89M&amp;list=UUfVxt2PKh1ANj1YlLfqJynQ&amp;index=20" rel="nofollow">Tor talk in Swedish</a>. Overall, the day went well. We had <a href="https://media.torproject.org/video/2013-02-16-cryptoparty-stockholm/PizzaLewmanNordberg3.JPG" rel="nofollow">huge pizzas</a> and generally a great time. Many people were new to cryptoparties and new to cryptography in general. It was a great time. As an American, it was nice to see about 50% women attending. There were a number of younger kids learning about all of this too. The cryptoparties I've attended in the USA have been all men and the maybe one girlfriend or wife dragged to the event.<br />
(Unfortunately, the camera recording my talk malfunctioned and corrupted the video. However, other images and videos from the day are <a href="https://media.torproject.org/video/2013-02-16-cryptoparty-stockholm/" rel="nofollow">available on our media server</a>.<br />
Thanks to <a href="https://dfri.se/" rel="nofollow">DFRI</a>, <a href="http://www.sparvnastet.org/" rel="nofollow">Sparvnästet</a>, and <a href="https://www.iis.se/" rel="nofollow">iis.se</a> for hosting the event and inviting me to attend.</p>

---
_comments:

<a id="comment-19036"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-19036" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">March 06, 2013</p>
    </div>
    <a href="#comment-19036">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-19036" class="permalink" rel="bookmark">How does Firefox Sync (with</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>How does Firefox Sync (with a master password) compare to Keepass, security-wise?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-19039"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-19039" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">March 06, 2013</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-19036" class="permalink" rel="bookmark">How does Firefox Sync (with</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-19039">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-19039" class="permalink" rel="bookmark">i think keepass keeps</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>i think keepass keeps passwords on your computer, whearas firefox sync keeps them in a server which is out of your hand.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-19049"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-19049" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">March 06, 2013</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-19039" class="permalink" rel="bookmark">i think keepass keeps</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-19049">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-19049" class="permalink" rel="bookmark">You can set up your own</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>You can set up your own Firefox Sync server if you want:<br />
<a href="http://docs.services.mozilla.com/howtos/run-sync.html" rel="nofollow">http://docs.services.mozilla.com/howtos/run-sync.html</a></p>
<p>The source is open, and everything is encrypted client-side. </p>
<p>I have never seen any security review of it though. What's got me more concerned is the password store in Firefox itself, is the encryption (when using a master password) "good enough"?</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-19046"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-19046" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">March 06, 2013</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-19036" class="permalink" rel="bookmark">How does Firefox Sync (with</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-19046">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-19046" class="permalink" rel="bookmark">have you read firefox sync</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>have you read firefox sync tos [1] or privacy policy [2]?</p>
<p>[1] <a href="https://services.mozilla.com/tos/" rel="nofollow">https://services.mozilla.com/tos/</a><br />
[2] <a href="https://services.mozilla.com/privacy-policy/" rel="nofollow">https://services.mozilla.com/privacy-policy/</a></p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-19066"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-19066" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">March 09, 2013</p>
    </div>
    <a href="#comment-19066">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-19066" class="permalink" rel="bookmark">Password Maker is better</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Password Maker is better than password stores:</p>
<p><a href="http://passwordmaker.org/" rel="nofollow">http://passwordmaker.org/</a></p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-19277"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-19277" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">March 26, 2013</p>
    </div>
    <a href="#comment-19277">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-19277" class="permalink" rel="bookmark">Wow that was strange. I just</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Wow that was strange. I just wrote an extremely long comment but after I clicked submit my comment didn?t appear. Grrrr? well I?m not writing all that over again. Anyways, just wanted to say excellent blog!</p>
</div>
  </div>
</article>
<!-- Comment END -->
