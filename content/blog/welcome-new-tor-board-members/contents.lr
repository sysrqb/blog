title: Updates on Tor Project’s Board
---
pub_date: 2020-09-16
---
author: isabela
---
tags:

board of directors
press release
---
summary: Today, we're welcoming two new members to our Board of Directors: Chelsea Komlo and Rabbi Rob. We are happy to say both have accepted our invitation and joined the Tor Project's Board.
---
_html_body:

<p>We would like to share some updates regarding the Tor Project’s Board. Last year Megan Price stepped down as she took a second maternity leave. And in the Spring of this year, Shari Steele asked to step down from the Board for personal reasons. Both Megan and Shari provided great contributions for the Board that Tor will always be thankful for. We are grateful to have them as supporters and friends of Tor.</p>
<p>But to move forward we decided to invite two new members. We are happy to say both have accepted our invitation and joined the Board.</p>
<p>Rabbi Rob, the founder and CEO of Team Cymru and who has been on Tor’s Board before, from 2011 to 2016. Rabbi Rob is part of our <a href="https://www.torproject.org/about/people/">core contributors community</a> and is a <a href="https://blog.torproject.org/double-your-donation-rabbi-rob-and-lauren-thomas-announce-matching-challenge-supporttor">great supporter of Tor</a>. Rabbi Rob says that, <em>"The Internet remains a top target for tyrants. Tor remains the best solution for those who seek online freedom and an escape from tyranny. It is a great honor to be a part of this important mission."</em></p>
<p>And Chelsea Komlo, cryptography and privacy researcher and engineer, is also part of our core contributors community and serves as a member of the <a href="https://research.torproject.org/safetyboard/">Tor Research Safety Board</a>. She has experience working on technical teams around the world on both enterprise and open-source projects, a role we are looking forward to having on our Board.</p>
<p>"Tor is in the unique position to provide critical internet privacy infrastructure for millions of people around the world. I am honored to be able to support Tor in this way", Chelsea Komlo.</p>
<p>The other seven members of the <a href="https://www.torproject.org/about/people/#board">Tor Project’s Board</a> are: Bruce Schneier, Cindy Cohn, Gabriella Coleman, Julius Mittenzwei, Matt Blaze, Nighat Dad and Ramy Raoof.</p>
<p><strong>Biographies of Incoming Board Members</strong></p>
<p><strong>Chelsea Komlo</strong> is a cryptography and privacy researcher and engineer, and has been a technical contributor to Tor since 2016. She most recently was a collaborator on "Walking Onions," a design that enables the Tor network to meaningfully scale while maintaining low overhead for users. Chelsea also serves as a member of the Tor Research Safety Board.</p>
<p>Chelsea has nearly a decade of engineering experience and is a lead author on several cryptographic designs ranging from multi-party signature schemes to post-quantum primitives with applications to secure messaging. She has experience working on technical teams around the world on both enterprise and open-source projects. She currently is completing her Ph.D at the University of Waterloo in the Cryptography, Security, and Privacy Lab, and is a Principal Technical Advisor and Researcher for the Zcash Foundation.</p>
<p><strong>Rabbi Rob Thomas</strong> is the founder and CEO of Team Cymru, and a member of the early generation of network defenders. He has worked at IBM, Sun, and Cisco, among others. During his career, Rabbi Rob has been a Unix kernel developer, ISP backbone engineer, security architect, and an adjunct professor. He was also the first individual member of FIRST (the Forum of Incident Response and Security Teams).</p>
<p>Rabbi Rob took his first C programming class at age 12, and has been addicted to technology ever since. He and Team Cymru are long-time fans and supporters of the Tor Project. They provide infrastructure and services to the project, with a particular interest in enabling dissidents around the world to operate without being tracked or hampered by oppressive regimes. Rabbi Rob and Team Cymru are rabidly devoted to delivering community services that make the Internet and world a safer place. They are proud to support the great work of the Tor Project and the freedoms its technology enables.</p>

---
_comments:

<a id="comment-289476"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-289476" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>publius (not verified)</span> said:</p>
      <p class="date-time">September 16, 2020</p>
    </div>
    <a href="#comment-289476">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-289476" class="permalink" rel="bookmark">I&#039;m sorry to see Shari go…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I'm sorry to see Shari go. Mazel tov for Megan! Welcome to Chelsea and Rabbi Rob. Also, thank you Team Cymru for being one of the founding members of the new membership program!</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-289478"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-289478" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Usually Quiet (not verified)</span> said:</p>
      <p class="date-time">September 17, 2020</p>
    </div>
    <a href="#comment-289478">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-289478" class="permalink" rel="bookmark">Congratulations.</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Congratulations.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-289497"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-289497" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Lazyman (not verified)</span> said:</p>
      <p class="date-time">September 19, 2020</p>
    </div>
    <a href="#comment-289497">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-289497" class="permalink" rel="bookmark">Good Luck for all!!!</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Good Luck for all!!!</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-289738"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-289738" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>M0thm3n (not verified)</span> said:</p>
      <p class="date-time">September 30, 2020</p>
    </div>
    <a href="#comment-289738">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-289738" class="permalink" rel="bookmark">Congratulations.
Very…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Congratulations.<br />
Very interesting.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-289739"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-289739" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Mahesh (not verified)</span> said:</p>
      <p class="date-time">September 30, 2020</p>
    </div>
    <a href="#comment-289739">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-289739" class="permalink" rel="bookmark">Thank you so much for…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Thank you so much for project me</p>
</div>
  </div>
</article>
<!-- Comment END -->
