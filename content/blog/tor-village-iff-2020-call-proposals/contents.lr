title: Tor Village at IFF: Call for Proposals
---
pub_date: 2020-02-19
---
author: ggus
---
tags:

cfp
internet freedom festival
community 
---
summary: **Update 3/3/2020: IFF has been cancelled this year.
---
_html_body:

<p><a href="https://internetfreedomfestival.org/wiki/index.php/Cancellation_of_the_2020_Internet_Freedom_Festival"><strong><em>**Update 3/3/2020: IFF has been cancelled this year.</em></strong></a></p>
<p>During this year's <a href="https://internetfreedomfestival.org/">Internet Freedom Festival (IFF)</a>, we're organizing a village with activities on privacy, anonymity, and anti-censorship based around Tor. IFF will take place from April 20 - 24 2020 in Valencia, Spain, and the Tor Village will take place on the last two days, April 23 and 24. </p>
<p>Proposals can be made in English or Spanish. <strong>Proposals should be sent by March 10th at 23:59 UTC to <a href="mailto:iff@torproject.org">iff@torproject.org</a>. </strong>Proposals sent after the deadline or by other means will not be accepted.</p>
<p>The activities will be approved by the Tor Project Community Team, and the participants will be notified by March 20. The first version of the schedule will be published on April 2nd.</p>
<p>With the exception of artistic activities, the activities must be carried out in under 1 hour between April 23 and 24. The session's facilitators of the Tor Village will have free ticket to the event. Unfortunately, in this first edition of the Village, the Tor Project cannot assist participants with travel expenses or other costs.</p>
<h2>Activity Ideas</h2>
<p>Here are several ideas we're interested for you to explore, but do not feel limited to these:</p>
<ul>
<li>Sharing experience and challenges about Tor workshops and anonymity</li>
<li>Automation and deployment of onion services</li>
<li>Forming your Tor relay association</li>
<li>How to convince your organization or your university to run an exit node</li>
<li>Presentation of research results on users of Tor ecosystem tools</li>
<li>Analysis of results on the usability of the Tor Navigator</li>
<li>Art installation on how the Tor network works</li>
<li>Localization of Tor based tools</li>
<li>Good practices for journalists when using Tails</li>
<li>Application containers using onion services</li>
<li>Presentation of personas study on Tor users</li>
</ul>
<h2>Proposal Format</h2>
<p>When you email us, be sure to include the following:</p>
<p>Proposal title:</p>
<p>Abstract (max 100 words):</p>
<p>Proposal description (max 500 words):</p>
<p>Time (max 50 minutes):</p>
<p>Availability:<br />
[ ] Only day 1<br />
[ ] Only day 2<br />
[ ] Both days</p>
<p>Type of activity:<br />
[ ] Workshop<br />
[ ] Talk<br />
[ ] Discussion<br />
[ ] Other: ________</p>
<p>Speaker name(s) / Organization name(s):</p>
<p>Tech rider: what equipment do you need for your presentation?</p>
<p>Language:</p>
<p>[ ] English<br />
[ ] Spanish</p>
<ul>
</ul>
<h2>Questions?</h2>
<p>In case of questions or concerns, contact us by email: <a href="mailto:iff@torproject.org">iff@torproject.org</a> or ask us during our weekly Community Team meetings: every Monday @ 1600 UTC in #tor-meeting - irc.oftc.net. <a href="https://support.torproject.org/get-in-touch/irc-help/">You can also chat with us on IRC</a>.</p>
<p><a href="https://internetfreedomfestival.org/wiki/index.php/Cancellation_of_the_2020_Internet_Freedom_Festival"><strong><em>**Update 3/3/2020: IFF has been cancelled this year.</em></strong></a></p>

