title: New releases: Tor 0.3.5.11, 0.4.2.8, and 0.4.3.6 (with security fixes)
---
pub_date: 2020-07-09
---
author: nickm
---
tags: security advisory
---
_html_body:

<p>We have new stable releases today. If you build Tor from source, you can download the source code for 0.4.3.6 <a href="https://www.torproject.org/download/tor/">on the website</a>. Packages should be available within the next several weeks, with a new Tor Browser by the end of the month.</p>
<p>There are also updated versions for older supported series. You can download 0.3.5.11 and 0.4.2.8 at <a href="https://dist.torproject.org/">https://dist.torproject.org/</a>.</p>
<p>These releases fix TROVE-2020-001, a medium-severity denial of service vulnerability affecting all versions of Tor when compiled with the NSS encryption library. (This is not the default configuration.) Using this vulnerability, an attacker could cause an affected Tor instance to crash remotely. This issue is also tracked as CVE-2020- 15572. Anybody running a version of Tor built with the NSS library should upgrade to 0.3.5.11, 0.4.2.8, 0.4.3.6, or 0.4.4.2-alpha or later. (If you are running a version of Tor built with OpenSSL, this bug does not affect your installation.)</p>
<p>Tor 0.4.3.6 backports several bugfixes from later releases, including some affecting usability. Below are the changes in 0.4.3.6.  You can also read the <a href="https://gitweb.torproject.org/tor.git/tree/ChangeLog?h=tor-0.3.5.11">changes in 0.3.5.11</a> and the <a href="https://gitweb.torproject.org/tor.git/tree/ChangeLog?h=tor-0.4.2.8">changes in 0.4.2.8</a>.</p>
<h2>Changes in version 0.4.3.6 - 2020-07-09</h2>
<ul>
<li>Major bugfixes (NSS, security, backport from 0.4.4.2-alpha):
<ul>
<li>Fix a crash due to an out-of-bound memory access when Tor is compiled with NSS support. Fixes bug <a href="https://bugs.torproject.org/33119">33119</a>; bugfix on 0.3.5.1-alpha. This issue is also tracked as TROVE-2020-001 and CVE-2020-15572.</li>
</ul>
</li>
<li>Minor bugfix (CI, Windows, backport from 0.4.4.2-alpha):
<ul>
<li>Use the correct 64-bit printf format when compiling with MINGW on Appveyor. Fixes bug <a href="https://bugs.torproject.org/40026">40026</a>; bugfix on 0.3.5.5-alpha.</li>
</ul>
</li>
</ul>
<p> </p>
<!--break--><ul>
<li>Minor bugfixes (client performance, backport from 0.4.4.1-alpha):
<ul>
<li>Resume use of preemptively-built circuits when UseEntryGuards is set to 0. We accidentally disabled this feature with that config setting, leading to slower load times. Fixes bug <a href="https://bugs.torproject.org/34303">34303</a>; bugfix on 0.3.3.2-alpha.</li>
</ul>
</li>
<li>Minor bugfixes (compiler warnings, backport from 0.4.4.2-alpha):
<ul>
<li>Fix a compiler warning on platforms with 32-bit time_t values. Fixes bug <a href="https://bugs.torproject.org/40028">40028</a>; bugfix on 0.3.2.8-rc.</li>
</ul>
</li>
<li>Minor bugfixes (linux seccomp sandbox, nss, backport from 0.4.4.1-alpha):
<ul>
<li>Fix a startup crash when tor is compiled with --enable-nss and sandbox support is enabled. Fixes bug <a href="https://bugs.torproject.org/34130">34130</a>; bugfix on 0.3.5.1-alpha. Patch by Daniel Pinto.</li>
</ul>
</li>
<li>Minor bugfixes (logging, backport from 0.4.4.2-alpha):
<ul>
<li>Downgrade a noisy log message that could occur naturally when receiving an extrainfo document that we no longer want. Fixes bug <a href="https://bugs.torproject.org/16016">16016</a>; bugfix on 0.2.6.3-alpha.</li>
</ul>
</li>
<li>Minor bugfixes (manual page, backport from 0.4.4.1-alpha):
<ul>
<li>Update the man page to reflect that MinUptimeHidServDirectoryV2 defaults to 96 hours. Fixes bug <a href="https://bugs.torproject.org/34299">34299</a>; bugfix on 0.2.6.3-alpha.</li>
</ul>
</li>
<li>Minor bugfixes (onion service v3, backport from 0.4.4.1-alpha):
<ul>
<li>Prevent an assert() that would occur when cleaning the client descriptor cache, and attempting to close circuits for a non- decrypted descriptor (lacking client authorization). Fixes bug <a href="https://bugs.torproject.org/33458">33458</a>; bugfix on 0.4.2.1-alpha.</li>
</ul>
</li>
<li>Minor bugfixes (portability, backport from 0.4.4.1-alpha):
<ul>
<li>Fix a portability error in the configure script, where we were using "==" instead of "=". Fixes bug <a href="https://bugs.torproject.org/34233">34233</a>; bugfix on 0.4.3.5.</li>
</ul>
</li>
<li>Minor bugfixes (relays, backport from 0.4.4.1-alpha):
<ul>
<li>Stop advertising incorrect IPv6 ORPorts in relay and bridge descriptors, when the IPv6 port was configured as "auto". Fixes bug <a href="https://bugs.torproject.org/32588">32588</a>; bugfix on 0.2.3.9-alpha.</li>
</ul>
</li>
<li>Documentation (backport from 0.4.4.1-alpha):
<ul>
<li>Fix several doxygen warnings related to imbalanced groups. Closes ticket <a href="https://bugs.torproject.org/34255">34255</a>.</li>
</ul>
</li>
</ul>

---
_comments:

<a id="comment-288685"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-288685" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">July 13, 2020</p>
    </div>
    <a href="#comment-288685">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-288685" class="permalink" rel="bookmark">Minor bugfix (CI, Windows,…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Minor bugfix (CI, Windows, backport from 0.4.4.2-alpha):<br />
    Use the correct 64-bit printf format when compiling with MINGW on Appveyor. Fixes bug 40026; bugfix on 0.3.5.5-alpha.<br />
<a href="https://bugs.torproject.org/40026" rel="nofollow">https://bugs.torproject.org/40026</a><br />
<a href="https://gitlab.torproject.org/legacy/trac/-/issues/40026" rel="nofollow">https://gitlab.torproject.org/legacy/trac/-/issues/40026</a><br />
 Page Not Found</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-288717"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-288717" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  nickm
  </article>
    <div class="comment-header">
      <p class="comment__submitted">nickm said:</p>
      <p class="date-time">July 16, 2020</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-288685" class="permalink" rel="bookmark">Minor bugfix (CI, Windows,…</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-288717">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-288717" class="permalink" rel="bookmark">Whoops. I&#039;m going to have to…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Whoops. I'm going to have to update the script that makes those links.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div>
