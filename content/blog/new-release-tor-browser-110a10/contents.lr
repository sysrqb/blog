title: New Release: Tor Browser 11.0a10 (Windows/macOS/Linux)
---
pub_date: 2021-11-05
---
author: sysrqb
---
tags:

tor browser
tbb-11.0
tbb
---
categories: applications
---
summary: Tor Browser 11.0a10 is now available from the Tor Browser download page and also from our distribution directory.
---
_html_body:

<p>Tor Browser 11.0a10 is now available from the <a href="https://www.torproject.org/download/alpha/">Tor Browser download page</a> and also from our <a href="https://www.torproject.org/dist/torbrowser/11.0a10/">distribution directory</a>.</p>
<p><b>Note:</b> This is an alpha release, an experimental version for users who want to help us test new features. For everyone else, we recommend downloading the latest stable <a href="https://blog.torproject.org/new-release-tor-browser-1058">Windows/macOS/Linux</a> release instead.</p>
<p>This version updates Firefox to version <b>91.3.0esr</b> on Windows, macOS, and Linux. This version includes important security updates to Firefox on <a href="https://www.mozilla.org/en-US/security/advisories/mfsa2021-49/">Windows, macOS, and Linux</a>.</p>
<p><b>Please <a href="https://support.torproject.org/misc/bug-or-feedback/">report</a> any issues and regressions you experience since upgrading from an earlier version based on Firefox 78esr.</b></p>
<p style="background-color:cornsilk; padding-left:1em; padding-right:1em;"><b>Warning</b>:<br />
Tor Browser Alpha does <em><b>not</b> support <u>version 2 onion services</u></em>. Tor Browser (Stable) will <em><b>stop</b> supporting <u>version 2 onion services</u></em> <em><b>very soon</b></em>. Please see the <a href="https://support.torproject.org/onionservices/v2-deprecation/">deprecation F.A.Q.</a> entry regarding Tor version 0.4.6. Migrate your services and update your bookmarks to version 3 onion services as soon as possible.</p>
<p>The full changelog since <a href="https://gitweb.torproject.org/builders/tor-browser-build.git/plain/projects/tor-browser/Bundle-Data/Docs/ChangeLog.txt?h=master"> Tor Browser 11.0a9:</a></p>
<ul>
<li>Windows + OS X + Linux
<ul>
<li>Update Firefox to 91.3.0esr</li>
<li>Update Tor to 0.4.7.2-alpha</li>
<li><a href="https://bugs.torproject.org/32624">Bug 32624</a>: localStorage is not shared between tabs [tor-browser]</li>
<li><a href="https://bugs.torproject.org/33125">Bug 33125</a>: Remove xpinstall.whitelist.add* as they don't do anything anymore [tor-browser]</li>
<li><a href="https://bugs.torproject.org/34188">Bug 34188</a>: Cleanup extensions.* prefs [tor-browser]</li>
<li><a href="https://bugs.torproject.org/tpo/applications/tor-browser/40053">Bug 40053</a>: investigate fingerprinting potential of extended TextMetrics interface</li>
<li><a href="https://bugs.torproject.org/tpo/applications/tor-browser/40083">Bug 40083</a>: Make sure Region.jsm fetching is disabled</li>
<li><a href="https://bugs.torproject.org/tpo/applications/tor-browser/40177">Bug 40177</a>: Clean up obsolete preferences in our 000-tor-browser.js</li>
<li><a href="https://bugs.torproject.org/tpo/applications/tor-browser/40220">Bug 40220</a>: Make sure tracker cookie purging is disabled</li>
<li><a href="https://bugs.torproject.org/tpo/applications/tor-browser/40342">Bug 40342</a>: Set `gfx.bundled-fonts.activate = 1` to preserve current bundled fonts behaviour</li>
<li><a href="https://bugs.torproject.org/tpo/applications/tor-browser/40463">Bug 40463</a>: Disable network.http.windows10-sso.enabled in FF 91</li>
<li><a href="https://bugs.torproject.org/tpo/applications/tor-browser/40483">Bug 40483</a>: Deutsche Welle v2 redirect</li>
<li><a href="https://bugs.torproject.org/tpo/applications/tor-browser/40548">Bug 40548</a>: Set network.proxy.failover_direct to false in FF 91</li>
<li><a href="https://bugs.torproject.org/tpo/applications/tor-browser/40630">Bug 40630</a>: New Identity and New Circuit icons</li>
<li><a href="https://bugs.torproject.org/tpo/applications/tor-browser/40641">Bug 40641</a>: Update Security Level selection in about:preferences to match style as tracking protection option bubbles</li>
<li><a href="https://bugs.torproject.org/tpo/applications/tor-browser/40648">Bug 40648</a>: Replace onion pattern divs/css with tiling SVG</li>
<li><a href="https://bugs.torproject.org/tpo/applications/tor-browser/40653">Bug 40653</a>: Onion Available text not aligned correctly in toolbar in ESR91</li>
<li><a href="https://bugs.torproject.org/tpo/applications/tor-browser/40655">Bug 40655</a>: esr91 is suggesting to make Tor Browser the default browse</li>
<li><a href="https://bugs.torproject.org/tpo/applications/tor-browser/40657">Bug 40657</a>: esr91 is missing "New identity" in hamburger menu</li>
<li><a href="https://bugs.torproject.org/tpo/applications/tor-browser/40680">Bug 40680</a>: Prepare update to localized assets for YEC</li>
</ul>
</li>
<li>Build System
<ul>
<li>Windows + OS X + Linux
<ul>
<li><a href="https://bugs.torproject.org/tpo/applications/tor-browser-build/40366">Bug 40366</a>: Use bullseye to build https-everywhere</li>
<li><a href="https://bugs.torproject.org/tpo/applications/tor-browser-build/40368">Bug 40368</a>: Use system's python3 for https-everywhere</li>
</ul>
</li>
</ul>
</li>
</ul>

