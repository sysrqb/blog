title: New Release: Tor 0.4.5.6
---
pub_date: 2021-02-15
---
author: nickm
---
tags: stable release
---
categories: releases
---
_html_body:

<p>After months of work, we have a new stable release series! If you build Tor from source, you can download the source code for 0.4.5.6 on the <a href="https://www.torproject.org/download/tor/">download page</a>. Packages should be available within the next several weeks, with a new Tor Browser likely next week.</p>
<p>The Tor 0.4.5.x release series is dedicated to the memory of <a href="https://blog.torproject.org/in-memoriam-of-karsten-loesing">Karsten Loesing</a> (1979-2020), Tor developer, cypherpunk, husband, and father. Karsten is best known for creating the Tor metrics portal and leading the metrics team, but he was involved in Tor from the early days. For example, while he was still a student he invented and implemented the v2 onion service directory design, and he also served as an ambassador to the many German researchers working in the anonymity field. We loved him and respected him for his patience, his consistency, and his welcoming approach to growing our community.</p>
<p>This release series introduces significant improvements in relay IPv6 address discovery, a new "MetricsPort" mechanism for relay operators to measure performance, LTTng support, build system improvements to help when using Tor as a static library, and significant bugfixes related to Windows relay performance. It also includes numerous smaller features and bugfixes.</p>
<p>Below are the changes since 0.4.4.7. For a list of changes since 0.4.5.5-rc, see the ChangeLog file.</p>
<h2>Changes in version 0.4.5.6 - 2021-02-15</h2>
<ul>
<li>Major features (build):
<ul>
<li>When building Tor, first link all object files into a single static library. This may help with embedding Tor in other programs. Note that most Tor functions do not constitute a part of a stable or supported API: only those functions in tor_api.h should be used if embedding Tor. Closes ticket <a href="https://bugs.torproject.org/tpo/core/tor/40127">40127</a>.</li>
</ul>
</li>
<li>Major features (metrics):
<ul>
<li>Introduce a new MetricsPort which exposes, through an HTTP interface, a series of metrics that tor collects at runtime. At the moment, the only supported output format is Prometheus data model. Closes ticket <a href="https://bugs.torproject.org/tpo/core/tor/40063">40063</a>. See the manual page for more information and security considerations.</li>
</ul>
</li>
</ul>
<p> </p>
<!--break--><ul>
<li>Major features (relay, IPv6):
<ul>
<li>The torrc option Address now supports IPv6. This unifies our address discovery interface to support IPv4, IPv6, and hostnames. Closes ticket <a href="https://bugs.torproject.org/tpo/core/tor/33233">33233</a>.</li>
<li>Launch IPv4 and IPv6 ORPort self-test circuits on relays and bridges. Closes ticket <a href="https://bugs.torproject.org/tpo/core/tor/33222">33222</a>.</li>
<li>Relays now automatically bind on IPv6 for their ORPort, unless specified otherwise with the IPv4Only flag. Closes ticket <a href="https://bugs.torproject.org/tpo/core/tor/33246">33246</a>.</li>
<li>When a relay with IPv6 support is told to open a connection to another relay, and the extend cell lists both IPv4 and IPv6 addresses, the first relay now picks randomly which address to use. Closes ticket <a href="https://bugs.torproject.org/tpo/core/tor/33220">33220</a>.</li>
<li>Relays now track their IPv6 ORPort reachability separately from the reachability of their IPv4 ORPort. They will not publish a descriptor unless _both_ ports appear to be externally reachable. Closes ticket <a href="https://bugs.torproject.org/tpo/core/tor/34067">34067</a>.</li>
</ul>
</li>
<li>Major features (tracing):
<ul>
<li>Add event-tracing library support for USDT and LTTng-UST, and a few tracepoints in the circuit subsystem. More will come incrementally. This feature is compiled out by default: it needs to be enabled at configure time. See documentation in doc/HACKING/Tracing.md. Closes ticket <a href="https://bugs.torproject.org/tpo/core/tor/32910">32910</a>.</li>
</ul>
</li>
<li>Major bugfixes (directory cache, performance, windows):
<ul>
<li>Limit the number of items in the consensus diff cache to 64 on Windows. We hope this will mitigate an issue where Windows relay operators reported Tor using 100% CPU, while we investigate better solutions. Fixes bug <a href="https://bugs.torproject.org/tpo/core/tor/24857">24857</a>; bugfix on 0.3.1.1-alpha.</li>
</ul>
</li>
<li>Major bugfixes (relay, windows):
<ul>
<li>Fix a bug in our implementation of condition variables on Windows. Previously, a relay on Windows would use 100% CPU after running for some time. Because of this change, Tor now require Windows Vista or later to build and run. Fixes bug <a href="https://bugs.torproject.org/tpo/core/tor/30187">30187</a>; bugfix on 0.2.6.3-alpha. (This bug became more serious in 0.3.1.1-alpha with the introduction of consensus diffs.) Patch by Daniel Pinto.</li>
</ul>
</li>
<li>Major bugfixes (TLS, buffer):
<ul>
<li>When attempting to read N bytes on a TLS connection, really try to read all N bytes. Previously, Tor would stop reading after the first TLS record, which can be smaller than the N bytes requested, and not check for more data until the next mainloop event. Fixes bug <a href="https://bugs.torproject.org/tpo/core/tor/40006">40006</a>; bugfix on 0.1.0.5-rc.</li>
</ul>
</li>
<li>Minor features (address discovery):
<ul>
<li>If no Address statements are found, relays now prioritize guessing their address by looking at the local interface instead of the local hostname. If the interface address can't be found, the local hostname is used. Closes ticket <a href="https://bugs.torproject.org/tpo/core/tor/33238">33238</a>.</li>
</ul>
</li>
<li>Minor features (admin tools):
<ul>
<li>Add a new --format argument to -key-expiration option to allow specifying the time format of the expiration date. Adds Unix timestamp format support. Patch by Daniel Pinto. Closes ticket <a href="https://bugs.torproject.org/tpo/core/tor/30045">30045</a>.</li>
</ul>
</li>
<li>Minor features (authority, logging):
<ul>
<li>Log more information for directory authority operators during the consensus voting process, and while processing relay descriptors. Closes ticket <a href="https://bugs.torproject.org/tpo/core/tor/40245">40245</a>.</li>
</ul>
</li>
<li>Minor features (bootstrap reporting):
<ul>
<li>When reporting bootstrapping status on a relay, do not consider connections that have never been the target of an origin circuit. Previously, all connection failures were treated as potential bootstrapping failures, including connections that had been opened because of client requests. Closes ticket <a href="https://bugs.torproject.org/tpo/core/tor/25061">25061</a>.</li>
</ul>
</li>
<li>Minor features (build):
<ul>
<li>When running the configure script, try to detect version mismatches between the OpenSSL headers and libraries, and suggest that the user should try "--with-openssl-dir". Closes 40138.</li>
<li>If the configure script has given any warnings, remind the user about them at the end of the script. Related to 40138.</li>
</ul>
</li>
<li>Minor features (configuration):
<ul>
<li>Allow using wildcards (* and ?) with the %include option on configuration files. Closes ticket <a href="https://bugs.torproject.org/tpo/core/tor/25140">25140</a>. Patch by Daniel Pinto.</li>
<li>Allow the configuration options EntryNodes, ExcludeNodes, ExcludeExitNodes, ExitNodes, MiddleNodes, HSLayer2Nodes and HSLayer3Nodes to be specified multiple times. Closes ticket <a href="https://bugs.torproject.org/tpo/core/tor/28361">28361</a>. Patch by Daniel Pinto.</li>
</ul>
</li>
<li>Minor features (control port):
<ul>
<li>Add a DROPTIMEOUTS command to drop circuit build timeout history and reset the current timeout. Closes ticket <a href="https://bugs.torproject.org/tpo/core/tor/40002">40002</a>.</li>
<li>When a stream enters the AP_CONN_STATE_CONTROLLER_WAIT status, send a control port event. Closes ticket <a href="https://bugs.torproject.org/tpo/core/tor/32190">32190</a>. Patch by Neel Chauhan.</li>
<li>Introduce GETINFO "stats/ntor/{assigned/requested}" and "stats/tap/{assigned/requested}" to get the NTor and TAP circuit onion handshake counts respectively. Closes ticket <a href="https://bugs.torproject.org/tpo/core/tor/28279">28279</a>. Patch by Neel Chauhan.</li>
</ul>
</li>
<li>Minor features (control port, IPv6):
<ul>
<li>Tor relays now try to report to the controller when they are launching an IPv6 self-test. Closes ticket <a href="https://bugs.torproject.org/tpo/core/tor/34068">34068</a>.</li>
<li>Introduce "GETINFO address/v4" and "GETINFO address/v6" in the control port to fetch the Tor host's respective IPv4 or IPv6 address. We keep "GETINFO address" for backwards-compatibility. Closes ticket <a href="https://bugs.torproject.org/tpo/core/tor/40039">40039</a>. Patch by Neel Chauhan.</li>
</ul>
</li>
<li>Minor features (directory authorities):
<ul>
<li>Add a new consensus method 30 that removes the unnecessary "=" padding from ntor-onion-key. Closes ticket <a href="https://bugs.torproject.org/tpo/core/tor/7869">7869</a>. Patch by Daniel Pinto.</li>
<li>Directory authorities now reject descriptors from relays running Tor versions from the obsolete 0.4.1 series. Resolves ticket <a href="https://bugs.torproject.org/tpo/core/tor/34357">34357</a>. Patch by Neel Chauhan.</li>
<li>The AssumeReachable option no longer stops directory authorities from checking whether other relays are running. A new AuthDirTestReachability option can be used to disable these checks. Closes ticket <a href="https://bugs.torproject.org/tpo/core/tor/34445">34445</a>.</li>
<li>When looking for possible Sybil attacks, also consider IPv6 addresses. Two routers are considered to have "the same" address by this metric if they are in the same /64 network. Patch from Maurice Pibouin. Closes ticket <a href="https://bugs.torproject.org/tpo/core/tor/7193">7193</a>.</li>
</ul>
</li>
<li>Minor features (directory authorities, IPv6):
<ul>
<li>Make authorities add their IPv6 ORPort (if any) to the trusted servers list. Authorities previously added only their IPv4 addresses. Closes ticket <a href="https://bugs.torproject.org/tpo/core/tor/32822">32822</a>.</li>
</ul>
</li>
<li>Minor features (documentation):
<ul>
<li>Mention the "!badexit" directive that can appear in an authority's approved-routers file, and update the description of the "!invalid" directive. Closes ticket <a href="https://bugs.torproject.org/tpo/core/tor/40188">40188</a>.</li>
</ul>
</li>
<li>Minor features (ed25519, relay):
<ul>
<li>Save a relay's base64-encoded ed25519 identity key to the data directory in a file named fingerprint-ed25519. Closes ticket <a href="https://bugs.torproject.org/tpo/core/tor/30642">30642</a>. Patch by Neel Chauhan.</li>
</ul>
</li>
<li>Minor features (heartbeat):
<ul>
<li>Include the total number of inbound and outbound IPv4 and IPv6 connections in the heartbeat message. Closes ticket <a href="https://bugs.torproject.org/tpo/core/tor/29113">29113</a>.</li>
</ul>
</li>
<li>Minor features (IPv6, ExcludeNodes):
<ul>
<li>Handle IPv6 addresses in ExcludeNodes; previously they were ignored. Closes ticket <a href="https://bugs.torproject.org/tpo/core/tor/34065">34065</a>. Patch by Neel Chauhan.</li>
</ul>
</li>
<li>Minor features (logging):
<ul>
<li>Add the running glibc version to the log, and the compiled glibc version to the library list returned when using --library-versions. Patch from Daniel Pinto. Closes ticket <a href="https://bugs.torproject.org/tpo/core/tor/40047">40047</a>.</li>
<li>Consider an HTTP 301 response to be an error (like a 404) when processing a directory response. Closes ticket <a href="https://bugs.torproject.org/tpo/core/tor/40053">40053</a>.</li>
<li>Log directory fetch statistics as a single line. Closes ticket <a href="https://bugs.torproject.org/tpo/core/tor/40159">40159</a>.</li>
<li>Provide more complete descriptions of our connections when logging about them. Closes ticket <a href="https://bugs.torproject.org/tpo/core/tor/40041">40041</a>.</li>
<li>When describing a relay in the logs, we now include its ed25519 identity. Closes ticket <a href="https://bugs.torproject.org/tpo/core/tor/22668">22668</a>.</li>
</ul>
</li>
<li>Minor features (onion services):
<ul>
<li>Only overwrite an onion service's existing hostname file if its contents are wrong. This enables read-only onion-service directories. Resolves ticket <a href="https://bugs.torproject.org/tpo/core/tor/40062">40062</a>. Patch by Neel Chauhan.</li>
</ul>
</li>
<li>Minor features (pluggable transports):
<ul>
<li>Add an OutboundBindAddressPT option to allow users to specify which IPv4 and IPv6 address pluggable transports should use for outgoing IP packets. Tor does not have a way to enforce that the pluggable transport honors this option, so each pluggable transport needs to implement support on its own. Closes ticket <a href="https://bugs.torproject.org/tpo/core/tor/5304">5304</a>.</li>
</ul>
</li>
<li>Minor features (protocol, proxy support, defense in depth):
<ul>
<li>Respond more deliberately to misbehaving proxies that leave leftover data on their connections, so as to make Tor even less likely to allow the proxies to pass their data off as having come from a relay. Closes ticket <a href="https://bugs.torproject.org/tpo/core/tor/40017">40017</a>.</li>
</ul>
</li>
<li>Minor features (relay address tracking):
<ul>
<li>We now store relay addresses for OR connections in a more logical way. Previously we would sometimes overwrite the actual address of a connection with a "canonical address", and then store the "real address" elsewhere to remember it. We now track the "canonical address" elsewhere for the cases where we need it, and leave the connection's address alone. Closes ticket <a href="https://bugs.torproject.org/tpo/core/tor/33898">33898</a>.</li>
</ul>
</li>
<li>Minor features (relay):
<ul>
<li>If a relay is unable to discover its address, attempt to learn it from the NETINFO cell. Closes ticket <a href="https://bugs.torproject.org/tpo/core/tor/40022">40022</a>.</li>
<li>Log immediately when launching a relay self-check. Previously we would try to log before launching checks, or approximately when we intended to launch checks, but this tended to be error-prone. Closes ticket <a href="https://bugs.torproject.org/tpo/core/tor/34137">34137</a>.</li>
</ul>
</li>
<li>Minor features (relay, address discovery):
<ul>
<li>If Address option is not found in torrc, attempt to learn our address with the configured ORPort address if any. Closes ticket <a href="https://bugs.torproject.org/tpo/core/tor/33236">33236</a>.</li>
</ul>
</li>
<li>Minor features (relay, IPv6):
<ul>
<li>Add an AssumeReachableIPv6 option to disable self-checking IPv6 reachability. Closes part of ticket <a href="https://bugs.torproject.org/tpo/core/tor/33224">33224</a>.</li>
<li>Add new "assume-reachable" and "assume-reachable-ipv6" consensus parameters to be used in an emergency to tell relays that they should publish even if they cannot complete their ORPort self- checks. Closes ticket <a href="https://bugs.torproject.org/tpo/core/tor/34064">34064</a> and part of 33224.</li>
<li>Allow relays to send IPv6-only extend cells. Closes ticket <a href="https://bugs.torproject.org/tpo/core/tor/33222">33222</a>.</li>
<li>Declare support for the Relay=3 subprotocol version. Closes ticket <a href="https://bugs.torproject.org/tpo/core/tor/33226">33226</a>.</li>
<li>When launching IPv6 ORPort self-test circuits, make sure that the second-last hop can initiate an IPv6 extend. Closes ticket <a href="https://bugs.torproject.org/tpo/core/tor/33222">33222</a>.</li>
</ul>
</li>
<li>Minor features (safety):
<ul>
<li>Log a warning at startup if Tor is built with compile-time options that are likely to make it less stable or reliable. Closes ticket <a href="https://bugs.torproject.org/tpo/core/tor/18888">18888</a>.</li>
</ul>
</li>
<li>Minor features (specification update):
<ul>
<li>Several fields in microdescriptors, router descriptors, and consensus documents that were formerly optional are now required. Implements proposal 315; closes ticket <a href="https://bugs.torproject.org/tpo/core/tor/40132">40132</a>.</li>
</ul>
</li>
<li>Minor features (state management):
<ul>
<li>When loading the state file, remove entries from the statefile that have been obsolete for a long time. Ordinarily Tor preserves unrecognized entries in order to keep forward-compatibility, but these entries have not actually been used in any release since before 0.3.5.x. Closes ticket <a href="https://bugs.torproject.org/tpo/core/tor/40137">40137</a>.</li>
</ul>
</li>
<li>Minor features (statistics, ipv6):
<ul>
<li>Relays now publish IPv6-specific counts of single-direction versus bidirectional relay connections. Closes ticket <a href="https://bugs.torproject.org/tpo/core/tor/33264">33264</a>.</li>
<li>Relays now publish their IPv6 read and write statistics over time, if statistics are enabled. Closes ticket <a href="https://bugs.torproject.org/tpo/core/tor/33263">33263</a>.</li>
</ul>
</li>
<li>Minor features (subprotocol versions):
<ul>
<li>Use the new limitations on subprotocol versions due to proposal 318 to simplify our implementation. Part of ticket <a href="https://bugs.torproject.org/tpo/core/tor/40133">40133</a>.</li>
</ul>
</li>
<li>Minor features (testing configuration):
<ul>
<li>The TestingTorNetwork option no longer implicitly sets AssumeReachable to 1. This change allows us to test relays' self- testing mechanisms, and to test authorities' relay-testing functionality. Closes ticket <a href="https://bugs.torproject.org/tpo/core/tor/34446">34446</a>.</li>
</ul>
</li>
<li>Minor features (testing):
<ul>
<li>Added unit tests for channel_matches_target_addr_for_extend(). Closes Ticket <a href="https://bugs.torproject.org/tpo/core/tor/33919">33919</a>. Patch by MrSquanchee.</li>
</ul>
</li>
<li>Minor bugfixes (circuit padding):
<ul>
<li>When circpad_send_padding_cell_for_callback is called, `is_padding_timer_scheduled` flag was not reset. Now it is set to 0 at the top of that function. Fixes bug <a href="https://bugs.torproject.org/tpo/core/tor/32671">32671</a>; bugfix on 0.4.0.1-alpha.</li>
<li>Add a per-circuit padding machine instance counter, so we can differentiate between shutdown requests for old machines on a circuit. Fixes bug <a href="https://bugs.torproject.org/tpo/core/tor/30992">30992</a>; bugfix on 0.4.1.1-alpha.</li>
<li>Add the ability to keep circuit padding machines if they match a set of circuit states or purposes. This allows us to have machines that start up under some conditions but don't shut down under others. We now use this mask to avoid starting up introduction circuit padding again after the machines have already completed. Fixes bug <a href="https://bugs.torproject.org/tpo/core/tor/32040">32040</a>; bugfix on 0.4.1.1-alpha.</li>
</ul>
</li>
<li>Minor bugfixes (circuit, handshake):
<ul>
<li>In the v3 handshaking code, use connection_or_change_state() to change the state. Previously, we changed the state directly, but this did not pass the state change to the pubsub or channel objects, potentially leading to bugs. Fixes bug <a href="https://bugs.torproject.org/tpo/core/tor/32880">32880</a>; bugfix on 0.2.3.6-alpha. Patch by Neel Chauhan.</li>
</ul>
</li>
<li>Minor bugfixes (compilation):
<ul>
<li>Change the linker flag ordering in our library search code so that it works for compilers that need the libraries to be listed in the right order. Fixes bug <a href="https://bugs.torproject.org/tpo/core/tor/33624">33624</a>; bugfix on 0.1.1.0-alpha.</li>
<li>Fix the "--enable-static-tor" switch to properly set the "-static" compile option onto the tor binary only. Fixes bug <a href="https://bugs.torproject.org/tpo/core/tor/40111">40111</a>; bugfix on 0.2.3.1-alpha.</li>
</ul>
</li>
<li>Minor bugfixes (configuration):
<ul>
<li>Exit Tor on a misconfiguration when the Bridge line is configured to use a transport but no corresponding ClientTransportPlugin can be found. Prior to this fix, Tor would attempt to connect to the bridge directly without using the transport, making it easier for adversaries to notice the bridge. Fixes bug <a href="https://bugs.torproject.org/tpo/core/tor/25528">25528</a>; bugfix on 0.2.6.1-alpha.</li>
</ul>
</li>
<li>Minor bugfixes (control port):
<ul>
<li>Make sure we send the SOCKS request address in relay begin cells when a stream is attached with the purpose CIRCUIT_PURPOSE_CONTROLLER. Fixes bug <a href="https://bugs.torproject.org/tpo/core/tor/33124">33124</a>; bugfix on 0.0.5. Patch by Neel Chauhan.</li>
</ul>
</li>
<li>Minor bugfixes (crash, relay, signing key):
<ul>
<li>Avoid assertion failures when we run Tor from the command line with `--key-expiration sign`, but an ORPort is not set. Fixes bug <a href="https://bugs.torproject.org/tpo/core/tor/40015">40015</a>; bugfix on 0.3.2.1-alpha. Patch by Neel Chauhan.</li>
</ul>
</li>
<li>Minor bugfixes (logging):
<ul>
<li>Avoid a spurious log message about missing subprotocol versions, when the consensus that we're reading from is older than the current release. Previously we had made this message nonfatal, but in practice, it is never relevant when the consensus is older than the current release. Fixes bug <a href="https://bugs.torproject.org/tpo/core/tor/40281">40281</a>; bugfix on 0.4.0.1-alpha.</li>
<li>Remove trailing whitespace from control event log messages. Fixes bug <a href="https://bugs.torproject.org/tpo/core/tor/32178">32178</a>; bugfix on 0.1.1.1-alpha. Based on a patch by Amadeusz Pawlik.</li>
<li>Turn warning-level log message about SENDME failure into a debug- level message. (This event can happen naturally, and is no reason for concern). Fixes bug <a href="https://bugs.torproject.org/tpo/core/tor/40142">40142</a>; bugfix on 0.4.1.1-alpha.</li>
<li>When logging a rate-limited message about how many messages have been suppressed in the last N seconds, give an accurate value for N, rounded up to the nearest minute. Previously we would report the size of the rate-limiting interval, regardless of when the messages started to occur. Fixes bug <a href="https://bugs.torproject.org/tpo/core/tor/19431">19431</a>; bugfix on 0.2.2.16-alpha.</li>
</ul>
</li>
<li>Minor bugfixes (onion services):
<ul>
<li>Avoid a non-fatal assertion in certain edge-cases when establishing a circuit to an onion service. Fixes bug <a href="https://bugs.torproject.org/tpo/core/tor/32666">32666</a>; bugfix on 0.3.0.3-alpha.</li>
</ul>
</li>
<li>Minor bugfixes (rust, protocol versions):
<ul>
<li>Declare support for the onion service introduction point denial of service extensions when building with Rust. Fixes bug <a href="https://bugs.torproject.org/tpo/core/tor/34248">34248</a>; bugfix on 0.4.2.1-alpha.</li>
<li>Make Rust protocol version support checks consistent with the undocumented error behavior of the corresponding C code. Fixes bug <a href="https://bugs.torproject.org/tpo/core/tor/34251">34251</a>; bugfix on 0.3.3.5-rc.</li>
</ul>
</li>
<li>Minor bugfixes (self-testing):
<ul>
<li>When receiving an incoming circuit, only accept it as evidence that we are reachable if the declared address of its channel is the same address we think that we have. Otherwise, it could be evidence that we're reachable on some other address. Fixes bug <a href="https://bugs.torproject.org/tpo/core/tor/20165">20165</a>; bugfix on 0.1.0.1-rc.</li>
</ul>
</li>
<li>Minor bugfixes (spec conformance):
<ul>
<li>Use the correct key type when generating signing-&gt;link certificates. Fixes bug <a href="https://bugs.torproject.org/tpo/core/tor/40124">40124</a>; bugfix on 0.2.7.2-alpha.</li>
</ul>
</li>
<li>Minor bugfixes (subprotocol versions):
<ul>
<li>Consistently reject extra commas, instead of only rejecting leading commas. Fixes bug <a href="https://bugs.torproject.org/tpo/core/tor/27194">27194</a>; bugfix on 0.2.9.4-alpha.</li>
<li>In summarize_protover_flags(), treat empty strings the same as NULL. This prevents protocols_known from being set. Previously, we treated empty strings as normal strings, which led to protocols_known being set. Fixes bug <a href="https://bugs.torproject.org/tpo/core/tor/34232">34232</a>; bugfix on 0.3.3.2-alpha. Patch by Neel Chauhan.</li>
</ul>
</li>
<li>Code simplification and refactoring:
<ul>
<li>Add and use a set of functions to perform down-casts on constant connection and channel pointers. Closes ticket <a href="https://bugs.torproject.org/tpo/core/tor/40046">40046</a>.</li>
<li>Refactor our code that logs descriptions of connections, channels, and the peers on them, to use a single call path. This change enables us to refactor the data types that they use, and eliminates many confusing usages of those types. Closes ticket <a href="https://bugs.torproject.org/tpo/core/tor/40041">40041</a>.</li>
<li>Refactor some common node selection code into a single function. Closes ticket <a href="https://bugs.torproject.org/tpo/core/tor/34200">34200</a>.</li>
<li>Remove the now-redundant 'outbuf_flushlen' field from our connection type. It was previously used for an older version of our rate-limiting logic. Closes ticket <a href="https://bugs.torproject.org/tpo/core/tor/33097">33097</a>.</li>
<li>Rename "fascist_firewall_*" identifiers to "reachable_addr_*" instead, for consistency with other code. Closes ticket <a href="https://bugs.torproject.org/tpo/core/tor/18106">18106</a>.</li>
<li>Rename functions about "advertised" ports which are not in fact guaranteed to return the ports that have been advertised. Closes ticket <a href="https://bugs.torproject.org/tpo/core/tor/40055">40055</a>.</li>
<li>Split implementation of several command line options from options_init_from_torrc into smaller isolated functions. Patch by Daniel Pinto. Closes ticket <a href="https://bugs.torproject.org/tpo/core/tor/40102">40102</a>.</li>
<li>When an extend cell is missing an IPv4 or IPv6 address, fill in the address from the extend info. This is similar to what was done in ticket <a href="https://bugs.torproject.org/tpo/core/tor/33633">33633</a> for ed25519 keys. Closes ticket <a href="https://bugs.torproject.org/tpo/core/tor/33816">33816</a>. Patch by Neel Chauhan.</li>
</ul>
</li>
<li>Deprecated features:
<ul>
<li>The "non-builtin" argument to the "--dump-config" command is now deprecated. When it works, it behaves the same as "short", which you should use instead. Closes ticket <a href="https://bugs.torproject.org/tpo/core/tor/33398">33398</a>.</li>
</ul>
</li>
<li>Documentation:
<ul>
<li>Replace URLs from our old bugtracker so that they refer to the new bugtracker and wiki. Closes ticket <a href="https://bugs.torproject.org/tpo/core/tor/40101">40101</a>.</li>
</ul>
</li>
<li>Removed features:
<ul>
<li>We no longer ship or build a "tor.service" file for use with systemd. No distribution included this script unmodified, and we don't have the expertise ourselves to maintain this in a way that all the various systemd-based distributions can use. Closes ticket <a href="https://bugs.torproject.org/tpo/core/tor/30797">30797</a>.</li>
<li>We no longer ship support for the Android logging API. Modern versions of Android can use the syslog API instead. Closes ticket <a href="https://bugs.torproject.org/tpo/core/tor/32181">32181</a>.</li>
<li>The "optimistic data" feature is now always on; there is no longer an option to disable it from the torrc file or from the consensus directory. Closes part of 40139.</li>
<li>The "usecreatefast" network parameter is now removed; there is no longer an option for authorities to turn it off. Closes part of 40139.</li>
</ul>
</li>
<li>Testing:
<ul>
<li>Add unit tests for bandwidth statistics manipulation functions. Closes ticket <a href="https://bugs.torproject.org/tpo/core/tor/33812">33812</a>. Patch by MrSquanchee.</li>
</ul>
</li>
<li>Code simplification and refactoring (autoconf):
<ul>
<li>Remove autoconf checks for unused funcs and headers. Closes ticket <a href="https://bugs.torproject.org/tpo/core/tor/31699">31699</a>; Patch by @bduszel</li>
</ul>
</li>
<li>Code simplification and refactoring (maintainer scripts):
<ul>
<li>Disable by default the pre-commit hook. Use the environment variable TOR_EXTRA_PRE_COMMIT_CHECKS in order to run it. Furthermore, stop running practracker in the pre-commit hook and make check-local. Closes ticket <a href="https://bugs.torproject.org/tpo/core/tor/40019">40019</a>.</li>
</ul>
</li>
<li>Code simplification and refactoring (relay address):
<ul>
<li>Most of IPv4 representation was using "uint32_t". It has now been moved to use the internal "tor_addr_t" interface instead. This is so we can properly integrate IPv6 along IPv4 with common interfaces. Closes ticket <a href="https://bugs.torproject.org/tpo/core/tor/40043">40043</a>.</li>
</ul>
</li>
<li>Documentation (manual page):
<ul>
<li>Move them from doc/ to doc/man/. Closes ticket <a href="https://bugs.torproject.org/tpo/core/tor/40044">40044</a>.</li>
<li>Describe the status of the "Sandbox" option more accurately. It is no longer "experimental", but it _is_ dependent on kernel and libc versions. Closes ticket <a href="https://bugs.torproject.org/tpo/core/tor/23378">23378</a>.</li>
</ul>
</li>
<li>Documentation (tracing):
<ul>
<li>Document in depth the circuit subsystem trace events in the new doc/tracing/EventsCircuit.md. Closes ticket <a href="https://bugs.torproject.org/tpo/core/tor/40036">40036</a>.</li>
</ul>
</li>
<li>Removed features (controller):
<ul>
<li>Remove the "GETINFO network-status" controller command. It has been deprecated since 0.3.1.1-alpha. Closes ticket <a href="https://bugs.torproject.org/tpo/core/tor/22473">22473</a>.</li>
</ul>
</li>
</ul>

---
_comments:

<a id="comment-291212"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-291212" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Dumb Guy (not verified)</span> said:</p>
      <p class="date-time">February 23, 2021</p>
    </div>
    <a href="#comment-291212">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-291212" class="permalink" rel="bookmark">Hello. I would like to make…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Hello. I would like to make all the traffic that goes out of my LAN pass through the Tor network, but I have no idea how to do it. Do I need to use a program, or change my NIC setings or something like that? Please awnser me</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-291239"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-291239" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">February 24, 2021</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-291212" class="permalink" rel="bookmark">Hello. I would like to make…</a> by <span>Dumb Guy (not verified)</span></p>
    <a href="#comment-291239">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-291239" class="permalink" rel="bookmark">Your LAN may host many end…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Your LAN may host many end-user client hardware devices. Did you mean to say your device alone? Each device would have to route through the Tor network. Each application on each device may leak metadata that hints at your identity regardless of whether it goes through Tor. Some web services (email, social, banks, work, shopping, etc.) may decide to reject traffic from Tor or lock/suspend accounts that connect from exit nodes. Tor Browser is not just Firefox redirected through the Tor network. It has many patches and reconfigurations to minimize leaking metadata and increase anonymity. Read about Torifying:</p>
<ul>
<li><a href="https://support.torproject.org/about/can-i-use-tor-with/" rel="nofollow">What programs can I use with Tor?</a> If you want to try something small, I recommend trying to Torify an application on your device. Tor Browser's SOCKS5 port is 9150, or the tor daemon's SOCKS5 port is 9050.</li>
</ul>
<p>If you wish to proceed, you could try configuring your device's OS to proxy all traffic through the tor daemon like Tails OS does, but some applications may ignore casual configuration or may leak certain types of traffic. If you wish to go further and do it for a LAN, you would have to configure a router to redirect all LAN traffic through a tor daemon. The daemon (a server-like application that waits for connections) can be installed on a device that can open its ports, or it can be installed on the router if the router's OS supports installing software and has enough space in its flash memory or supports a USB drive. Most retail routers do not support these kinds of advanced configurations, and even if you have one, a "dumb guy" (as OP called himself; not my words) could not do it correctly and preserve pseudonymity unless he reads extensive guides for embedded hardware and networking software (networking boxes and circuitboards). For a "dumb guy", the simplest option is something like Anonabox or InvizBox. A smarter person might try to configure a Raspberry Pi as a WiFi hotspot, and connect the Pi's ethernet to the ISP. The smartest people might go to OpenWRT's website and buy a router that is supported by OpenWRT and has lots of RAM and flash memory and whatever else. DD-WRT might be another option. A rich person might purchase an enterprise-grade managed router or managed switch and make sure it supports configuring iptables or firewall rules like redirect and DNAT. But metadata leakage is a many-headed beast. This is because, most of all, mainstream developers are not reducing it and not designing for privacy by default. Many are developing, above all, for shareholder profit.</p>
<p>The tor daemon binary (exe) is found here:<br />
<a href="https://www.torproject.org/download/tor/" rel="nofollow">https://www.torproject.org/download/tor/</a><br />
or as a package for Debian-based GNU/Linux OS here:<br />
<a href="https://support.torproject.org/apt/" rel="nofollow">https://support.torproject.org/apt/</a><br />
or as a package in repositories of other GNU/Linux OS distributions (such as OpenWRT), but check that the package version is up to date, or build the source in the first link.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-291221"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-291221" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">February 24, 2021</p>
    </div>
    <a href="#comment-291221">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-291221" class="permalink" rel="bookmark">Not working under Windows XP…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Not working under Windows XP. Ditto for the build used with newest Tor Browser, 32-bit.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-291225"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-291225" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  nickm
  </article>
    <div class="comment-header">
      <p class="comment__submitted">nickm said:</p>
      <p class="date-time">February 24, 2021</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-291221" class="permalink" rel="bookmark">Not working under Windows XP…</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-291225">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-291225" class="permalink" rel="bookmark">Sorry, we can&#039;t support XP…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Sorry, we can't support XP any more.  Microsoft hasn't been supported it for over 6 years now.</p>
<p>From the changelog above:</p>
<blockquote><ul>
<li> Major bugfixes (relay, windows):
<ul>
<li> Fix a bug in our implementation of condition variables on Windows. Previously, a relay on Windows would use 100% CPU after running for some time. Because of this change, Tor now require Windows Vista or later to build and run. Fixes bug 30187; bugfix on 0.2.6.3-alpha. (This bug became more serious in 0.3.1.1-alpha with the introduction of consensus diffs.) Patch by Daniel Pinto.
</li>
</ul>
</li>
</ul>
</blockquote>
<p>We don't break old platforms lightly, but we've reached the point where the pain of restricting ourselves to the features available on older versions of Windows made things painful for users on more recent versions.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-291261"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-291261" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>1 (not verified)</span> said:</p>
      <p class="date-time">February 25, 2021</p>
    </div>
              <p class="parent visually-hidden">In reply to nickm</p>
    <a href="#comment-291261">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-291261" class="permalink" rel="bookmark">Why have you decided to do…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Why have you decided to do it before the next LTS tor and not after?</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div></div>
