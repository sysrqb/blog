title: Global Encryption Day: #MakeTheSwitch
---
pub_date: 2021-10-21
---
author: alsmith
---
summary: Today, Oct 21, 2021, is the very first Global Encryption Day, organized by the Global Encryption Coalition. Global Encryption Day is an opportunity for businesses, civil society organizations, technologists, and millions of Internet users worldwide to show our communities why encryption matters. It’s also a day for all of us to pledge to Make the Switch to encrypted services (like Tor!) and prioritize our privacy and security online.
---
_html_body:

<p>Today, Oct 21, 2021, is the very first <a href="https://ged.globalencryption.org">Global Encryption Day</a>, organized by the <a href="https://www.globalencryption.org/">Global Encryption Coalition</a>, where we are a member. Global Encryption Day is an opportunity for businesses, civil society organizations, technologists, and millions of Internet users worldwide to show our communities why encryption matters. It’s also a day for all of us to <a href="https://ged.globalencryption.org/#pledge">pledge to Make the Switch</a> to encrypted services (like Tor!) and prioritize our privacy and security online.</p>
<p>At the Tor Project, we’re proud to help millions of people take back their right to privacy, to freely access and share information, and to more easily circumvent internet censorship--and encryption makes this possible.</p>
<p>Encryption allows us to provide these tools: for example, <a href="https://support.torproject.org/glossary/tor-tor-network-core-tor">Tor</a> uses three layers of encryption in the Tor <a href="https://support.torproject.org/glossary/circuit">circuit</a>; each<a href="https://support.torproject.org/glossary/relay"> relay</a> decrypts one layer before passing the request on to the next relay. Encryption is used in many other ways as well! Without encryption, millions of people would lose their access to the safe and uncensored internet. </p>
<p>In honor of this inaugural Global Encryption Day, the Tor Project, along with 148 other organizations and businesses have signed the <a href="https://ged.globalencryption.org/statement/">Global Encryption Day Statement</a>, calling on governments and businesses to reject efforts to undermine encryption and instead pursue policies that enhance, strengthen, and promote use of strong encryption to protect people everywhere.</p>
<p>As an individual, you can get involved with Global Encryption Day by:</p>
<ul>
<li aria-level="1"><a href="https://ged.globalencryption.org/events/">Joining more than 40 events celebrating and discussing encryption</a>, hosted from all around the world.</li>
<li aria-level="1"><a href="https://ged.globalencryption.org/pledge-to-switch-individual/">Pledging to #MakeTheSwitch to end-to-end encrypted services</a> and products and put your online security and privacy back in your own hands.</li>
<li aria-level="1"><a href="https://donate.torproject.org">Supporting the Tor Project with a donation</a> and helping everyone who needs strong encryption, privacy, security, and censorship circumvention easily access the tools that the Tor Project provides.</li>
</ul>

