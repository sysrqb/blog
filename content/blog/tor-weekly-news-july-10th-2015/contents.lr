title: Tor Weekly News — July 10th, 2015
---
pub_date: 2015-07-10
---
author: harmony
---
tags: tor weekly news
---
categories: reports
---
_html_body:

<p>Welcome to the twenty-seventh issue in 2015 of Tor Weekly News, the <a href="https://lists.torproject.org/cgi-bin/mailman/listinfo/tor-news" rel="nofollow">weekly newsletter</a> that covers what’s happening in the Tor community.</p>

<h1>Tails 1.4.1 is out</h1>

<p>The Tails team <a href="https://tails.boum.org/news/version_1.4.1/" rel="nofollow">announced</a> version 1.4.1 of the anonymous live operating system. Most notable in this release is the fix of automatic upgrades in Windows Camouflage mode, and plugging a hole in Tor Browser’s AppArmor sandbox that previously allowed it to access the list of recently-used files.</p>

<p>For a full list of changes, see the team’s announcement. This release contains important security updates, so head to the <a href="https://tails.boum.org/download/" rel="nofollow">download page</a> (or the automatic upgrader) as soon as possible. </p>

<h1>Tor Browser 4.5.3 and 5.0a3 are out</h1>

<p>The Tor Browser team put out new releases in both the stable and alpha series of the secure, private web browser. <a href="https://blog.torproject.org/blog/tor-browser-453-released" rel="nofollow">Tor Browser 4.5.3</a> contains updates to Firefox, OpenSSL, NoScript, and Torbutton; it also fixes a crash triggered by .svg files when the security slider was set to “High”, and backports a Tor patch that allows domain names containing underscores (a practice generally discouraged) to resolve properly.  For example, users should now be able to view the website of the New York Times without problems.</p>

<p><a href="https://blog.torproject.org/blog/tor-browser-50a3-released" rel="nofollow">Tor Browser 5.0a3</a>, meanwhile, is the first release to be based on Firefox 38 ESR. “For this release, we performed a thorough network and feature review of Firefox 38, and fixed the most pressing privacy issues, as well as all Tor proxy safety issues that we discovered during the audit”, wrote Georg Koppen. Changes to the toolchain used to build the browser mean “we are […] especially interested in feedback if there are stability issues or broken Tor Browser bundles due to these toolchain upgrades.</p>

<p>These are important security releases, and you should upgrade to the new version in whichever series you prefer. Head to the <a href="https://www.torproject.org/download/download-easy.html" rel="nofollow">download page</a> to get your first copy of Tor Browser, or use the in-browser updater.</p>

<h1>Tor unaffected by new OpenSSL security issue</h1>

<p>A few days ago, the team behind the essential Internet encryption toolkit OpenSSL <a href="https://mta.openssl.org/pipermail/openssl-announce/2015-July/000037.html" rel="nofollow">announced</a> that a security issue classified as “high” would shortly be disclosed and fixed, leading to concern that another <a href="https://lists.torproject.org/pipermail/tor-news/2014-April/000040.html" rel="nofollow">Heartbleed</a> was on the cards. In the event, the now-disclosed CVE-2015-1793 vulnerability does not appear to affect either the Tor daemon or Tor Browser, as Nick Mathewson <a href="https://lists.torproject.org/pipermail/tor-dev/2015-July/009050.html" rel="nofollow">explained</a>.  However, you should still upgrade your OpenSSL as soon as possible, in order to protect the other software you use which may be vulnerable.</p>

<h1>OVH is the largest and fastest-growing AS on the Tor network</h1>

<p>nusenu <a href="https://lists.torproject.org/pipermail/tor-relays/2015-July/007310.html" rel="nofollow">observed</a> that the hosting company OVH is both the largest <a href="https://en.wikipedia.org/wiki/Autonomous_system_(Internet)" rel="nofollow">autonomous system</a> on the Tor network by number of relays, and the fastest-growing. While it’s no bad thing to have multiple relays located on the same network, it becomes a problem if any one entity (or someone who watches them closely enough) is able to observe too large a fraction of Tor traffic — they would then be in a position to harm the anonymity of Tor users.</p>

<p>This is what is meant by “diversity” on the Tor network. If you’re considering running a Tor relay, then as nusenu says, “choose non-top 10 ASes when adding relays (10 is an arbitrary number)”. See nusenu’s post for more information on how to select a hosting location for a stronger and more diverse Tor network.</p>

<h1>More monthly status reports for June 2015</h1>

<p>The wave of regular monthly reports from Tor project members for the month of June continued, with reports from <a href="https://lists.torproject.org/pipermail/tor-reports/2015-July/000865.html" rel="nofollow">Leiah Jansen</a> (working on graphic design and branding), <a href="https://lists.torproject.org/pipermail/tor-reports/2015-July/000866.html" rel="nofollow">Georg Koppen</a> (developing Tor Browser), <a href="https://lists.torproject.org/pipermail/tor-reports/2015-July/000867.html" rel="nofollow">Isabela Bagueros</a> (overall project management), <a href="https://lists.torproject.org/pipermail/tor-reports/2015-July/000868.html" rel="nofollow">Sukhbir Singh</a> (developing Tor Messenger), <a href="https://lists.torproject.org/pipermail/tor-reports/2015-July/000870.html" rel="nofollow">Arlo Breault</a> (also working on Tor Messenger, as well as Tor Check), <a href="https://lists.torproject.org/pipermail/tor-reports/2015-July/000871.html" rel="nofollow">Colin Childs</a> (carrying out support, localization, and outreach), and <a href="https://lists.torproject.org/pipermail/tor-reports/2015-July/000873.html" rel="nofollow">Juha Nurmi</a> (working on onion service indexing).</p>

<p>Donncha O’Cearbhaill sent his third Tor Summer of Privacy <a href="https://lists.torproject.org/pipermail/tor-reports/2015-July/000869.html" rel="nofollow">status report</a> with updates about the OnionBalance onion service load-balancing tool, while Jesse Victors did the <a href="https://lists.torproject.org/pipermail/tor-dev/2015-July/009049.html" rel="nofollow">same</a> for the DNS-like Onion Naming System, and Israel Leiva submitted a <a href="https://lists.torproject.org/pipermail/tor-reports/2015-July/000872.html" rel="nofollow">status update</a> for the GetTor alternative software distributor, which is also being expanded as part of TSoP, as explained in Israel’s <a href="https://lists.torproject.org/pipermail/tor-dev/2015-July/009034.html" rel="nofollow">re-introduction of the project</a>. Cristobal Leiva also introduced his TSoP project, a <a href="https://lists.torproject.org/pipermail/tor-dev/2015-July/009036.html" rel="nofollow">web-based status dashboard for Tor relay operators</a></p>

<h1>Miscellaneous news</h1>

<p>David Fifield published the regular <a href="https://lists.torproject.org/pipermail/tor-dev/2015-July/009030.html" rel="nofollow">summary of costs</a> incurred by the infrastructure for the meek pluggable transport over the past month. “The rate limiting of meek-google and meek-amazon has been partially effective in bringing costs down. […] meek-azure bandwidth use continues to increase, up 17% compared to the previous month. Keep in mind that our grant expires in October, so you should not count on it continuing to work after that.”</p>

<p>Following Donncha O’Cearbhaill’s 0.0.1 alpha release of <a href="https://lists.torproject.org/pipermail/tor-talk/2015-July/038312.html" rel="nofollow">OnionBalance</a>, s7r <a href="https://lists.torproject.org/pipermail/tor-talk/2015-July/038314.html" rel="nofollow">called for help</a> putting it to the test on a running onion service. <a href="https://lists.torproject.org/pipermail/tor-talk/2015-July/038373.html" rel="nofollow">One week on</a>, there have been four million hits on the service, with hardly a murmur of complaint from OnionBalance or the service it is handling: “the same instances are running since service first started, no reboot or application restart”. See s7r’s post for more numbers.</p>

<p>This issue of Tor Weekly News has been assembled by the Tails team, Karsten Loesing, teor, and Harmony.</p>

<p>Want to continue reading TWN? Please help us create this newsletter. We still need more volunteers to watch the Tor community and report important news. Please see the <a href="https://trac.torproject.org/projects/tor/wiki/TorWeeklyNews" rel="nofollow">project page</a>, write down your name and subscribe to the <a href="https://lists.torproject.org/cgi-bin/mailman/listinfo/news-team" rel="nofollow">team mailing list</a> if you want to get involved!</p>

