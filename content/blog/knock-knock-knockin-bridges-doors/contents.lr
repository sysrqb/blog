title: Knock Knock Knockin' on Bridges' Doors
---
pub_date: 2012-01-07
---
author: twilde
---
tags:

censorship
GFW
research
censorship circumvention
research results
---
categories:

circumvention
research
---
_html_body:

<p>Greetings!  My name is Tim Wilde, Software Engineer at Team Cymru, Inc., and a big fan of the Tor Project and everything that they do.  We've helped out the Tor Project with a few investigations into probing/blocking of Tor by oppressive regimes, and the guys asked me to write this one up for the Tor Blog, so here I am!  Note: any opinions expressed here are mine, nor those of Team Cymru or the Tor Project.</p>

<p>In October 2011, <a href="https://trac.torproject.org/projects/tor/ticket/4185" rel="nofollow">ticket #4185</a> was filed in the Tor bug tracker by a user in China who found that their connections to US-based Tor bridge relays were being regularly cut off after a very short period of time.  At the time we performed some basic experimentation and discovered that Chinese IPs (presumably at the behest of the Great Firewall of China, or GFW) would reach out to the US-based bridge and connect to it shortly after the Tor user in China connected, and, if successful, shortly thereafter the connection would be blocked by the GFW.  There wasn't time for a detailed investigation and analysis at the time, but that kernel eventually grew into the investigation detailed below.  We were, however, able to determine that limiting connections to the bridge relay to only the single IP expected to be its client would, in fact, block the probes and allow the connection to remain open for an extended period (&gt;48 hours in our testing).</p>

<p>Between 05 DEC and 09 DEC 2011, we undertook a detailed and methodical investigation into probing and blocking of Tor connections originating within China.  Unfortunately for our analysis, it appears that the GFW's active blocking of connections to Tor bridges had stopped, but we were still able to gather valuable data about the probing performed by the GFW, which previously led directly and verifiably to blocking.</p>

<p>To this end we discovered two types of probing.  First, "garbage binary" probes, containing nothing more than arbitrary (but sometimes repeated in later probes) binary data, were experienced by the non-China side of any connection that originated from China to TCP port 443 (HTTPS) in which an SSL negotiation was performed.  This probe was performed in near-real-time after the connection was established, implying near-line-rate deep packet inspection (DPI) capabilities.  TCP/443 connections not performing an SSL handshake, such as using the obfsproxy obfs2 protocol or a plain-text protocol, did not provoke probing.  The purpose of these probes is unknown, and further investigation is difficult to justify when it seems relatively clear that these probes are not aimed at Tor.</p>

<p>The second type of probe, on the other hand, is aimed quite directly at Tor.  When a Tor client within China connected to a US-based bridge relay, we consistently found that at the next round 15 minute interval (HH:00, HH:15, HH:30, HH:45), the bridge relay would receive a probe from hosts within China that not only established a TCP connection, but performed an SSL negotiation, an SSL renegotiation, and then spoke the Tor protocol sufficiently to build a one-hop circuit and send a BEGIN_DIR cell.  No matter what TCP port the bridge was listening on, once a Tor client from China connected, within 3 minutes of the next 15 minute interval we saw a series of probes including at least one connection speaking the Tor protocol.</p>

<p>The good news is, we were able to isolate which characteristic of the Tor handshake the GFW was using to decide whether or not to initiate these probes.  By making a simple change to the list of supported SSL ciphers in the "hello" packet sent by the Tor client within China, we were able to prevent the probes from taking place.  This has been documented and is being discussed in <a href="https://trac.torproject.org/projects/tor/ticket/4744" rel="nofollow">ticket #4744</a>.  This differs slightly from the <a href="https://blog.torproject.org/blog/iran-blocks-tor-tor-releases-same-day-fix" rel="nofollow">method used by Iran to block Tor in September 2011</a>, using the client-side of the SSL negotiation as its trigger rather than the server-side.  It is likely, however, that technology capable of targeting either side of the connection to this degree could also target the other side, so it remains important to consider both the server and client sides of the Tor connection when attempting to blend in with normal, benign traffic.</p>

<p>The bad news, however, is that this is happening at all.  This probe again implies sophisticated near-line-rate DPI technology, coupled with a system that is aimed directly at Tor, using code that actually speaks the Tor protocol.  Clearly there is a target painted firmly on Tor, and it is quite likely that the Chinese will continue to adapt their censorship technology as the Tor Project adapts to them.</p>

<p>There is light at the end of the tunnel, though.  A number of ideas have been put forward about new protocols, handshakes, and extensions to the Tor protocol that could be used to combat this type of censorship technology in a more long-term-sustainable way.  <a href="https://gitweb.torproject.org/torspec.git/blob/HEAD:/proposals/190-password-bridge-authorization.txt" rel="nofollow">Proposal 190</a> provides for password authorization for bridge relays.  <a href="https://gitweb.torproject.org/obfsproxy.git/blob/HEAD:/doc/tor-obfs-howto.txt" rel="nofollow">obfsproxy</a> provides an extra layer on top of a Tor connection that makes it look like generic binary data.  The Tor v3 link protocol/handshake, currently available and in testing in the 0.2.3.x-alpha series of Tor releases, eliminates SSL renegotiation in Tor session establishment, removing one of Tor's current "sore thumbs" that stick out from normal HTTPS SSL traffic.</p>

<p>You're welcome to check out my <a href="https://gist.github.com/da3c7a9af01d74cd7de7" rel="nofollow">full report on this investigation</a> for more detail than I could provide in the blog post here.  Thanks very much to everyone who assisted with the investigation and the report!  It was fun to investigate and report on this, and I hope to have the opportunity to help out with similar adventures in the future.</p>

---
_comments:

<a id="comment-13426"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-13426" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">January 08, 2012</p>
    </div>
    <a href="#comment-13426">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-13426" class="permalink" rel="bookmark">Does you know if Chinese</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Does you know if Chinese block all public Tor Relays or if the Public Tor Relays are blocked on the basis of Active Probes?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-13433"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-13433" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  twilde
  </article>
    <div class="comment-header">
      <p class="comment__submitted">twilde said:</p>
      <p class="date-time">January 08, 2012</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-13426" class="permalink" rel="bookmark">Does you know if Chinese</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-13433">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-13433" class="permalink" rel="bookmark">We have not definitively</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>We have not definitively tested this, but our hypothesis is that public Tor relays are blocked by downloading the consensus from a directory server, the same way that Tor clients get lists of relays when they attempt to connect to the network.  This would be much simpler than active probing of this type.</p>
<p>Interestingly, during this testing we did note that the GFW does not currently appear to be performing blocking of public Tor relays either.  This isn't actually included in the published results (though I may add it as an appendix if I get a chance), but we ran a quick "can we talk to it" check from within China of all of the relays in the current consensus (during the testing week 05 DEC - 09 DEC 2011) and found that a large segment of them were in fact not blocked.  At a quick check it appeared that "newer" relays (with the time cutoff for new vs. old not well defined) were not blocked, while older relays were, implying that the Chinese stopped updating their blocking of public relays at some time.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-13430"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-13430" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">January 08, 2012</p>
    </div>
    <a href="#comment-13430">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-13430" class="permalink" rel="bookmark">Thank you for this</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Thank you for this interesting information.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-13457"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-13457" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">January 09, 2012</p>
    </div>
    <a href="#comment-13457">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-13457" class="permalink" rel="bookmark">Thank you for sharing the</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Thank you for sharing the information. The full report mentions that connecting to "old" hosts (i.e., long-established hosts that<br />
have had HTTPS services running for an extended period of time) does not trigger probing. How did you test this? Did you run the "old" hosts  and check if they are probed?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-13459"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-13459" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  twilde
  </article>
    <div class="comment-header">
      <p class="comment__submitted">twilde said:</p>
      <p class="date-time">January 09, 2012</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-13457" class="permalink" rel="bookmark">Thank you for sharing the</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-13459">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-13459" class="permalink" rel="bookmark">This observation is based on</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>This observation is based on making test connections from within China to a few of our web servers that have been providing SSL services for a long time (multiple years).  Despite the fact that the garbage probes occurred within moments, and could always be triggered again by waiting 10 minutes or more, on other US-based hosts connected to from within China, we did not capture any of these garbage probes when we connected to these "old" SSL hosts.  It's admittedly difficult (if not impossible) to prove a negative, and the sample size here was quite small, so I wouldn't call that one of the strongest conclusions of the report, but I felt it worth mentioning.</p>
<p>Thanks for your question and reading the report!</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-13460"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-13460" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">January 09, 2012</p>
    </div>
    <a href="#comment-13460">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-13460" class="permalink" rel="bookmark">I verified that:
- the GFW</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I verified that:<br />
- the GFW filter are not on the IP address, but on the IP:port.<br />
- if you change the TCP port, your Tor Bridge get unfiltered on the new port<br />
- On exiting Tor Relays (that have filtered port), non-tor port are reachable.</p>
<p>You verified that "old Tor relay" are filtered, but new Tor relay not.</p>
<p>I'm wondering if, once we found the right GFW bypass, we should not make a "Call for Port Change" to all Tor Operators, to have their IP:Port be fresh and non-filtered.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-13462"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-13462" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">January 09, 2012</p>
    </div>
    <a href="#comment-13462">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-13462" class="permalink" rel="bookmark">Great read and a great</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Great read and a great Article Tim!</p>
<p>-CSinghaus - HopOne Abuse Dept-</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-13464"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-13464" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">January 09, 2012</p>
    </div>
    <a href="#comment-13464">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-13464" class="permalink" rel="bookmark">Carry on the great work,</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Carry on the great work, guys!</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-13465"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-13465" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">January 09, 2012</p>
    </div>
    <a href="#comment-13465">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-13465" class="permalink" rel="bookmark">great job dude!!</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>great job dude!!</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-13473"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-13473" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">January 10, 2012</p>
    </div>
    <a href="#comment-13473">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-13473" class="permalink" rel="bookmark">Fascinating. As to the</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Fascinating. As to the "garbage binary" ssl probes... perhaps they are exploitation attempts?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-13477"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-13477" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">January 10, 2012</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-13473" class="permalink" rel="bookmark">Fascinating. As to the</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-13477">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-13477" class="permalink" rel="bookmark">This is exactly what I</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>This is exactly what I thought, sounds like they have an exploit for some proxy and are hopping to auto exploit it for whatever reasons.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-13479"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-13479" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">January 10, 2012</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-13473" class="permalink" rel="bookmark">Fascinating. As to the</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-13479">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-13479" class="permalink" rel="bookmark">More likely probes to</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>More likely probes to determine the software versions running on those machines (nmap -sV).</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-13480"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-13480" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">January 10, 2012</p>
    </div>
    <a href="#comment-13480">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-13480" class="permalink" rel="bookmark">So up to a certain point in</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>So up to a certain point in time they scanned directory servers and blocked anything found. After roughly that time they stopped doing this but implemented a probing scheme whenever a Tor connection was potentially being established.</p>
<p>This makes sense - the former scheme would only catch public servers while the new one should catch unlisted servers and allow more dynamic blocking (or at least test moving in that direction)</p>
<p>Sneaky.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-13484"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-13484" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">January 10, 2012</p>
    </div>
    <a href="#comment-13484">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-13484" class="permalink" rel="bookmark">Given the ability to set up</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Given the ability to set up bridges anywhere is the world, what locations would be most helpful?</p>
<p>Which is more effective, a single bridge with 1000KB/sec of bandwidth, or 5 bridges with 200KB/sec bandwidth each?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-13492"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-13492" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">January 11, 2012</p>
    </div>
    <a href="#comment-13492">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-13492" class="permalink" rel="bookmark">I experimented with Tor in</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I experimented with Tor in China, and I found that even connecting to a brand new private bridge, I would be blocked within about half an hour. I hopped between bridges on diverse netblocks for several days trying to avoid this, but it was always re-blocked. I'd be happy to go into detail about other things I've tried...</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-13615"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-13615" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">January 22, 2012</p>
    </div>
    <a href="#comment-13615">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-13615" class="permalink" rel="bookmark">Hey, There, I found your</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Hey, There, I found your blog while surfing the web. This is a really well written article. I’ll be sure to bookmark it and come back to read more of your useful information. Thanks for the excellent post. I will certainly return. <a href="http://www.aleyram.com/" rel="nofollow">sesli chat</a></p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-13803"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-13803" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">February 04, 2012</p>
    </div>
    <a href="#comment-13803">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-13803" class="permalink" rel="bookmark">Three.co.uk the blocking you</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Three.co.uk the blocking you speak of is not exactly correct, the true situation is the filtering software by default is child friendly and therefore crap.</p>
<p>The solution is for an adult account holder to call three and say<br />
 "they want to look at porn",</p>
<p> three will then lecture you on keeping device away from children as internet will be unrestricted. I can confirm this works and resolves correctly.</p>
<p>Running a tor bridge myself it is important to be factually correct, nobody likes censorship and the above is the fastest way to resolve it. I would guess the same for the other uk networks.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-14469"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-14469" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">March 01, 2012</p>
    </div>
    <a href="#comment-14469">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-14469" class="permalink" rel="bookmark">hi, i&#039;m live in from china.</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>hi, i'm live in from china. sorry, now obfs2 ips all blocked by china firewall. on february i'm lucky to get obfsproxy connected on tor network is work normal, but now china gov find and blocked all these obfs2 ips. it's no way, we chinese couldn't visit foreign web again. sorry, thank you done good on test in china firewall.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-14984"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-14984" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">April 14, 2012</p>
    </div>
    <a href="#comment-14984">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-14984" class="permalink" rel="bookmark">i&#039;m from china. today tor</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>i'm from china. today tor can work. its version is 0.2.2.35-0.2.15. i don't know if it's because the group of tor have done something or the bad guys in china have not done something. is there somebody that can tell?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-14985"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-14985" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">April 14, 2012</p>
    </div>
    <a href="#comment-14985">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-14985" class="permalink" rel="bookmark">hi, today tor can work. i&#039;m</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>hi, today tor can work. i'm from china. the version of tor is 0.2.2.35.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-15092"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-15092" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">April 20, 2012</p>
    </div>
    <a href="#comment-15092">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-15092" class="permalink" rel="bookmark">You are my hero! Chinese</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>You are my hero! Chinese people need Tor to know more about <b>CPC</b>. Spreading the scandals of <i>Bo Xilai</i>  and other dictators will eventually awaken our nation!</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-15342"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-15342" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">May 02, 2012</p>
    </div>
    <a href="#comment-15342">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-15342" class="permalink" rel="bookmark">I was in China from 2007 -</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I was in China from 2007 - 2011, and I pretty much gave up on TOR once TOR started using bridges for they were blocked too often, and I would e-mail for more. It was a big hassle. If I really tried for a while like 20 minutes or more then I could maybe get TOR working. Then the speed of TOR in China is sometimes quite slow as well making it a pain even more.</p>
<p>You guys need more trusted people in China to check on things for you.</p>
<p>Also since obfs2 is separate and commonly blocked are you guys going to integrate into later versions of TOR?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-15934"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-15934" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">June 03, 2012</p>
    </div>
    <a href="#comment-15934">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-15934" class="permalink" rel="bookmark">The 0.2.3.x-alpha series of</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>The 0.2.3.x-alpha series of Tor releases currently under beta-testing ?<br />
 When will it be included in the TBB ( latest TBB tor version 0.2.2.36) ?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-15946"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-15946" class="contextual-region comment js-comment" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  arma
  </article>
    <div class="comment-header">
      <p class="comment__submitted">arma said:</p>
      <p class="date-time">June 03, 2012</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-15934" class="permalink" rel="bookmark">The 0.2.3.x-alpha series of</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-15946">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-15946" class="permalink" rel="bookmark">When it finishes</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>When it finishes beta-testing and becomes the new stable.</p>
<p>We might make some Tor 0.2.3 TBBs before that, if we get our act together. Depends how much time our builders find.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-16099"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-16099" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">June 11, 2012</p>
    </div>
    <a href="#comment-16099">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-16099" class="permalink" rel="bookmark">Tor is stable in china when</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Tor is stable in china when run through a vpn. properly configured it works just fine for me.<br />
I love Tor, cause Tor loves me...</p>
</div>
  </div>
</article>
<!-- Comment END -->
