# -*- coding: utf-8 -*-
import datetime
import os
import re
import subprocess

from lektor.pluginsystem import Plugin
from lektor.reporter import reporter
from lektor.metaformat import tokenize


class TorBlogPlugin(Plugin):
    """ Custom plugin for the Tor Project Blog """

    name = 'TorBlog'
    description = u'Custom plugin for the Tor Project blog.'

    def __init__(self, *args, **kwargs):
        Plugin.__init__(self, *args, **kwargs)

        self.alt_sidebar_placeholder = '<!-- ##SIDEBAR## -->'
        self.alt_sidebar_include = '{%- include "sidebar.html" -%}'
        self.partial_build_days = 90

        self.alt_sidebar = 'BLOG_ALT_SIDEBAR' in os.environ
        self.partial_build = 'BLOG_PARTIAL_BUILD' in os.environ
        self.partial_build_keep = 'BLOG_PARTIAL_BUILD_KEEP' in os.environ

        self.convert_webp = 'BLOG_CONVERT_WEBP' in os.environ
        self.cwebp_path = '/usr/bin/cwebp'
        self.webp_width_medium = 940
        self.webp_width_small = 376
        self.png_to_webp = ['-q', '100', '-mt', '-quiet']
        self.jpg_to_webp = ['-q', '75', '-mt', '-quiet']


    def alt_sidebar_pre(self):
        """ Prepare sources to generate sidebar HTML and replace template include """

        root = self.env.root_path

        # create model
        with open(os.path.join(root, 'models/sidebar.ini'), 'w') as model:
            model.write('[model]\nname = Sidebar\n')

        # create contents file
        if not os.path.exists(os.path.join(root, 'content/sidebar')):
            os.mkdir(os.path.join(root, 'content/sidebar'))
        with open(os.path.join(root, 'content/sidebar/contents.lr'), 'w') as contents:
            contents.write('_model: sidebar\n---\n_template: sidebar.html\n')

        # replace sidebar template include with placeholder
        with open(os.path.join(root, 'templates/layout.html'), 'r') as layout_file:
            layout = layout_file.read()
            layout = re.sub(self.alt_sidebar_include, self.alt_sidebar_placeholder, layout)
        with open(os.path.join(root, 'templates/layout.html'), 'w') as layout_template:
            layout_template.write(layout)


    def alt_sidebar_post(self, builder):
        """ Process build artifacts to replace sidebar placeholder with real HTML """

        destpath = builder.destination_path

        # read sidebar html
        with open(os.path.join(destpath, 'sidebar/index.html'), 'r') as sidebar_file:
            sidebar = sidebar_file.read()

        # swap placeholder with html
        for root, _, files in os.walk(builder.destination_path):
            if 'index.html' in files:
                with open(root + '/index.html', 'r') as index_file:
                    html = index_file.read()
                    # fix relative links
                    components = re.sub(destpath, '', root).split('/')
                    num_comp = len(components)
                    if num_comp == 1:
                        sb = re.sub(r'\bhref="../', 'href="', sidebar)
                    elif num_comp > 2:
                        dots = '../' * (num_comp - 1)
                        sb = re.sub(r'\bhref="../', 'href="' + dots, sidebar)
                    else:
                        sb = sidebar
                    # perform substitution
                    (html, subs) = re.subn(self.alt_sidebar_placeholder, sb, html, count=1)
                if subs:
                    with open(root + '/index.html', 'w') as index_file:
                        index_file.write(html)

        # restore sidebar template
        root = self.env.root_path
        with open(os.path.join(root, 'templates/layout.html'), 'r') as layout_file:
            layout = layout_file.read()
            layout = re.sub(self.alt_sidebar_placeholder, self.alt_sidebar_include, layout)
        with open(os.path.join(root, 'templates/layout.html'), 'w') as layout_template:
            layout_template.write(layout)


    def partial_build_pre(self):
        """ Exclude older content from the build by appening .skip to the filename """

        today = datetime.datetime.today()

        for base in [ 'content/blog', 'content/event' ]:
            for root, _, files in os.walk(os.path.join(self.env.root_path, base)):
                if re.match(base + '$', root):
                    continue
                if 'contents.lr' in files:
                    filepath = os.path.join(root, 'contents.lr')
                    with open(filepath, "rb") as contents:
                        for key, lines in tokenize(contents, encoding="utf-8"):
                            if key in [ 'pub_date', 'start_date' ]:
                                date = datetime.datetime.strptime(lines[0], '%Y-%m-%d')
                                days_diff = abs((today - date).days)
                                if days_diff > self.partial_build_days:
                                    os.rename(filepath, filepath + '.skip')
                                continue


    def partial_build_post(self):
        """ Restore original contents.lr file """

        for root, _, files in os.walk(os.path.join(self.env.root_path, 'content')):
            if 'contents.lr.skip' in files:
                skippath = os.path.join(root, 'contents.lr.skip')
                filepath = os.path.join(root, 'contents.lr')
                os.rename(skippath, filepath)


    def convert_feed_links(self, builder):
        """ Convert feed links from relative to absolute per standard """

        destpath = builder.destination_path
        config = self.env.load_config()

        for root, _, files in os.walk(builder.destination_path):
            if 'feed.xml' in files:
                with open(root + '/feed.xml', 'r') as feed_file:
                    xml = feed_file.read()
                    baseurl = config.base_url
                    (xml, subs) = re.subn(r'((href|src) *= *[\x27s"](?!(http|https)))/?', r'\1' + baseurl, xml)
                if subs:
                    with open(root + '/feed.xml', 'w') as feed_file:
                        feed_file.write(xml)


    def convert_webp_pre(self):
        """ Generate WebP alternative sources in medium and small sizes """

        base = 'content/blog'

        for root, _, files in os.walk(os.path.join(self.env.root_path, base)):

            if 'contents.lr' not in files:
                # nothing to do here
                continue

            if 'lead.webp' in files:
                # conversion already done? skip
                continue

            elif 'lead.png' in files:
                src_img = os.path.join(root, 'lead.png')
                convert_medium_cmd = [ self.cwebp_path, \
                                       '-resize', str(self.webp_width_medium), '0' ] + \
                                       self.png_to_webp + \
                                       [ src_img, '-o', os.path.join(root, 'lead.webp') ]
                convert_small_cmd = [ self.cwebp_path, \
                                       '-resize', str(self.webp_width_small), '0' ] + \
                                       self.png_to_webp + \
                                       [ src_img, '-o', os.path.join(root, 'lead_small.webp') ]

                subprocess.run(convert_medium_cmd, check=True)
                subprocess.run(convert_small_cmd, check=True)

            elif 'lead.jpg' in files:
                src_img = os.path.join(root, 'lead.jpg')
                convert_medium_cmd = [ self.cwebp_path, \
                                       '-resize', str(self.webp_width_medium), '0' ] + \
                                       self.jpg_to_webp + \
                                       [ src_img, '-o', os.path.join(root, 'lead.webp') ]
                convert_small_cmd = [ self.cwebp_path, \
                                       '-resize', str(self.webp_width_small), '0' ] + \
                                       self.jpg_to_webp + \
                                       [ src_img, '-o', os.path.join(root, 'lead_small.webp') ]

                subprocess.run(convert_medium_cmd, check=True)
                subprocess.run(convert_small_cmd, check=True)


    def on_before_build_all(self, builder, **extra):
        """ Before build process begins """

        if self.alt_sidebar:
            reporter.report_generic("Start setting up alternate sidebar")
            self.alt_sidebar_pre()
            reporter.report_generic("Finished setting up alternate sidebar")

        if self.partial_build:
            reporter.report_generic("Start pruning sources for partial build")
            self.partial_build_pre()
            reporter.report_generic("Finished pruning sources for partial build")

        if self.convert_webp:
            reporter.report_generic("Start lead image conversion to WebP")
            if not os.path.exists(self.cwebp_path):
                reporter.report_generic("ERROR: Executable not found: " + self.cwebp_path)
            else:
                self.convert_webp_pre()
            reporter.report_generic("Finished lead image conversion to WebP")


    def on_after_build_all(self, builder, **extra):
        """ After build process is finished """

        if self.alt_sidebar:
            reporter.report_generic("Start integrating alternate sidebar")
            self.alt_sidebar_post(builder)
            reporter.report_generic("Finish integrating alternate sidebar")

        if self.partial_build and not self.partial_build_keep:
            reporter.report_generic("Start restoring sources for partial build")
            self.partial_build_post()
            reporter.report_generic("Finished restoring sources for partial build")

        reporter.report_generic("Start converting feed links")
        self.convert_feed_links(builder)
        reporter.report_generic("Finished converting feed links")


    def on_setup_env(self, **extra):
        """ Setup extra build environment vars """

        # set up 'today' global with the current date
        self.env.jinja_env.globals['today'] = datetime.date.today()

        # set up cutoff date where we stop displaying a default lead image
        # because most posts don't have one
        self.env.jinja_env.globals['default_image_cutoff'] = datetime.date(2017, 5, 31)
